#!/usr/bin/env bash

if [ -n "$MHM2_BUILD_ENV" ]; then
    echo "Source $MHM2_BUILD_ENV"
    source $MHM2_BUILD_ENV
fi

upcxx_exec=`which upcxx`


if [ -z "$upcxx_exec" ]; then
    echo "upcxx not found. Please install or set path."
    exit 1
fi

upcxx_exec_canonical=$(readlink -f $upcxx_exec)
if [ "$upcxx_exec_canonical" != "$upcxx_exec" ]; then
    echo "Found symlink for upcxx - using target at $upcxx_exec_canonical"
    export PATH=`dirname $upcxx_exec_canonical`:$PATH
fi


set -e

SECONDS=0

rootdir=`pwd`

INSTALL_PATH=${MHM2_INSTALL_PATH:=$rootdir/install}

if [ "$1" == "Debug" ] || [ "$1" == "Release" ] || [ "$1" == "RelWithDebInfo" ] || [ "$1" == "clean" ]; then
    ADDITIONAL_PARMS=$2
else
    ADDITIONAL_PARMS=$1
fi

if [[ $ADDITIONAL_PARMS == *"-DENABLE_CUDA=0"* ]]; then
    build_dir=.build/CPU
    INSTALL_PATH=$INSTALL_PATH/CPU
elif [[ $ADDITIONAL_PARMS == *"-DENABLE_CUDA=1"* ]]; then
    build_dir=.build/GPU
    INSTALL_PATH=$INSTALL_PATH/GPU
else
    build_dir=.build
    INSTALL_PATH=$INSTALL_PATH
fi


echo "Installing into $INSTALL_PATH"

rm -rf $INSTALL_PATH/bin/mhm2

if [ "$1" == "cleanall" ]; then
    cd $rootdir
    rm -rf $build_dir/*
    # if this isn't removed then the the rebuild will not work
    rm -rf $INSTALL_PATH/cmake
    exit 0
elif [ "$1" == "clean" ]; then
    cd $rootdir
    rm -rf $build_dir/{bin,CMake*,lib*,makeVersionFile,src,test,cmake*,Makefile,make*}
    # if this isn't removed then the the rebuild will not work
    rm -rf $INSTALL_PATH/cmake
    exit 0
else
    mkdir -p $rootdir/$build_dir
    cd $rootdir/$build_dir
    testing=0
    if [ "$1" == "Debug" ] || [ "$1" == "RelWithDebInfo" ]; then
      testing=1
    fi
    if [ "$1" == "Debug" ] || [ "$1" == "Release" ] || [ "$1" == "RelWithDebInfo" ]; then
        #rm -rf *
        rm -rf $INSTALL_PATH/cmake
        cmake $rootdir -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=$1 -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH \
              -DMHM2_ENABLE_TESTING=$testing -DCMAKE_CXX_FLAGS="-Wno-deprecated-declarations" $MHM2_CMAKE_EXTRAS $ADDITIONAL_PARMS
    fi
    #make -j ${MHM2_BUILD_THREADS} all || make VERBOSE=1 all
    make -j ${MHM2_BUILD_THREADS} all
    if [ "$testing" == "1" ] ; then
       make check
    fi
    make -j ${MHM2_BUILD_THREADS} install
fi

echo "Build took $((SECONDS))s"
