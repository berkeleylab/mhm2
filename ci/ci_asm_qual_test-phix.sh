#!/bin/bash

mhm2_install_dir=$(dirname $(dirname $(realpath $0) ) )
if [ -n "$1" -a -d "$1" ] || [ ! -x ${mhm2_install_dir}/bin/ci_asm_qual_test.sh ]
then
  mhm2_install_dir=$(realpath $1)
  shift
fi

refs=PhiX174_Ill.fsa
if [ ! -f "$refs" ]; then
    rm -f ${refs}.gz
    wget https://portal.nersc.gov/dna/RD/Adv-Seq/PhiX/PhiX174_Ill.fsa.gz
    gunzip ${refs}.gz 
fi
reads=5000-10M-1083.8.1176.fastq
reads2=10000-10M-1089.8.1179.fastq
if [ ! -f "$reads" ]; then
    rm -f 50x-PhiX.tar.gz
    wget https://portal.nersc.gov/dna/RD/Adv-Seq/PhiX/50x-PhiX.tar.gz
    tar -xvzf 50x-PhiX.tar.gz
fi

wd=`pwd`
test_dir=$wd/test-phix
if [[ "$*" != *"--restart"* ]]
then
  rm -rf $test_dir
else
  echo "Restarting in $test_dir"
fi
uptime
set -x
TIMEOUTOPTS="-k 1m -s INT -v"
if [ -t 0 -a -t 1 ]; then TIMEOUTOPTS="${TIMEOUTOPTS} --foreground" ; fi
timeout ${TIMEOUTOPTS} 5m $mhm2_install_dir/bin/mhm2.py --adapter-trim=0 $@ -r $reads $reads2 -o $test_dir --post-asm  
status=$?
if [ $status -ne 0 ]
then
  echo "MHM2 failed! - $status"
  exit 1
fi
uptime
timeout ${TIMEOUTOPTS} 5m $mhm2_install_dir/bin/check_asm_quality.py --asm-dir $test_dir --expected-quals $mhm2_install_dir/share/good-phix.txt --refs $wd/$refs 2>&1 \
   | tee $test_dir/check_asm_quality_test.log
status="$? ${PIPESTATUS[*]}"
if [ "$status" != "0 0 0" ]
then
  echo "check_asm_quality.py failed! - $status"
  exit 1
fi
uptime
