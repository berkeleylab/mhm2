All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                     final_assembly
# contigs (>= 0 bp)          7295          
# contigs (>= 1000 bp)       4976          
# contigs (>= 5000 bp)       2080          
# contigs (>= 10000 bp)      1324          
# contigs (>= 25000 bp)      657           
# contigs (>= 50000 bp)      392           
Total length (>= 0 bp)       95433785      
Total length (>= 1000 bp)    93808116      
Total length (>= 5000 bp)    87133325      
Total length (>= 10000 bp)   81788291      
Total length (>= 25000 bp)   71509133      
Total length (>= 50000 bp)   62429980      
# contigs                    7295          
Largest contig               1244687
Total length                 95433785      
Reference length             102521842     
N50                          100878
N75                          24954         
L50                          204           
L75                          660           
# misassemblies              103           
# misassembled contigs       100           
Misassembled contigs length  7580859       
# local misassemblies        28
# scaffold gap ext. mis.     0             
# scaffold gap loc. mis.     0             
# unaligned mis. contigs     0             
# unaligned contigs          0 + 2 part    
Unaligned length             1227
Genome fraction (%)          93.766        
Duplication ratio            1.004         
# N's per 100 kbp            0.00          
# mismatches per 100 kbp     30.0
# indels per 100 kbp         1.45          
Largest alignment            1244956
Total aligned length         95417119      
NA50                         99287
NA75                         23720         
LA50                         214           
LA75                         687           
