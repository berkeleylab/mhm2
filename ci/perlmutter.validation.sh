#!/bin/bash

# CI Validation script for perlmutter

set -e
set -x

uname -a
pwd
date

if [ -z "${MHM2_SOURCE}" ] || [ -z "${CI_SCRATCH}" ]
then
  echo "please set the MHM2_SOURCE and CI_SCRATCH environmental variables"
  exit 1
fi

export INSTALL_PREFIX=${CI_SCRATCH}/install
export BUILD_PREFIX=${CI_SCRATCH}/build
export RUN_PREFIX=${CI_SCRATCH}/runs
mkdir -p ${RUN_PREFIX}

cd ${MHM2_SOURCE}
git describe || git describe --always
cd upcxx-utils
git describe || git describe --always
cd -

export HIPMER_DATA=${SCRATCH}/GitlabCIData/
export GASNET_BACKTRACE=1
export GASNET_USE_HUGEPAGES=0

cd $CI_SCRATCH

for f in arctic_sample_0.fq  arctic_samples.fq  arcticsynth-refs.fa PhiX174_Ill.fsa 5000-10M-1083.8.1176.fastq 10000-10M-1089.8.1179.fastq
do
  if [ ! -f ${HIPMER_DATA}/$f ]
  then
    echo "Missing $f in $HIPMER_DATA"
    exit 1
  fi
done

slurm_jobs=
echo "Testing builds on perlmutter"

for arch in gpu cpu
do
  slurm_opts="-C $arch --qos=debug --time=30:00 --account=m342"
  phix_gpu_opts=
  nodes=5
  if [ "$arch" == "gpu" ]
  then
    nodes=2
    slurm_opts="${slurm_opts}_g -G $((4*nodes))"
  fi
  inst=${INSTALL_PREFIX}-${arch}
  DBG=$inst-Debug/bin/mhm2.py
  REL=$inst-Release/bin/mhm2.py
  RWDI=$inst-RelWithDebInfo/bin/mhm2.py
  PHIX_OPTS="--adapter-trim=0 -r ${HIPMER_DATA}/5000-10M-1083.8.1176.fastq ${HIPMER_DATA}/10000-10M-1089.8.1179.fastq --post-asm ${phix_gpu_opts}"
  OPTS="-r ${HIPMER_DATA}/arctic_sample_0.fq -v --checkpoint=yes"
  echo "Submitting job on ${nodes} $arch nodes"
  old=${SLURM_MEM_PER_CPU}
  unset SLURM_MEM_PER_CPU
  job=$(sbatch --parsable --job-name="CImvg-${CI_COMMIT_SHORT_SHA}" ${slurm_opts} --nodes=${nodes} --wrap="source $inst-Release/env.sh ; module list ; env|grep SLURM; env|grep GAS; env|grep UPC; env|grep FI; set -x; ${DBG} $PHIX_OPTS -o ${RUN_PREFIX}/$arch-dbg-phix && echo 'sleeping for 15s' && sleep 15 && ${RWDI} $OPTS -o ${RUN_PREFIX}/$arch-rwdi-0 && echo 'sleeping for 15s so that srun can recover' && sleep 15 && ${RWDI} ${OPTS} --kmer-lens 63 --scaff-kmer-lens 63 --use-kmer-filter=0 -o ${RUN_PREFIX}/$arch-rwdi-0-k63 && echo Good")
  export SLURM_MEM_PER_CPU=${old}
  slurm_jobs="$slurm_jobs $job"
done

for job in $slurm_jobs
do
  echo "Waiting for jobs to complete: $slurm_jobs at $(date)"
  while /bin/true
  do
    sleep 60
    echo "Checking for ${job} at $(date)"
    sacct=$(sacct -j $job -o state -X -n 2>/dev/null || true)
    if [ -n "${sacct}" -a -z "$(echo "${sacct}" | grep ING)" ] ; then break ; fi
    squeue -u ${USER} -o "%.16i %.10q %.5P %.10j %.8u %.5T %.10M %.9l %.16S %.9p %.6D %.6C %R"
  done
  echo "sacct $sacct"
  sacct=$(sacct -j $job -X -n)
  echo "sacct $sacct"
  cat slurm-${job}.out
  wasgood=$(echo "${sacct}" | grep -v '0:0' || true)
  if [ -z "$wasgood" ] ; then  true ; else  echo "job ${job} failed somehow - ${wasgood}"; false ; fi
done

cmds="set -x ;"
waits="passed=PASSED ; echo Waiting for Assemblies to complete && date" 
for arch in gpu cpu
do
  d1=${arch}-rwdi-0 
  d2=${arch}-rwdi-0-k63
  d3=${arch}-dbg-phix
  for r in $d1 $d2
  do
    d=${RUN_PREFIX}/$r
    if [ ! -f $d/final_assembly.fasta ] ; then FAILED="${FAILED} Did not find final_assembly.fasta in $d" ; fi
  done
  cmds="$cmds $inst-Release/bin/check_asm_quality.py --asm-dir ${RUN_PREFIX}/${d1} --expected-quals ${inst}-Release/share/good-arctic-sample0.txt --refs ${HIPMER_DATA}/arcticsynth-refs.fa && echo ${d1} okay &"
  waits="$waits ; wait -n && echo Completed Assembly && date || passed=FAILED "
  cmds="$cmds $inst-Release/bin/check_asm_quality.py --asm-dir ${RUN_PREFIX}/${d2} --expected-quals ${inst}-Release/share/good-arctic-sample0-k63.txt --refs ${HIPMER_DATA}/arcticsynth-refs.fa && echo ${d2} okay &"
  waits="$waits ; wait -n && echo Completed Assembly && date || passed=FAILED "
  cmds="$cmds $inst-Release/bin/check_asm_quality.py --asm-dir ${RUN_PREFIX}/${d3} --expected-quals ${inst}-Release/share/good-phix.txt  --refs ${HIPMER_DATA}/PhiX174_Ill.fsa && echo ${d3} okay &"
  waits="$waits ; wait -n && echo Completed Assembly && date || passed=FAILED "
  waits="$waits ; [ \$passed == 'PASSED' ] && echo Assemblies completed && date"
done

echo "Submitting $cmds $waits"
dt=$(date '+%Y%m%d_%H%M%S')
OUT=perlmutter.validation.${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}.out
sbatch --output=$OUT --wait --qos=debug --time=30:00 --account=m342 -C cpu -c 16 --wrap="$cmds $waits" || FAILED="Failed quality checks"
cat $OUT

if [ -z "$FAILED" ] ; then echo "OK" ; else echo "Something failed somehow - ${FAILED}" ; false ; fi

echo "Done validating $(date) in ${SECONDS}"
