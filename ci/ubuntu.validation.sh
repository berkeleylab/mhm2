#!/bin/bash

set -e

USAGE="$0 base_dir [lockpath]
Optionally set UPCXX_VER to download and install that version of UPCXX

"


BASE=$1
LOCKPATH=$2
if [ -z "$BASE" ]
then
	echo $USAGE
	exit 1
fi

UPCXX_VER=${UPCXX_VER:=2023.9.0}
echo "Using upcxx version $UPCXX_VER"

CI_CMAKE_OPTS=${CI_CMAKE_OPTS}
echo "Using CI_CMAKE_OPTS=${CI_CMAKE_OPTS}"

export CI_INSTALL=${CI_INSTALL:=$BASE/ci-install-${CI_PROJECT_NAME}-upcxx-${UPCXX_VER}}
export HIPMER_DATA=${HIPMER_DATA:=${BASE}/scratch/}
export CI_SCRATCH=${CI_SCRATCH:=${BASE}/scratch/${CI_PROJECT_NAME}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_TAG}-${CI_PIPELINE_ID}}
export RUN_PREFIX=${RUN_PREFIX:=${CI_SCRATCH}/runs}
export INSTALL_PREFIX=${INSTALL_PREFIX:=${CI_SCRATCH}}

mkdir -p ${HIPMER_DATA}
export GASNET_BACKTRACE=1

if [ -z "$LOCKPATH" ]
then
  LOCKPATH=$HIPMER_DATA
fi

echo "Validating all tests under BASE=$BASE and CI_SCRATCH=$CI_SCRATCH. Locking: $LOCKPATH"

export MHM2_SOURCE=$(pwd)
df -h
uname -a
uptime
pwd
date

FAILED=""
which upcxx || FAILED="${FAILED} could not install upcxx"
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]
upcxx --version || FAILED="${FAILED} no upcxx was found"
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

set -x

cd ${CI_SCRATCH}
reads=${HIPMER_DATA}/arctic_sample_0.fq 
export DBG=${INSTALL_PREFIX}/mhm2-dbg/bin/
export RWDI=${INSTALL_PREFIX}/mhm2-rwdi/bin/
export REL=${INSTALL_PREFIX}/mhm2-rel/bin/

for f in arctic_sample_0.fq arcticsynth-refs.fa PhiX174_Ill.fsa 5000-10M-1083.8.1176.fastq 10000-10M-1089.8.1179.fastq arctic_sample_0_tiny.fq arctic_sample_0_tiny2.fq
do
  if [ ! -f $f -a -f ${HIPMER_DATA}/$f ] ; then ln -s ${HIPMER_DATA}/$f ; fi
done

echo "Locking against multiple instantiations on this node"
lockfile=${LOCKPATH}/mhm2-ci-$(uname -n)
${DBG}/ci_lock_multinode.sh ${lockfile} 1800 $(pwd)

echo "Starting Debug PhiX CI using ${DBG}/mhm2"
${DBG}/ci_asm_qual_test-phix.sh || FAILED="${FAILED} Could not run ci_asm_qual_test-phix"
rm -f ${lockfile}
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

${DBG}/ci_lock_multinode.sh ${lockfile} 600 $(pwd)
echo "Starting Debug tiny CI using ${DBG}/mhm2"
${DBG}/ci_asm_qual_test-tiny.sh || FAILED="${FAILED} Could not run ci_asm_qual_test-tiny"
rm -f ${lockfile}
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

${RWDI}/ci_lock_multinode.sh ${lockfile} 600 $(pwd)
echo "Starting RelWithDebInfo mhm2 on Arctic $reads using ${RWDI}/mhm2"
${RWDI}/ci_asm_qual_test.sh || FAILED="${FAILED} Could not run ci_asm_qual_test"
rm -f ${lockfile}
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

if [ ! -f ./test-arctic-sample0/final_assembly.fasta ] ; then FAILED="${FAILED} Did not find final_assembly.fasta for rwdi in test-arctic-sample0" ; fi
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

rm -rf ${RUN_PREFIX}/rwdi-test-arctic-sample0
mv test-arctic-sample0 ${RUN_PREFIX}/rwdi-test-arctic-sample0
echo "Starting debug run on with reduced workflow using ${DBG}/mhm2"

${RWDI}/ci_lock_multinode.sh ${lockfile} 600 $(pwd)
TIMEOUTOPTS="-k 1m -s INT -v"
if [ -t 0 -a -t 1 ]; then TIMEOUTOPTS="${TIMEOUTOPTS} --foreground" ; fi
timeout ${TIMEOUTOPTS} 25m ${DBG}/mhm2.py -r $reads -o ${RUN_PREFIX}/dbg-k3163 --kmer-lens 31,63 --scaff-kmer-lens 63 --checkpoint=no -v || FAILED="${FAILED} Could not run dbg"
rm -f ${lockfile}
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

echo "verify dbg-k3163 results $(ls -la ${RUN_PREFIX}/dbg-k3163)"
if [ ! -f ${RUN_PREFIX}/dbg-k3163/final_assembly.fasta ] ; then FAILED="${FAILED} Did not find final_assembly.fasta on dbg-k3163" ; fi
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

${RWDI}/ci_lock_multinode.sh ${lockfile} 600 $(pwd)
timeout ${TIMEOUTOPTS} 10m ${DBG}/check_asm_quality.py --asm-dir ${RUN_PREFIX}/dbg-k3163 --expected-quals ${DBG}/../share/good-arctic-sample0-k31k63.txt --refs ${HIPMER_DATA}/arcticsynth-refs.fa || FAILED="${FAILED} did not pass check_asm_quality.py for dbg-k3163 with reduced workflow k31k63"
rm -f ${lockfile}
if [ -z "$FAILED" ] ; then  true ; else echo "Something failed somehow - ${FAILED}"; false ; fi
echo "FAILED=${FAILED}" && [ -z "$FAILED" ]

rm -f $lockfile

echo "Completed $0 Successfully at $(date)"
