#!/bin/bash

lockfile=$1
timeout_secs=$2
lockname=$3

[ -n "${timeout_secs}" ] || timeout_secs=600 # 10 minutes

USAGE="$0 lockfile [timeout=${timeout_secs} ] [ description ]"
if [ -z "$lockfile" ]
then
  echo "$USAGE" 1>&2
  exit 1
fi

set -e

get_val()
{
 tmout=$1
 echo "$(($(date '+%s')+$tmout))-$(uname -n)-${lockname}"
}

msg=0
while [ -h "$lockfile" ]
do
  val=$(readlink ${lockfile})
  val_s=${val%%-*}
  now=$(get_val 0)
  now_s=${now%%-*}
  if [ $val_s -lt $now_s ]
  then
    echo "Breaking exiting lock ${lockfile} -> ${val} as it has expired"
    rm ${lockfile}
  else
    if [ $msg -eq 0 ]
    then
      echo "Lock exists already. waiting for it to be released or timeout in $(($val_s - $now_s)) s: ${val}"
      msg=1
    fi
    echo waiting... $SECONDS of $timeout_secs
    sleep 15
  fi
  if [ $SECONDS -gt $timeout_secs ]
  then
    echo "Timeout waiting for existing lock to expire: ${lockfile} -> ${val}"
    exit 1
  fi
done  

ln -s "$(get_val $timeout_secs)" $lockfile
ls -la ${lockfile}
