#!/bin/bash

# CI Build script for perlmutter

set -e

uname -a
pwd
date

if [ -z "${MHM2_SOURCE}" ] || [ -z "${CI_SCRATCH}" ]
then
  echo "please set the MHM2_SOURCE and CI_SCRATCH environmental variables"
  exit 1
fi

export INSTALL_PREFIX=${CI_SCRATCH}/install
export BUILD_PREFIX=/dev/shm/build
export RUN_PREFIX=${CI_SCRATCH}/runs
export TMPDIR=${CI_SCRATCH}/tmp
export MHM2_BUILD_THREADS=${MHM2_BUILD_THREADS:=64}
export HIPMER_DATA=${SCRATCH}/GitlabCIData/

mkdir -p ${CI_SCRATCH}/tmp

echo "Verifing and/or downloading test arctic data set"
export ARCTIC_URL=https://portal.nersc.gov/project/hipmer/MetaHipMer_datasets_12_2019/ArcticSynth/samples/
[ -f ${HIPMER_DATA}/arcticsynth-refs.fa ] || ( curl -LO ${ARCTIC_URL}/arcticsynth-refs.fa.gz && gunzip arcticsynth-refs.fa.gz && mv arcticsynth-refs.fa ${HIPMER_DATA}/arcticsynth-refs.fa )
for i in 0 1 2 3 4 5 6 7 8 9 10 11 ; do [ -f ${HIPMER_DATA}/arctic_sample_$i.fq ] || ( curl -LO ${ARCTIC_URL}/arctic_sample_$i.fq.gz && gunzip arctic_sample_$i.fq.gz && mv arctic_sample_$i.fq ${HIPMER_DATA}/arctic_sample_$i.fq ) ; done
[ -f ${HIPMER_DATA}/arctic_samples.fq ] || cat ${HIPMER_DATA}/arctic_sample_?.fq ${HIPMER_DATA}/arctic_sample_??.fq > ${HIPMER_DATA}/arctic_samples.fq

echo "Verifing and/or downloading PhiX data set"

phix=PhiX174_Ill.fsa
[ -f "${HIPMER_DATA}/$phix" ] || ( curl -LO https://portal.nersc.gov/dna/RD/Adv-Seq/PhiX/${phix}.gz && gunzip ${phix}.gz && mv ${phix} ${HIPMER_DATA}/$phix )

reads=5000-10M-1083.8.1176.fastq
reads2=10000-10M-1089.8.1179.fastq
[ -f "${HIPMER_DATA}/$reads" -a -f "${HIPMER_DATA}/$reads2" ] || ( curl -LO https://portal.nersc.gov/dna/RD/Adv-Seq/PhiX/50x-PhiX.tar.gz && tar -xvzf 50x-PhiX.tar.gz && mv $reads $reads2 ${HIPMER_DATA}/ )


cd ${MHM2_SOURCE}
git describe || git describe --always
cd upcxx-utils
git describe || git describe --always
cd -

(
echo "Loading GPU environment"
envir=${MHM2_SOURCE}/contrib/environments/perlmutter/gnu.sh
source $envir
module list
env | grep '\(GASNET\|FI_\|UPC\)'
env | grep SLURM || true
env | grep TMP

df -h $TMPDIR

for t in Debug RelWithDebInfo Release
do
  export MHM2_BUILD=${BUILD_PREFIX}-gpu-$t
  mkdir -p ${MHM2_BUILD}
  cd $MHM2_BUILD
  echo "Building gpu GNU ${t}"
  CXX=CC cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX}-gpu-${t} -DCMAKE_BUILD_TYPE=${t} ${MHM2_CMAKE_EXTRAS} ${MHM2_SOURCE}
  make -j ${MHM2_BUILD_THREADS} VERBOSE=1 all check install
  cp -p $envir ${INSTALL_PREFIX}-gpu-${t}/env.sh # store environment to support execution
  cd ${CI_SCRATCH} && mv $MHM2_BUILD . &
done
)

(
echo "Loading CPU-only environment"
envir=${MHM2_SOURCE}/contrib/environments/perlmutter/gnu-cpuonly.sh
source $envir
module list
env | grep '\(GASNET\|FI_\|UPC\)'

for t in Debug RelWithDebInfo Release
do
  export MHM2_BUILD=${BUILD_PREFIX}-cpu-$t
  mkdir -p ${MHM2_BUILD}
  cd $MHM2_BUILD
  echo "Building cpu GNU ${t}"
  CXX=CC cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX}-cpu-${t} -DCMAKE_BUILD_TYPE=${t} ${MHM2_CMAKE_EXTRAS} ${MHM2_SOURCE}
  make -j ${MHM2_BUILD_THREADS} VERBOSE=1 all check install
  cp -p $envir ${INSTALL_PREFIX}-cpu-${t}/env.sh # store environment to support execution
  cd ${CI_SCRATCH} && mv $MHM2_BUILD . &
done
)

echo "Done building $(date) in ${SECONDS}"

