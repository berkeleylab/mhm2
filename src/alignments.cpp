/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include "alignments.hpp"

#include <fcntl.h>
#include <limits>
#include <sstream>
#include <string>
#include <upcxx/upcxx.hpp>
#include <unordered_set>
#include <bitset>

#include "contigs.hpp"
#include "utils.hpp"
#include "version.h"
#include "zstr.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/ofstream.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "upcxx_utils/timers.hpp"
#include "upcxx_utils/mem_profile.hpp"

using namespace upcxx_utils;
using namespace std;

using upcxx::future;

static int get_pair_id(const string &read_id) {
  string pair_id_str = read_id.substr(read_id.length() - 2, 2);
  int pair_id = 0;
  if (pair_id_str == "/1") pair_id = 1;
  if (pair_id_str == "/2") pair_id = 2;
  return pair_id;
}
//
// class Aln
//
Aln::Aln()
    : read_id("")
    , cid(-1)
    , cstart(0)
    , cstop(0)
    , clen(0)
    , rstart(0)
    , rstop(0)
    , rlen(0)
    , score1(0)
    , score2(0)
    , mismatches(0)
    , identity(0)
    , mapq(0)
    , sam_string({})
    , cigar({})
    , read_group_id(-1)
    , orient() {}

Aln::Aln(const string &read_id, int64_t cid, int rstart, int rstop, int rlen, int cstart, int cstop, int clen, char orient,
         int score1, int score2, int mismatches, int read_group_id)
    : read_id(read_id)
    , cid(cid)
    , cstart(cstart)
    , cstop(cstop)
    , clen(clen)
    , rstart(rstart)
    , rstop(rstop)
    , rlen(rlen)
    , score1(score1)
    , score2(score2)
    , mismatches(mismatches)
    , identity(0)
    , mapq(0)
    , sam_string({})
    , cigar({})
    , read_group_id(read_group_id)
    , orient(orient) {
  set_identity();
  assert(is_valid());
}

Aln::Aln(const string &read_id, int64_t cid, int rlen, int cstart, int clen, char orient, int read_group_id)
    : read_id(read_id)
    , cid(cid)
    , cstart(cstart)
    , cstop(cstart)
    , clen(clen)
    , rstart(0)
    , rstop(0)
    , rlen(rlen)
    , score1(0)
    , score2(0)
    , mismatches(0)
    , identity(0)
    , mapq(0)
    , sam_string({})
    , cigar({})
    , read_group_id(read_group_id)
    , orient(orient) {
  assert(is_valid());
}

void Aln::set(int ref_begin, int ref_end, int query_begin, int query_end, int top_score, int next_best_score, int aln_mismatches,
              int aln_read_group_id) {
  cstop = cstart + ref_end + 1;
  cstart += ref_begin;
  rstop = rstart + query_end + 1;
  rstart += query_begin;
  if (orient == '-') switch_orient(rstart, rstop, rlen);
  score1 = top_score;
  score2 = next_best_score;
  mismatches = aln_mismatches;
  read_group_id = aln_read_group_id;
  set_identity();
}

static string construct_sam_string(const string &read_id, int flags, int64_t cid, int map_pos, int mapq, const string &cigar,
                                   string pair_read_id, int pair_pos, int tlen, int score1, int mismatches, int read_group_id,
                                   int identity) {
  string sam_string = "";
  // query name
  sam_string = read_id + "\t";
  // flags: https://www.samformat.info/sam-format-flag
  sam_string += to_string(flags) + "\t";
  // reference name
  sam_string += "scaffold_" + to_string(cid) + "\t";
  // first mapping position in reference
  sam_string += to_string(map_pos) + "\t";
  // mapping quality score
  sam_string += to_string(mapq) + "\t";
  // cigar
  sam_string += cigar + "\t";
  // reference name that mate read maps to
  sam_string += pair_read_id + "\t";
  // position of mate read in reference
  sam_string += to_string(pair_pos) + "\t";
  // template length (should be aln length)
  sam_string += to_string(tlen) + "\t";
  // read sequence - empty to save space
  sam_string += "*\t";
  // read quals - empty to save space
  sam_string += "*\t";
  // tag - alignment score
  sam_string += "AS:i:" + to_string(score1) + "\t";
  // tag - number of mismatches
  sam_string += "NM:i:" + to_string(mismatches) + "\t";
  // tag - read group (sample)
  sam_string += "RG:Z:" + to_string(read_group_id) + "\t";
  // tag - identity
  sam_string += "YI:i:" + to_string(identity);
  return sam_string;
}

void Aln::set_identity() {
  // calculate id from actual aligned length plus overlaps, not from cigar string
  auto [left_overlap, right_overlap] = get_unaligned_overlaps();
  auto overlap = left_overlap + right_overlap;
  int aln_len = max(abs(rstop - rstart), abs(cstop - cstart)) + overlap;
  identity = round(100.0 * (double)(aln_len - mismatches - overlap) / aln_len);
}

static double calc_identity_from_cigar(const string &cigar) {
  int mismatches = 0;
  // need to count the number of mismatches because these are not available from the GPU computation
  string count_str = "";
  int aln_len = 0;
  int matches = 0;
  char c;
  for (int i = 0; i < cigar.length(); i++) {
    c = cigar[i];
    if (isdigit(c)) {
      count_str += c;
    } else {
      int count = std::stoi(count_str);
      if (c == 'X' || c == 'I' || c == 'D')
        mismatches += count;
      else if (c == '=')
        matches += count;
      if (c != 'D') aln_len += count;
      count_str = "";
    }
  }
  // calculate aln length from cigar string. Identity is just then number of matches divided by alignment length
  return 100.0 * (double)matches / aln_len;
}

static int calc_mismatches_from_cigar(const string &cigar) {
  int mismatches = 0;
  // need to count the number of mismatches because these are not available from the GPU computation
  string count_str = "";
  char c;
  for (int i = 0; i < cigar.length(); i++) {
    c = cigar[i];
    if (isdigit(c)) {
      count_str += c;
    } else {
      int count = std::stoi(count_str);
      if (c == 'X' || c == 'I' || c == 'D') mismatches += count;
      count_str = "";
    }
  }
  return mismatches;
}

void Aln::set_sam_string(string cigar) {
  assert(is_valid());
  this->cigar = cigar;
  int aln_len;
  mismatches = calc_mismatches_from_cigar(cigar);
  //  This mapq is calculated in a similar way to how it is done in bbmap (current/stream/SamLine.java:1958), assuming no penalizing
  //  ambiguity. For compatibility, the bbmap scores max out at 100 per perfect match base, so we convert using our match score
  //  value for the MHM local alignment
  int match_score = (int)(::to_string(BLASTN_ALN_SCORES)[0] - '0');
  double other_score = 1.6 * ((double)score1 - (double)rlen * match_score * 0.4);
  double max_val = 1.5 * log2(rlen) + 36;
  int adjusted = round((other_score * max_val) / (match_score * rlen));
  mapq = max(4, adjusted);
  int flags = (orient == '-' ? 16 : 0);
  sam_string =
      construct_sam_string(read_id, flags, cid, cstart + 1, mapq, cigar, "*", 0, 0, score1, mismatches, read_group_id, identity);
}

void Aln::add_cigar_pair_info(int64_t other_cid, int other_aln_cstart, char other_orient, int read_len) {
  int pair_id = get_pair_id(read_id);
  int flags = 0;
  // read paired
  if (pair_id != 0) flags = 1;
  // read mapped in proper pair
  if (other_cid == cid && orient != other_orient) flags |= 2;
  // mate unmapped
  if (other_cid == -1) flags |= 8;
  // read reverse strand
  if (orient == '-') flags |= 16;
  // mate reverse strand
  if (other_orient == '-') flags |= 32;
  // first in pair
  if (pair_id == 1) flags |= 64;
  // second in pair
  if (pair_id == 2) flags |= 128;
  string other_cid_str = "*";
  if (other_cid == cid || other_cid == -1)
    other_cid_str = "=";
  else
    other_cid_str = "scaffold_" + ::to_string(other_cid);
  int tlen = (other_cid != cid ? 0 : abs(cstart - other_aln_cstart) + read_len);
  if (pair_id == 2) tlen *= -1;
  // calculate id from actual aligned length plus overlaps, not from cigar string
  auto [left_overlap, right_overlap] = get_unaligned_overlaps();
  auto overlap = left_overlap + right_overlap;
  int aln_len = max(abs(rstop - rstart), abs(cstop - cstart)) + overlap;
  identity = round(100.0 * (double)(aln_len - mismatches - overlap) / aln_len);
  sam_string = construct_sam_string(read_id, flags, cid, cstart + 1, mapq, cigar, other_cid_str, other_aln_cstart + 1, tlen, score1,
                                    mismatches, read_group_id, identity);
  // for debugging
  // sam_string += "\t";
  /*sam_string += "YII:i:" + to_string(identity) + "\t";
  sam_string += "YRB:i:" + to_string(rstart) + "\t";
  sam_string += "YRE:i:" + to_string(rstop) + "\t";
  sam_string += "YCB:i:" + to_string(cstart) + "\t";
  sam_string += "YCE:i:" + to_string(cstop) + "\t";
  sam_string += "YCL:i:" + to_string(clen) + "\t";
  sam_string += "YO:c:";
  sam_string += orient;
  sam_string += "\t";
  sam_string += "YAL:i:" + to_string(aln_len) + "\t";
  sam_string += "YLO:i:" + to_string(left_overlap) + "\t";
  sam_string += "YRO:i:" + to_string(right_overlap);*/
}

// minimap2 PAF output format
string Aln::to_paf_string() const {
  assert(is_valid());
  ostringstream os;
  os << read_id << "\t" << rstart + 1 << "\t" << rstop << "\t" << rlen << "\t"
     << "Contig" << cid << "\t" << cstart + 1 << "\t" << cstop << "\t" << clen << "\t" << (orient == '+' ? "Plus" : "Minus") << "\t"
     << score1 << "\t" << score2;
  //<< "0";
  return os.str();
}

// blast6 output format
string Aln::to_blast6_string() const {
  assert(is_valid());
  ostringstream os;
  // we don't track gap opens
  int gap_opens = 0;
  int aln_len = max(rstop - rstart, abs(cstop - cstart));
  os << read_id << "\t"
     << "Contig" << cid << "\t" << fixed << setprecision(3) << identity << "\t" << aln_len << "\t" << mismatches << "\t"
     << gap_opens << "\t" << rstart + 1 << "\t" << rstop << "\t";
  // subject start and end reversed when orientation is minus
  if (orient == '+')
    os << cstart + 1 << "\t" << cstop;
  else
    os << cstop << "\t" << cstart + 1;
  // evalue and bitscore, which we don't have here
  os << "\t0\t0";
  return os.str();
}

bool Aln::is_valid() const {
  assert(rstart >= 0 && "start >= 0");
  assert(rstop <= rlen && "stop <= len");
  assert(cstart >= 0 && "cstart >= 0");
  assert(cstop <= clen && "cstop <= clen");

  return read_group_id >= 0 && (orient == '+' || orient == '-') && mismatches >= 0 && cid >= 0 && read_id.size() > 0;
}

string Aln::to_string() const {
  return make_string(read_id, " rlen=", rlen, " ", rstart, "-", rstop, " orient=", orient, " cid=", cid, " clen=", clen, " ",
                     cstart, "-", cstop, " mm=", mismatches, " pctid=", identity, " mapq=", mapq, " rg=", read_group_id,
                     " cigar=", cigar, " sam=", sam_string);
}

pair<int, int> Aln::get_unaligned_overlaps() const {
  assert(is_valid());
  int fwd_cstart = cstart, fwd_cstop = cstop;
  if (orient == '-') switch_orient(fwd_cstart, fwd_cstop, clen);
  int unaligned_left = min(rstart, fwd_cstart);
  int unaligned_right = min(rlen - rstop, clen - fwd_cstop);
  return {unaligned_left, unaligned_right};
}

bool Aln::check_quality() const {
  int aln_len = max(rstop - rstart, abs(cstop - cstart));
  double perc_id = 100.0 * (aln_len - mismatches) / aln_len;
  int cigar_aln_len = 0;
  int cigar_mismatches = 0;
  string num_str = "";
  for (int i = 0; i < cigar.length(); i++) {
    if (isdigit(cigar[i])) {
      num_str += cigar[i];
      continue;
    }
    int count = stoi(num_str);
    if (cigar[i] != 'S') cigar_aln_len += count;
    num_str = "";
    switch (cigar[i]) {
      case 'X': cigar_mismatches++; break;
      case 'I':
        cigar_mismatches++;
        cigar_aln_len++;
        break;
      case 'D':
        cigar_mismatches++;
        cigar_aln_len--;
        break;
      case '=':
      case 'M':
      case 'S': break;
      default: WARN("unexpected type in cigar: '", cigar[i], "'");
    };
  }
  if (aln_len != cigar_aln_len) DIE(cigar, " ", aln_len, " ", mismatches, " [", cigar_aln_len, " ", cigar_mismatches, "]");
  return true;
}

bool Aln::cmp(const Aln &aln1, const Aln &aln2) {
  assert(aln1.is_valid());
  assert(aln2.is_valid());
  if (aln1.read_id == aln2.read_id) {
    // sort by score, then cstart, then rstart, to get the same ordering across runs (can't use cid since that can change
    // between runs
    if (aln1.score1 != aln2.score1) return aln1.score1 > aln2.score1;
    if (aln1.clen != aln2.clen) return aln1.clen > aln2.clen;
    if (aln1.cstart != aln2.cstart) return aln1.cstart < aln2.cstart;
    if (aln1.rstart != aln2.rstart) return aln1.rstart < aln2.rstart;
    if (aln1.cstop != aln2.cstop) return aln1.cstop < aln2.cstop;
    if (aln1.rstop != aln2.rstop) return aln1.rstop < aln2.rstop;
    if (aln1.orient != aln2.orient) return aln1.orient < aln2.orient;
    if (aln1.cid != aln2.cid) return aln1.cid < aln2.cid;
    // WARN("Duplicate alns: ", aln1.to_paf_string(), " ", aln2.to_paf_string());
    // if (aln1 != aln2) WARN("BUT NOT EQUAL!");
  }
  if (aln1.read_id.length() == aln2.read_id.length()) {
    auto id_len = aln1.read_id.length();
    auto id_cmp = aln1.read_id.compare(0, id_len - 2, aln2.read_id, 0, id_len - 2);
    if (id_cmp == 0) return (aln1.read_id[id_len - 1] == '1');
    return id_cmp > 0;
  }
  return aln1.read_id > aln2.read_id;
}

bool operator==(const Aln &aln1, const Aln &aln2) {
  if (aln1.read_id != aln2.read_id) return false;
  if (aln1.cid != aln2.cid) return false;
  if (aln1.cstart != aln2.cstart) return false;
  if (aln1.cstop != aln2.cstop) return false;
  if (aln1.clen != aln2.clen) return false;
  if (aln1.rstart != aln2.rstart) return false;
  if (aln1.rstop != aln2.rstop) return false;
  if (aln1.rlen != aln2.rlen) return false;
  if (aln1.score1 != aln2.score1) return false;
  if (aln1.orient != aln2.orient) return false;
  return true;
}

bool operator!=(const Aln &aln1, const Aln &aln2) { return (!(aln1 == aln2)); }

//
// class Alns
//

Alns::Alns()
    : num_dups(0)
    , num_bad(0)
    , read_len(0) {
  DBG("Created empty ", (void *)this, "\n");
}

Alns::Alns(int read_len)
    : num_dups(0)
    , num_bad(0)
    , read_len(read_len) {
  DBG("Created empty read_len=", read_len, " ", (void *)this, "\n");
}

Alns::~Alns() { DBG("Destroying with sz=", size(), " ", (void *)this, "\n"); }

void Alns::clear() {
  alns.clear();
  alns_t().swap(alns);
}

void Alns::add_aln(Aln &aln) {
  assert(aln.is_valid());
  // aln.set_identity();
  //  This is not done in bbmap - poorer alns are kept. This filtering is done when computing aln depths
  //  if (new_identity < 97) {
  //    num_bad++;
  //    return;
  //  }
  //   check for multiple read-ctg alns. Check backwards from most recent entry, since all alns for a read are grouped
  for (auto it = alns.rbegin(); it != alns.rend();) {
    assert(it->is_valid());
    // we have no more entries for this read
    if (it->read_id != aln.read_id) break;
    if (it->cid == aln.cid) {
      num_dups++;
      auto old_identity = it->identity;
      auto old_aln_len = it->rstop - it->rstart;
      it++;
      if ((aln.identity > old_identity) || (aln.identity == old_identity && (aln.rstop - aln.rstart > old_aln_len))) {
        // new one is better - erase the old one
        auto fit = it.base();
        if (fit != alns.end()) alns.erase(fit);
        // can only happen once because previous add_aln calls will have ensured there is only the best single aln for that cid
        break;
      } else {
        // new one is no better - don't add
        return;
      }
    } else {
      it++;
    }
  }
  if (!aln.is_valid()) DIE("Invalid alignment: ", aln.to_paf_string());
  assert(aln.is_valid());
  // FIXME: we'd like to require high value alignments, but can't do this because mismatch counts are not yet supported in ADEPT
  // if (aln.identity >= KLIGN_ALN_IDENTITY_CUTOFF)
  // Currently, we just filter based on excessive unaligned overlap
  // Only filter out if the SAM string is not set, i.e. we are using the alns internally rather than for post processing output
  auto [unaligned_left, unaligned_right] = aln.get_unaligned_overlaps();
  auto unaligned = unaligned_left + unaligned_right;
  // int aln_len = max(aln.rstop - aln.rstart + unaligned, abs(aln.cstop - aln.cstart + unaligned));
  if (!aln.sam_string.empty() || (unaligned_left <= KLIGN_UNALIGNED_THRES && unaligned_right <= KLIGN_UNALIGNED_THRES))
    alns.push_back(aln);
  else
    num_bad++;
}

void Alns::add_alns(Alns &alns_to_add) {
  DBG("Appending up to ", alns_to_add.size(), " alignments to ", alns.size(), "\n");
  assert(alns_to_add.num_dups == 0 && "add_alns has not already been added");
  assert(alns_to_add.num_bad == 0 && "add_alns has not already been added");
  reserve(alns.size() + alns_to_add.alns.size());
  for (auto &a : alns_to_add.alns) add_aln(a);
  alns_to_add.clear();
}

void Alns::append(Alns &more_alns) {
  if (more_alns.empty()) return;
  DBG("Appending ", more_alns.size(), " alignments to ", (void *)&alns, " with ", alns.size(), "\n");
  int rg = -2;
  reserve(alns.size() + more_alns.alns.size());
  for (auto &a : more_alns.alns) {
    assert(a.is_valid());
    alns.emplace_back(move(a));
  }
  num_dups += more_alns.num_dups;
  num_bad += more_alns.num_bad;
  more_alns.clear();
}

const Aln &Alns::get_aln(int64_t i) const {
  assert(i < alns.size());
  return alns[i];
}

Aln &Alns::get_aln(int64_t i) {
  assert(i < alns.size());
  return alns[i];
}

size_t Alns::size() const { return alns.size(); }

void Alns::reserve(size_t capacity) {
  // no reserve in deque: alns.reserve(capacity);
}

void Alns::reset() { alns.clear(); }

int64_t Alns::get_num_dups() { return num_dups; }

int64_t Alns::get_num_bad() { return num_bad; }

template <typename OSTREAM>
void Alns::dump_all(OSTREAM &os, Format fmt, int min_ctg_len) const {
  // all ranks dump their valid alignments
  for (const auto &aln : alns) {
    if (aln.cid == -1) continue;
    assert(aln.is_valid());
    // DBG(aln.to_paf_string(), "\n");
    if (aln.clen < min_ctg_len) continue;
    switch (fmt) {
      case Format::PAF: os << aln.to_paf_string() << "\n"; break;
      case Format::BLAST: os << aln.to_blast6_string() << "\n"; break;
      case Format::SAM: os << aln.sam_string << "\n"; break;
    }
    progress();
  }
}

void Alns::dump_rank_file(string fname, Format fmt) const {
  get_rank_path(fname, rank_me());
  zstr::ofstream f(fname);
  dump_all(f, fmt);
  f.close();
  upcxx::barrier();
}

upcxx::future<> Alns::dump_single_file(string fname, Format fmt) const {
  if (fname.rfind(".gz") != fname.size() - 3) fname += ".gz";  // will be a gzip file
  std::stringstream ss;
  zstr::ostream zos(ss);
  dump_all(zos, fmt);
  zos.flush();
  dist_ofstream of(fname);
  of.swapss(ss);  // equivalent but more efficient than cat_stream(ss, of);
  return of.close_and_report_timings();
}

upcxx::future<> Alns::write_sam_header(dist_ofstream &of, const vector<string> &read_group_names, const Contigs &ctgs,
                                       int min_ctg_len) {
  std::stringstream ss;
  zstr::ostream zos(ss);
  // First all ranks dump Sequence tags - @SQ	SN:Contig0	LN:887
  for (const auto &ctg : ctgs) {
    if (ctg.seq.length() < min_ctg_len) continue;
    assert(ctg.id >= 0);
    zos << "@SQ\tSN:scaffold_" << to_string(ctg.id) << "\tLN:" << to_string(ctg.seq.length()) << "\n";
    progress();
  }
  zos.flush();
  of.swapss(ss);  // equivalent but more efficient than: cat_stream(ss, of);
  // all @SQ headers aggregated to the top of the file
  auto all_done = of.flush_collective();

  // rank 0 continues with header
  if (!upcxx::rank_me()) {
    ss.clear();  // will resuse so clear eof
    // add ReadGroup tags - @RG ID:[0-n] DS:filename
    for (int i = 0; i < read_group_names.size(); i++) {
      string basefilename = upcxx_utils::get_basename(read_group_names[i]);
      zos << "@RG\tID:" << to_string(i) << "\tDS:" << basefilename << "\n";
    }
    // add program information
    zos << "@PG\tID:MHM2\tPN:MHM2\tVN:" << string(MHM2_VERSION) << "\n";
    zos.flush();
    of.swapss(ss);  // equivalent but more efficient than: cat_stream(ss, of);
  }
  progress();

  return all_done;
}

upcxx::future<> Alns::write_sam_alignments(dist_ofstream &of, int min_ctg_len) const {
  // next alignments.  rank0 will be first with the remaining header fields
  std::stringstream ss;
  zstr::ostream zos(ss);
  dump_all(zos, Format::SAM, min_ctg_len);
  zos.flush();
  cat_stream(ss, of);
  return of.flush_collective();
}

upcxx::future<> Alns::dump_sam_file(std::string fname, const vector<string> &read_group_names, const Contigs &ctgs,
                                    int min_ctg_len) const {
  if (fname.rfind(".gz") != fname.size() - 3) fname += ".gz";  // will be a gzip file
  dist_ofstream of(fname);
  auto fut = write_sam_header(of, read_group_names, ctgs, min_ctg_len);
  auto fut2 = write_sam_alignments(of, min_ctg_len);
  when_all(fut, fut2, of.close_async()).wait();
  return of.close_and_report_timings();
}

upcxx::future<> Alns::dump_sam_file(string fname, int min_ctg_len) const {
  if (fname.rfind(".gz") != fname.size() - 3) fname += ".gz";  // will be a gzip file
  dist_ofstream of(fname);
  auto fut = write_sam_alignments(of, min_ctg_len);
  when_all(fut, of.close_async()).wait();
  return of.close_and_report_timings();
}

int Alns::calculate_unmerged_rlen() const {
  BarrierTimer timer(__FILEFUNC__);
  // get the unmerged read length - most common read length
  DBG("calculate_unmerged_rlen alns.size=", alns.size(), "\n");
  HASH_TABLE<int, int64_t> rlens;
  int64_t sum_rlens = 0;
  for (auto &aln : alns) {
    if (aln.cid == -1) continue;
    assert(aln.is_valid());
    // DBG("aln.rlen=", aln.rlen, " ", aln.to_paf_string(), "\n");
    rlens[aln.rlen]++;
    sum_rlens += aln.rlen;
  }
  auto all_sum_rlens = upcxx::reduce_all(sum_rlens, op_fast_add).wait();
  auto all_nalns = upcxx::reduce_all(alns.size(), op_fast_add).wait();
  auto avg_rlen = all_nalns > 0 ? all_sum_rlens / all_nalns : 0;
  int most_common_rlen = avg_rlen;
  int64_t max_count = 0;
  for (auto &rlen : rlens) {
    if (rlen.second > max_count) {
      max_count = rlen.second;
      most_common_rlen = rlen.first;
    }
  }
  SLOG_VERBOSE("Computed unmerged read length as ", most_common_rlen, " with a count of ", max_count, " and average of ", avg_rlen,
               " over ", all_nalns, " alignments\n");
  return most_common_rlen;
}

bool Alns::is_valid() const {
  bool is_valid = true;
  for (auto &aln : alns)
    if (!aln.is_valid()) {
      DBG("Invalid alignment ", aln.to_string(), "\n");
      is_valid = false;
    }
  return is_valid;
}

void Alns::sort_alns() {
  BaseTimer timer(__FILEFUNC__);
  assert(is_valid());
  timer.start();
  // first remove any invalid alignments (i.e without a map)
  size_t invalid = 0;
  int rg = -2;
  auto it = alns.begin();
  while (it != alns.end()) {
    auto &aln = *it;
    if (aln.is_valid()) {
      if (rg == -2) rg = aln.read_group_id;
      if (rg != aln.read_group_id) DIE("Detected different read_groups while sorting: ", rg, " vs ", aln.to_string());
      it++;
    } else {
      DBG_VERBOSE("Removing invalid alignment ", aln.to_string(), "\n");
      if (&aln != &alns.back()) std::swap(aln, alns.back());
      alns.pop_back();
      invalid++;
    }
  }
  // sort the alns by name and then for the read from best score to worst - this is needed in later stages
  sort(alns.begin(), alns.end(), Aln::cmp);
  // now purge any duplicates
  auto start_size = alns.size();
  auto ip = unique(alns.begin(), alns.end());
  alns.resize(distance(alns.begin(), ip));
  timer.stop();
  auto num_dups = start_size - alns.size();
  LOG("Sorted ", alns.size(), " alns and removed ", invalid, " invalid and ", num_dups, " duplicates in ", timer.get_elapsed(),
      " s\n");
}

void Alns::merge_alns(vector<Alns> &subset_alns) {
  // consumes a vector of subset alns and merge-sorts them into this Alns
  vector<alns_t::iterator> iterators;
  auto num = subset_alns.size();
  int rg = -2;
  for (auto &a : subset_alns) {
    a.sort_alns();
    iterators.push_back(a.begin());
    if (!a.empty()) {
      if (rg == -2) rg = a.begin()->read_group_id;
      if (rg != a.begin()->read_group_id)
        DIE("merge_alns merging from different read_groups: ", rg, " vs ", a.begin()->to_string());
    }
  }
  assert(iterators.size() == num);
  bool done = false;
  while (true) {
    int best_idx = num;
    for (int i = 0; i < num; i++) {
      if (iterators[i] == subset_alns[i].end()) continue;
      if (best_idx == num) {
        best_idx = i;
        continue;
      }
      if (Aln::cmp(*iterators[i], *iterators[best_idx])) best_idx = i;
    }
    if (best_idx == num) break;
    Aln &new_aln = *(iterators[best_idx]++);
    add_aln(new_aln);
    new_aln = {};
  }
  subset_alns = {};
}

void Alns::split_by_read_group(std::map<int, Alns> &destinations) {
  for (auto &aln : alns) {
    auto rg = aln.read_group_id;
    auto it = destinations.find(rg);
    if (it == destinations.end()) {
      auto x = destinations.insert({rg, Alns(read_len)});
      assert(x.second);
      it = x.first;
    }
    it->second.alns.emplace_back(std::move(aln));
  }
  clear();
}

void Alns::compute_stats(size_t &num_reads_mapped, size_t &num_bases_mapped) {
  auto get_read_id = [](const string &read_id) {
    int pair_id = get_pair_id(read_id);
    if (pair_id == 0) return make_pair(read_id, 0);
    return make_pair(read_id.substr(0, read_id.length() - 2), pair_id);
  };

  BaseTimer timer(__FILEFUNC__);
  // bitset needs to be defined at compile time and the max paired read length we expect is 450. This means we use 2x the
  // space needed for a read length of 101, with 250 taking 32 and 101 taking 16, and 512 taking 128
  HASH_TABLE<string, bitset<KLIGN_MAX_READ_LENGTH>> mapped_reads(alns.size());
  for (auto &aln : alns) {
    if (aln.cid == -1) continue;
    auto elem = mapped_reads.find(aln.read_id);
    if (elem == mapped_reads.end()) elem = mapped_reads.insert({aln.read_id, bitset<KLIGN_MAX_READ_LENGTH>()}).first;
    for (int i = aln.rstart; i < aln.rstop; i++) elem->second[i] = 1;
  }
  num_reads_mapped = mapped_reads.size();
  num_bases_mapped = 0;
  for (auto &mapped_read : mapped_reads) num_bases_mapped += mapped_read.second.count();

  // FIXME does this crash post-assm???  LOG_MEM("After alns.compute_stats");
}

static int get_insert_size(const Aln &aln1, const Aln &aln2) { return max(aln1.cstop, aln2.cstop) - min(aln1.cstart, aln2.cstart); }

static bool is_proper_pair(const Aln &aln1, const Aln &aln2) {
  if (aln1.cid == aln2.cid && aln1.orient != aln2.orient && get_insert_size(aln1, aln2) < 2000) return true;
  return false;
}

bool Alns::set_pair_info(const string &read_id, vector<size_t> &read1_aln_indexes, vector<size_t> &read2_aln_indexes) {
  auto get_highest_ri = [&alns = this->alns](const vector<size_t> &read_aln_indexes) {
    if (read_aln_indexes.empty()) return (int64_t)-1;
    int64_t highest_ri = -1;
    int highest_score = 0;
    for (int i = 0; i < read_aln_indexes.size(); i++) {
      int64_t ri = read_aln_indexes[i];
      Aln &aln = alns[ri];
      if (aln.score1 > highest_score) {
        highest_score = aln.score1;
        highest_ri = ri;
      }
    }
    return highest_ri;
  };

  auto clear_other_alns = [&alns = this->alns](int64_t highest_ri, vector<size_t> read_aln_indexes) {
    for (auto ri : read_aln_indexes) {
      if (ri != highest_ri) alns[ri].cid = -1;
    }
  };

  // bbmap chooses alns by highest score
  size_t highest_r1 = get_highest_ri(read1_aln_indexes);
  size_t highest_r2 = get_highest_ri(read2_aln_indexes);
  // if the highest scoring pair is not a proper pair, then see if we can find a proper pair with a boosted higher average score
  // bbmap supposedly gives a boost to properly paired alignments, but this doesn't seem to make any difference on arcticsyth
  if (highest_r1 != -1 && highest_r2 != -1 && !is_proper_pair(alns[highest_r1], alns[highest_r2])) {
    // find highest average scoring proper pair
    int max_score1 = alns[highest_r1].score1;
    int max_score2 = alns[highest_r2].score1;
    for (auto aln_r1 : read1_aln_indexes) {
      Aln &aln1 = alns[aln_r1];
      for (auto aln_r2 : read2_aln_indexes) {
        Aln &aln2 = alns[aln_r2];
        if (!is_proper_pair(aln1, aln2)) continue;
        // boost if this is close to the average insert size
        // FIXME: need to pass in the average insert size here
        double boost = max(0.0, 0.5 - abs((double)get_insert_size(aln1, aln2) - 270) / 1080);
        int score1 = aln1.score1 + aln2.score1 * boost;
        int score2 = aln2.score1 + aln1.score1 * boost;
        if (score1 > max_score1) {
          highest_r1 = aln_r1;
          max_score1 = score1;
        }
        if (score2 > max_score2) {
          highest_r2 = aln_r2;
          max_score2 = score2;
        }
      }
    }
  }
  if (highest_r1 != -1) {
    if (highest_r2 != -1)
      alns[highest_r1].add_cigar_pair_info((alns[highest_r2].cid), alns[highest_r2].cstart, alns[highest_r2].orient, read_len);
    else
      alns[highest_r1].add_cigar_pair_info(-1, 0, ' ', read_len);
  }
  if (highest_r2 != -1) {
    if (highest_r1 != -1)
      alns[highest_r2].add_cigar_pair_info((alns[highest_r1].cid), alns[highest_r1].cstart, alns[highest_r1].orient, read_len);
    else
      alns[highest_r2].add_cigar_pair_info(-1, 0, ' ', read_len);
  }
  clear_other_alns(highest_r1, read1_aln_indexes);
  clear_other_alns(highest_r2, read2_aln_indexes);
  if (highest_r1 != -1 && highest_r2 != -1) return is_proper_pair(alns[highest_r1], alns[highest_r2]);
  return false;
}

void Alns::select_pairs(size_t &num_proper_pairs) {
  BaseTimer timer(__FILEFUNC__);
  vector<size_t> read1_aln_indexes;
  vector<size_t> read2_aln_indexes;
  string curr_read_id = "";
  num_proper_pairs = 0;
  // chose only one pair of alns per read pair
  for (size_t i = 0; i < alns.size(); i++) {
    Aln &aln = alns[i];
    // bbmap can drop alns for low identity, but note that we calculate identity a bit differently, so the results will be a bit
    // different
    if (aln.identity < POST_ASM_MIN_IDENT) {
      aln.cid = -1;
      continue;
    }
    string read_id = aln.read_id.substr(0, aln.read_id.length() - 2);
    if (i == 0) curr_read_id = read_id;
    if (read_id != curr_read_id) {
      if (set_pair_info(curr_read_id, read1_aln_indexes, read2_aln_indexes)) num_proper_pairs++;
      read1_aln_indexes.clear();
      read2_aln_indexes.clear();
      i--;
      curr_read_id = read_id;
      continue;
    }
    int pair = get_pair_id(aln.read_id);
    switch (pair) {
      case 1: read1_aln_indexes.push_back(i); break;
      case 2: read2_aln_indexes.push_back(i); break;
      default: DIE("Incorrect pair information for read ", aln.read_id, " found '", pair, "'");
    }
  }
  if (!read1_aln_indexes.empty() || !read2_aln_indexes.empty()) {
    if (set_pair_info(curr_read_id, read1_aln_indexes, read2_aln_indexes)) num_proper_pairs++;
  }
  LOG("Selected ", num_proper_pairs, " pairs out of ", alns.size(), " in ", timer.get_elapsed(), " s\n");
}

//
// AlignBlockData
//

AlignBlockData::AlignBlockData(vector<Aln> &_kernel_alns, vector<string> &_ctg_seqs, vector<string> &_read_seqs, int64_t max_clen,
                               int64_t max_rlen)
    : kernel_alns{}
    , ctg_seqs{}
    , read_seqs{}
    , max_clen(max_clen)
    , max_rlen(max_rlen)
    , final_alns() {
  assert(upcxx::master_persona().active_with_caller());
  // may be called from within progress
  // copy/swap/reserve necessary data and configs
  assert(!_kernel_alns.empty());
  assert(_kernel_alns.size() == _ctg_seqs.size());
  assert(_kernel_alns.size() == _read_seqs.size());
  assert(_kernel_alns.size() <= KLIGN_GPU_BLOCK_SIZE);
  assert(max_clen > 0);
  assert(max_rlen > 0);
  size_t batch_sz = std::max(std::max(kernel_alns.size(), _kernel_alns.size()), (size_t)KLIGN_GPU_BLOCK_SIZE);
  kernel_alns.swap(_kernel_alns);
  _kernel_alns.reserve(batch_sz);
  ctg_seqs.swap(_ctg_seqs);
  _ctg_seqs.reserve(batch_sz);
  read_seqs.swap(_read_seqs);
  _read_seqs.reserve(batch_sz);
#ifdef DEBUG
  for (auto &ctg_seq : ctg_seqs) assert(ctg_seq.size() <= max_clen);
  for (auto &read_seq : read_seqs) assert(read_seq.size() <= max_rlen);
#endif
  assert(_ctg_seqs.empty());
  assert(_read_seqs.empty());
  assert(_kernel_alns.empty());
  validate("Construction");
}

AlignBlockData::~AlignBlockData() {
  validate("Destruction");
  if (!kernel_alns.empty())
    DIE("finish() was never called before destrution! ", kernel_alns.size(), " alns remain to be processed\n");
}

void AlignBlockData::clear_inputs() {
  assert(kernel_alns.size() == ctg_seqs.size());
  assert(kernel_alns.size() == read_seqs.size());
  kernel_alns.clear();
  ctg_seqs.clear();
  read_seqs.clear();
}

size_t AlignBlockData::finish(Alns &dest_alns) {
  validate("Finish");
  ctg_seqs.clear();
  read_seqs.clear();
  auto start_sz = dest_alns.size();
  size_t ret = 0;
  if (final_alns.empty()) {
    // kernel_alns have not been added to an Alns yet so add to dest
    DBG("Finishing kernel_alns ", kernel_alns.size(), ", adding to ", (void *)&dest_alns, " with ", dest_alns.size(), "\n");
    for (auto &aln : kernel_alns) {
      if (aln.is_valid()) dest_alns.add_aln(aln);
    }
    ret = dest_alns.size() - start_sz;
    LOG("Finished.  Added ", dest_alns.size() - start_sz, " out of ", kernel_alns.size(), "\n");
    clear_inputs();
  } else {
    // final_alns have been added to an Alns and can simply be appended to dest
    LOG("Appending ", final_alns.size(), " final_alns to ", (void *)&dest_alns, " with ", dest_alns.size(), "\n");
    assert(kernel_alns.empty() && "either kernel_alns or final_alns should be empty when finish is called");
    ret = final_alns.size();
    dest_alns.append(final_alns);
    final_alns.clear();
  }
  return ret;
}

void AlignBlockData::validate(string msg) const { DBG(msg + ": ", (void *)this, " with ", kernel_alns.size(), "\n"); }
