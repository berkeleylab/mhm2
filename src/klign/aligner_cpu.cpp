/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include "aligner_cpu.hpp"
#include "contigs.hpp"

#ifdef __PPC64__  // FIXME remove after solving Issues #60 #35 #49
#define NO_KLIGN_CPU_WORK_STEAL
#endif

using namespace std;
using namespace upcxx;
using namespace upcxx_utils;

int get_cigar_length(const string &cigar) {
  // check that cigar string length is the same as the sequence, but only if the sequence is included
  int base_count = 0;
  string num = "";
  for (char c : cigar) {
    switch (c) {
      case 'M':
      case 'S':
      case '=':
      case 'X':
      case 'I':
        base_count += stoi(num);
        num = "";
        break;
      case 'D':
        // base_count -= stoi(num);
        num = "";
        break;
      default:
        if (!isdigit(c)) DIE("Invalid char detected in cigar: '", c, "'");
        num += c;
        break;
    }
  }
  return base_count;
}

CPUAligner::CPUAligner(bool report_cigar, bool use_blastn_scores)
    : ssw_aligner()
    , ssw_filter() {
  // aligner construction: SSW internal defaults are 2 2 3 1
  ssw_aligner.Clear();
  if (!ssw_aligner.ReBuild(to_string(use_blastn_scores ? BLASTN_ALN_SCORES : ALTERNATE_ALN_SCORES)))
    SDIE("Failed to set aln scores");
  ssw_filter.report_cigar = report_cigar;
  SLOG_VERBOSE("Alignment scoring parameters: match ", (int)ssw_aligner.get_match_score(), " mismatch ",
               (int)ssw_aligner.get_mismatch_penalty(), " gap open ", (int)ssw_aligner.get_gap_opening_penalty(), " gap extend ",
               (int)ssw_aligner.get_gap_extending_penalty(), " ambiguity ", (int)ssw_aligner.get_ambiguity_penalty(), "\n");
}

CPUAligner::CPUAligner(const CPUAligner &copy)
    : ssw_aligner(copy.ssw_aligner)
    , ssw_filter(copy.ssw_filter) {}

void CPUAligner::ssw_align_read(StripedSmithWaterman::Aligner &ssw_aligner, StripedSmithWaterman::Filter &ssw_filter,
                                Alns *dest_alns, Aln &aln, const string_view &cseq, const string_view &rseq, int read_group_id) {
  // debugging with these alignments
  // missing from mhm:
  // CP000510.1-101195/2	Contig6043	96.667	150	5	0	1	150	109	258	1.44e-66	249
  // probably too close to this one, which is found by mhm:
  // CP000510.1-101195/2	Contig6043	92.708	96	7	0	6	101	1	96	1.54e-34	142

  assert(aln.clen >= cseq.length() && "contig seq is contained within the greater contig");
  assert(aln.rlen >= rseq.length() && "read seq is contained with the greater read");

  StripedSmithWaterman::Alignment ssw_aln;

  // align query, ref, reflen
  ssw_aligner.Align(rseq.data(), rseq.length(), cseq.data(), cseq.length(), ssw_filter, &ssw_aln,
                    max((int)(rseq.length() / 2), 15));
  aln.set(ssw_aln.ref_begin, ssw_aln.ref_end, ssw_aln.query_begin, ssw_aln.query_end, ssw_aln.sw_score, ssw_aln.sw_score_next_best,
          ssw_aln.mismatches, read_group_id);
  if (ssw_filter.report_cigar) aln.set_sam_string(ssw_aln.cigar_string);
  if (aln.is_valid()) dest_alns->add_aln(aln);
  /*
  if (ssw_filter.report_cigar && (aln.read_id == "CP000510.1-101195/2")) {
    cout << KLGREEN << "aln: " << aln.to_string() << KNORM << endl;
    cout << KLGREEN << "rseq " << rseq << KNORM << endl;
    cout << KLGREEN << "cseq " << cseq << KNORM << endl;
    cout << KLGREEN << "ssw aln: ref begin " << ssw_aln.ref_begin << " ref_end " << ssw_aln.ref_end << " query begin "
         << ssw_aln.query_begin << " query end " << ssw_aln.query_end << KNORM << endl;
  }*/
}

void CPUAligner::ssw_align_read(Alns *dest_alns, Aln &aln, const string &cseq, const string &rseq, int read_group_id) {
  ssw_align_read(ssw_aligner, ssw_filter, dest_alns, aln, cseq, rseq, read_group_id);
}

upcxx::future<ShAlignBlockData> CPUAligner::ssw_align_block(ShAlignBlockData aln_block_data, IntermittentTimer &aln_kernel_timer) {
  assert(upcxx::initialized() && upcxx::master_persona().active_with_caller() && "Called from master persona");
  // may be called from within progress
  AsyncTimer t("ssw_align_block (thread)");
  assert(aln_block_data);
  auto sh_cpu_aligner = make_shared<CPUAligner>(*this);  // make a copy for thread safety
  auto lambda = [sh_cpu_aligner, aln_block_data, t]() {
    t.start();
    assert(!aln_block_data->kernel_alns.empty());
    DBG("Starting _ssw_align_block of ", aln_block_data->kernel_alns.size(), "\n");
    auto &final_alns = aln_block_data->final_alns;
    for (int i = 0; i < aln_block_data->kernel_alns.size(); i++) {
      Aln &aln = aln_block_data->kernel_alns[i];
      string &cseq = aln_block_data->ctg_seqs[i];
      string &rseq = aln_block_data->read_seqs[i];
      DBG_VERBOSE("aligning ", i, " of ", aln_block_data->kernel_alns.size(), " ", aln.read_id, "\n");
      sh_cpu_aligner->ssw_align_read(&final_alns, aln, cseq, rseq, aln.read_group_id);
    }
    aln_block_data->clear_inputs();
    t.stop();
  };

  future<> fut = make_future();
#ifndef KLIGN_NO_THREAD_POOL
  DBG("Processing lambda abd.use_count=", aln_block_data.use_count(), "\n");
  fut = upcxx_utils::execute_serially_in_thread_pool(lambda);
#else
  progress();
  lambda();
  progress();
#endif
  return fut.then([aln_block_data, t, &aln_kernel_timer, sh_cpu_aligner]() {
    aln_kernel_timer += t;
    return aln_block_data;
  });
}
