/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include <fcntl.h>
#include <math.h>
#include <stdarg.h>
#include <unistd.h>
#include <string_view>
#include <unordered_set>
#include <deque>

#include <algorithm>
#include <forward_list>
#include <iterator>
#include <iostream>
#include <random>
#include <upcxx/upcxx.hpp>

#include "aligner.hpp"
#include "klign.hpp"
#include "kmer.hpp"
#include "contigs.hpp"
#include "ssw.hpp"
#include "utils.hpp"
#include "zstr.hpp"
#include "aligner_cpu.hpp"

#include "upcxx_utils/limit_outstanding.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/mem_profile.hpp"
#include "upcxx_utils/progress_bar.hpp"
#include "upcxx_utils/thread_pool.hpp"
#include "upcxx_utils/three_tier_aggr_store.hpp"
#include "upcxx_utils/timers.hpp"

using namespace std;
using namespace upcxx;
using namespace upcxx_utils;

template <int MAX_K>
class KmerCtgDHT {
  using dist_kmer_ctg_t = dist_object<KmerCtg<MAX_K>>;
  dist_kmer_ctg_t dist_kmer_ctg;

  using local_kmer_map_t = typename KmerCtg<MAX_K>::local_kmer_map_t;
  shared_ptr<ThreeTierAggrStore<KmerAndCtgLoc<MAX_K>>> sh_kmer_store;
  ThreeTierAggrStore<KmerAndCtgLoc<MAX_K>> &kmer_store;

 public:
  static size_t &get_kmer_seed_lookups() {
    static size_t _ = 0;
    return _;
  };
  static size_t &get_unique_kmer_seed_lookups() {
    static size_t _ = 0;
    return _;
  }
  unsigned kmer_len;
  bool allow_multi_kmers = false;

  KmerCtgDHT(int max_store_size, int max_rpcs_in_flight, bool allow_multi_kmers, uint64_t num_contig_kmers = 0)
      : dist_kmer_ctg(KmerCtg<MAX_K>{})
      , sh_kmer_store(make_shared<ThreeTierAggrStore<KmerAndCtgLoc<MAX_K>>>())
      , kmer_store(*sh_kmer_store)
      , allow_multi_kmers(allow_multi_kmers) {
    if (allow_multi_kmers) SLOG_VERBOSE("Finding multiple kmer to contig mappings\n");
    kmer_len = Kmer<MAX_K>::get_k();
    LOG("Constructing KmerCtgDHT for k=", kmer_len, "\n");
    kmer_store.set_size("insert ctg seeds", max_store_size, max_rpcs_in_flight, num_contig_kmers);
    kmer_store.set_update_func([this, allow_multi_kmers](KmerAndCtgLoc<MAX_K> kmer_and_ctg_loc) {
      auto &kmer_map = this->dist_kmer_ctg->kmer_map;
      auto &num_dropped_seed_to_ctgs = this->dist_kmer_ctg->num_dropped_seed_to_ctgs;
      CtgLoc ctg_loc = kmer_and_ctg_loc.ctg_loc;
      auto it = kmer_map.find(kmer_and_ctg_loc.kmer);
      if (it == kmer_map.end()) {
        // always insert if not found
        SingleOrMultipleCtgLocs cl;
        if (allow_multi_kmers) {
          CtgLocVector clv{ctg_loc};
          cl = std::move(clv);
        } else {
          cl = ctg_loc;
        }
        it = kmer_map.insert({kmer_and_ctg_loc.kmer, cl}).first;
      } else {
        if (allow_multi_kmers) {
          auto &clv = std::get<CtgLocVector>(it->second);
          // only add if we haven't hit the threshold to prevent excess memory usage and load imbalance here
          if (clv.size() < KLIGN_MAX_CTGS_PER_KMER) clv.push_back(ctg_loc);
        } else {
          // there are conflicts so don't allow any kmer mappings. This improves the assembly when scaffolding k is smaller than
          // the final contigging k, e.g. sk=33
          auto &cl = std::get<CtgLoc>(it->second);
          cl.cid = -1;  // signal invalid
          num_dropped_seed_to_ctgs++;
        }
      }
    });
  }  // construct KmerCtgDHT

  // releases the data but keeps the aggregate stores - used for post-asm subsets
  void release_data() {
    assert(upcxx::master_persona().active_with_caller());
    assert(!upcxx::in_progress());
    LOG("release_data: ", dist_kmer_ctg->empty(), "\n");
    // ensure all use of this data is completed
    Timings::wait_pending();
    KmerCtg<MAX_K> empty;
    dist_kmer_ctg->swap(empty);
  }

  // releases everything.  Called in destructor
  void clear() {
    assert(!upcxx::in_progress());
    assert(upcxx::master_persona().active_with_caller());
    LOG("aggregated kmer seed lookups ",
        perc_str(get_kmer_seed_lookups() - get_unique_kmer_seed_lookups(), get_kmer_seed_lookups()), ", total ",
        get_kmer_seed_lookups(), "\n");
    release_data();
    clear_stores();
    get_kmer_seed_lookups() = 0;
    get_unique_kmer_seed_lookups() = 0;
  }

  void clear_stores() {
    assert(upcxx::master_persona().active_with_caller());
    LOG("clear stores: ", sh_kmer_store, "\n");
    // frees up the kmer store which must be freed in the primary scope
    if (sh_kmer_store) {
      assert(!upcxx::in_progress());
      kmer_store.clear();  // implicit barriers
      sh_kmer_store.reset();
    }
  }

  ~KmerCtgDHT() {
    assert(!upcxx::in_progress());
    assert(upcxx::master_persona().active_with_caller());
    // may be called from within progress only if clear_stores() has already been called outside of progress
    LOG("Destroying KmerCtgDHT in_progress:", upcxx::in_progress(), "\n");
    clear();
  }

  intrank_t get_target_rank(const Kmer<MAX_K> &kmer) const { return std::hash<Kmer<MAX_K>>{}(kmer) % rank_n(); }

  future<size_t> fut_get_num_kmers(bool all = false) {
    auto &pr = Timings::get_promise_reduce();
    auto &kmer_map = dist_kmer_ctg->kmer_map;
    if (!all) return pr.reduce_one(kmer_map.size(), op_fast_add, 0);
    return pr.reduce_all(kmer_map.size(), op_fast_add);
  }

  future<int64_t> fut_get_num_dropped_seed_to_ctgs(bool all = false) {
    auto &pr = Timings::get_promise_reduce();
    auto &num = dist_kmer_ctg->num_dropped_seed_to_ctgs;
    if (!all) return pr.reduce_one(num, op_fast_add, 0);
    return pr.reduce_all(num, op_fast_add);
  }

  void add_kmer(const Kmer<MAX_K> &kmer_fw, CtgLoc &ctg_loc) {
    Kmer<MAX_K> kmer_rc = kmer_fw.revcomp();
    ctg_loc.is_rc = false;
    const Kmer<MAX_K> *kmer_lc = &kmer_fw;
    if (kmer_rc < kmer_fw) {
      kmer_lc = &kmer_rc;
      ctg_loc.is_rc = true;
    }
    KmerAndCtgLoc<MAX_K> kmer_and_ctg_loc = {*kmer_lc, ctg_loc};
    kmer_store.update(get_target_rank(*kmer_lc), kmer_and_ctg_loc);
  }  // add_kmer

  void flush_add_kmers() {
    assert(upcxx::master_persona().active_with_caller());
    assert(!upcxx::in_progress());
    BarrierTimer timer(__FILEFUNC__, false);  // barrier on exit, not entrance
    kmer_store.flush_updates();
    size_t max_ctgs = 0;
    // determine max number of ctgs mapped to by a single kmer
    for (auto &elem : dist_kmer_ctg->kmer_map) {
      auto num_ctgs_for_kmer =
          allow_multi_kmers ? std::get<CtgLocVector>(elem.second).size() : (std::get<CtgLoc>(elem.second).cid == -1 ? 0 : 1);
      max_ctgs = ::max(max_ctgs, num_ctgs_for_kmer);
    }
    auto &pr = Timings::get_promise_reduce();
    auto fut_reduce = when_all(pr.reduce_one(max_ctgs, op_fast_max, 0));
    auto fut_report = fut_reduce.then([](auto all_max_ctgs) {
      if (all_max_ctgs > 1) SLOG_VERBOSE("Max contigs mapped by a single kmer: ", all_max_ctgs, "\n");
    });
    Timings::set_pending(fut_report);
  }  // flush_add_kmers

  future<vector<CtgLocAndKmerIdx>> get_ctgs_with_kmers(int target_rank, const vector<Kmer<MAX_K>> &kmers, KlignTimers &timers) {
    assert(upcxx::master_persona().active_with_caller());
    assert(!upcxx::in_progress());
    DBG_VERBOSE("Sending request for ", kmers.size(), " to ", target_rank, "\n");
    static size_t _count_outer = 0;
    auto count_outer = _count_outer++;
    bool log_outer = rand() % 1000 == 0;

    AsyncTimer t_o(make_string("get_ctgs_with_kmers outer tgt=", target_rank, " sz=", kmers.size(), " count=", count_outer));
    t_o.start();
    auto fut_rpc = rpc(
        target_rank,
        [](const vector<Kmer<MAX_K>> &kmers, dist_kmer_ctg_t &dist_kmer_ctg, bool allow_multi_kmers) {
          BaseTimer t_i("with get_ctgs_with_kmers rpc inner");
          t_i.start();
          static int large_threshold = allow_multi_kmers ? 20 : 4000;
#ifdef DEBUG
          // to test running in a thread
          large_threshold = 2;
#endif
          static int num_calls = 0, large_num_calls = 0;
          num_calls++;
          if (kmers.size() >= large_threshold) large_num_calls++;
          auto inner_lambda = [&, num_calls = num_calls, large_num_calls = large_num_calls, large_threshold = large_threshold,
                               allow_multi_kmers]() -> vector<CtgLocAndKmerIdx> {
            BaseTimer t_l("with get_ctgs_with_kmers rpc lambda");
            t_l.start();
            vector<CtgLocAndKmerIdx> ctg_locs;
            ctg_locs.reserve(kmers.size() * (allow_multi_kmers ? 20 : 2));
            for (int i = 0; i < kmers.size(); i++) {
              auto &kmer = kmers[i];
              assert(kmer.is_least());
              assert(kmer.is_valid());
              const auto it = dist_kmer_ctg->kmer_map.find(kmer);
              KmerCtgDHT::get_kmer_seed_lookups()++;
              if (it == dist_kmer_ctg->kmer_map.end()) continue;
              if (allow_multi_kmers) {
                auto &clv = std::get<CtgLocVector>(it->second);
                if (clv.size() == 1) KmerCtgDHT::get_unique_kmer_seed_lookups()++;
                for (auto &ctg_loc : clv) ctg_locs.push_back({ctg_loc, i});
              } else {
                auto &cl = std::get<CtgLoc>(it->second);
                if (cl.cid != -1) {
                  KmerCtgDHT::get_unique_kmer_seed_lookups()++;
                  ctg_locs.push_back({cl, i});
                }
              }
            }
            t_l.stop();
            if (kmers.size() >= large_threshold) KlignTimers::get_ctgs_with_kmers_tp_t() += t_l;
            // For Issue137 tracking
            if (ctg_locs.size() * sizeof(CtgLocAndKmerIdx) > KLIGN_MAX_RPC_MESSAGE_SIZE || rand() % 1000 == 0)
              LOG("Received ", kmers.size(), " kmers ", get_size_str(kmers.size() * sizeof(Kmer<MAX_K>)), " responding with ",
                  ctg_locs.size(), " ctg_locs ", get_size_str(ctg_locs.size() * sizeof(CtgLocAndKmerIdx)), " in ",
                  t_l.get_elapsed(), " s num_calls=", num_calls, " large_num_calls=", large_num_calls, "\n");
            return ctg_locs;
          };  // inner_lambda

          // run large queries in a separate thread to keep attention
          auto fut = (kmers.size() < large_threshold) ? make_future(inner_lambda()) : execute_in_thread_pool(inner_lambda);
          t_i.stop();
          KlignTimers::get_ctgs_with_kmers_master_t() += t_i;
          return fut;
        },
        kmers, dist_kmer_ctg, allow_multi_kmers);

    // maintain attention and to not overload network
    auto fut_limit = fut_rpc.then([=, &timers](auto &ignored) {
      t_o.stop(log_outer);
      timers.get_ctgs_with_kmers_rt_t += t_o;
      DBG("get_ctgs_with_kmers rpc completed round trip in ", t_o.get_elapsed(), " s\n");
    });
    limit_outstanding_futures(fut_limit).wait();
    progress();

    return fut_rpc;
  }  // get_ctgs_with_kmers

  void dump_ctg_kmers() {
    BarrierTimer timer(__FILEFUNC__, false);  // barrier on exit not entrance
    string dump_fname = "ctg_kmers-" + to_string(kmer_len) + ".txt.gz";
    get_rank_path(dump_fname, rank_me());
    zstr::ofstream dump_file(dump_fname);
    ostringstream out_buf;
    auto &kmer_map = dist_kmer_ctg->kmer_map;
    ProgressBar progbar(kmer_map.size(), "Dumping kmers to " + dump_fname);
    int64_t i = 0;
    for (auto &elem : kmer_map) {
      vector<CtgLoc> v;
      if (!allow_multi_kmers) {
        auto &cl = std::get<CtgLoc>(elem.second);
        if (cl.cid != -1) v.push_back(cl);
      }
      for (auto &ctg_loc : allow_multi_kmers ? std::get<CtgLocVector>(elem.second) : v) {
        out_buf << elem.first << " " << ctg_loc.cid << " " << ctg_loc.clen << " " << ctg_loc.depth << " " << ctg_loc.pos << " "
                << ctg_loc.is_rc << "\n";
        i++;
        if (!(i % 1000)) {
          dump_file << out_buf.str();
          out_buf = ostringstream();
        }
      }
      progbar.update();
    }
    if (!out_buf.str().empty()) dump_file << out_buf.str();
    dump_file.close();
    auto fut_rep = when_all(progbar.set_done(), this->fut_get_num_kmers()).then([](auto tot_num_kmers) {
      SLOG_VERBOSE("Dumped ", tot_num_kmers, " kmers\n");
    });
    Timings::set_pending(fut_rep);
  }  // dump_ctg_kmers

  void build(Contigs &ctgs, unsigned min_ctg_len) {
    BarrierTimer timer(__FILEFUNC__);
    int64_t num_kmers = 0;
    ProgressBar progbar(ctgs.size(), "Extracting seeds from contigs");
    // estimate and reserve room in the local map to avoid excessive reallocations
    int64_t est_num_kmers = 0;
    size_t ctg_seq_lengths = 0, min_len_ctgs = 0;
    for (auto it = ctgs.begin(); it != ctgs.end(); ++it) {
      auto ctg = it;
      auto len = ctg->seq.length();
      ctg_seq_lengths += len;
      min_len_ctgs++;
      if (len < min_ctg_len) continue;
      est_num_kmers += len - kmer_len + 1;
    }
    auto msm_kmers = upcxx_utils::min_sum_max_reduce_all(est_num_kmers).wait();
    est_num_kmers = msm_kmers.sum;
    auto msm_ctgs = upcxx_utils::min_sum_max_reduce_one(min_len_ctgs).wait();

    auto tot_ctg_lengths = reduce_one(ctg_seq_lengths, op_fast_add, 0).wait();
    auto msm_num_ctgs = upcxx_utils::min_sum_max_reduce_one(min_len_ctgs).wait();
    auto tot_num_ctgs = msm_num_ctgs.sum;

    auto my_reserve = 1.2 * est_num_kmers / rank_n() + 2000;  // 120% to keep the map fast
    SLOG_VERBOSE("Estimated ", est_num_kmers, " contig ", kmer_len, "-kmers for ", tot_num_ctgs, " contigs with total len ",
                 tot_ctg_lengths, " each above ", min_ctg_len, ".\n");
    SLOG_VERBOSE("ctg-kmers: ", msm_kmers.to_string(), "\n");
    SLOG_VERBOSE("num_ctgs: ", msm_num_ctgs.to_string(), "\n");
    auto my_required_mem = my_reserve * (sizeof(typename local_kmer_map_t::value_type) +
                                         sizeof(CtgLoc));  // 1 map key/value entry + 1 CtgLoc within the vector
    LOG("Reserving ", my_reserve, " for my entries at approx ", get_size_str(my_required_mem * upcxx::local_team().rank_n()),
        " per node for the local hashtable\n");
    LOG_MEM("Before reserving entries for ctg kmers");
    dist_kmer_ctg->reserve_kmers(my_reserve);
    dist_kmer_ctg->reserve_ctgs(ctgs.size());

    LOG_MEM("After reserving entries for ctg kmers");
    {  // necessary for attention!
      BarrierTimer timer1("Barrier after memory allocated on all nodes");
    }
    vector<Kmer<MAX_K>> kmers;
    for (auto it = ctgs.begin(); it != ctgs.end(); ++it) {
      auto ctg = it;
      progbar.update();
      if (ctg->seq.length() < min_ctg_len) continue;
      global_ptr<char> seq_gptr = dist_kmer_ctg->add_ctg_seq(ctg->seq);
      CtgLoc ctg_loc = {.cid = ctg->id, .seq_gptr = seq_gptr, .clen = (uint32_t)ctg->seq.length(), .depth = (float)ctg->depth};
      Kmer<MAX_K>::get_kmers(kmer_len, string_view(ctg->seq.data(), ctg->seq.size()), kmers, true);
      num_kmers += kmers.size();
      for (unsigned i = 0; i < kmers.size(); i++) {
        ctg_loc.pos = i;
        if (ctg_loc.pos >= ctg_loc.clen) DIE("ctg_loc.pos ", ctg_loc.pos, " ctg_loc.clen ", ctg_loc.clen);
        if (!kmers[i].is_valid()) continue;
        add_kmer(kmers[i], ctg_loc);
      }
      progress();
    }
    flush_add_kmers();
    auto fut = progbar.set_done();
    auto &pr = Timings::get_promise_reduce();

    auto fut_reduce =
        when_all(fut, pr.reduce_one(num_kmers, op_fast_add, 0), fut_get_num_kmers(), fut_get_num_dropped_seed_to_ctgs());

    auto fut_report = fut_reduce.then(
        [=, sz = this->dist_kmer_ctg->kmer_map.size()](auto tot_num_kmers, auto num_kmers_in_ht, auto num_dropped_seed_to_ctgs) {
          LOG("Estimated room for ", my_reserve, " my final count ", sz, "\n");
          SLOG_VERBOSE("Total contigs >= ", min_ctg_len, ": ", tot_num_ctgs, " seq_length: ", tot_ctg_lengths, "\n");
          SLOG_VERBOSE("Processed ", tot_num_kmers, " seeds from contigs, added ", num_kmers_in_ht, "\n");

          if (num_dropped_seed_to_ctgs)
            SLOG_VERBOSE("For k = ", kmer_len, " dropped ", num_dropped_seed_to_ctgs, " non-unique seed-to-contig mappings (",
                         setprecision(2), fixed, (100.0 * num_dropped_seed_to_ctgs / tot_num_kmers), "%)\n");
        });
    Timings::set_pending(fut_report);
  }  // build

  void purge_high_count_seeds() {
    BarrierTimer timer(__FILEFUNC__);
    // FIXME: Hack for Issue137 to be fixed robustly later
    size_t num_kmers = 0;
    size_t num_unique_kmers = 0;
    size_t num_purged = 0;
    size_t num_ctg_locs_purged = 0;
    size_t num_resized = 0;
    size_t num_ctg_locs_resized = 0;
    size_t max_ctg_locs = 0;
    double total_depth = 0.0;
    double total_purged_depth = 0.0;
    double max_depth = 0.0;
    vector<Kmer<MAX_K>> to_be_purged;
    auto &kmer_map = dist_kmer_ctg->kmer_map;
    for (auto &elem : kmer_map) {
      auto &[kmer, ctg_locs] = elem;
      num_kmers++;
      double depth = 0.0;
      if (allow_multi_kmers) {
        auto &clv = std::get<CtgLocVector>(ctg_locs);
        if (clv.size() == 1) num_unique_kmers++;
        for (auto &ctg_loc : clv) {
          depth += ctg_loc.depth;
        }
        max_ctg_locs = max(max_ctg_locs, clv.size());
      } else {
        auto &cl = std::get<CtgLoc>(ctg_locs);
        if (cl.cid != -1) {
          depth += cl.depth;
          num_unique_kmers++;
        }
        max_ctg_locs = max(max_ctg_locs, (size_t)(cl.cid == -1 ? 0 : 1));
      }
      max_depth = max(max_depth, depth);
      total_depth += depth;
      // we should never actually exceed this threshold because we limit the number of insertions
      if (allow_multi_kmers && (KLIGN_MAX_CTGS_PER_KMER > 0 || KLIGN_LIMIT_CTGS_HITS_PER_KMER > 0)) {
        auto &clv = std::get<CtgLocVector>(ctg_locs);
        if (KLIGN_MAX_CTGS_PER_KMER > 0 && clv.size() >= KLIGN_MAX_CTGS_PER_KMER) {
          DBG("Removing kmer with ", clv.size(), " ctg_loc hits aggregated depth=", depth, ": ", kmer.to_string(), "\n");
          to_be_purged.push_back(kmer);
          num_purged++;
          num_ctg_locs_purged += clv.size();
          total_purged_depth += depth;
        } else if (KLIGN_LIMIT_CTGS_HITS_PER_KMER > 0 && clv.size() > KLIGN_LIMIT_CTGS_HITS_PER_KMER) {
          std::sort(clv.begin(), clv.end(), CtgLoc::cmp);
          DBG("Resizing ctg_locs from ", clv.size(), " to ", KLIGN_LIMIT_CTGS_HITS_PER_KMER, " front=", clv.front().to_string(),
              ", first purged=", clv[KLIGN_LIMIT_CTGS_HITS_PER_KMER].to_string(), "\n");
          num_resized++;
          num_ctg_locs_resized += clv.size() - KLIGN_LIMIT_CTGS_HITS_PER_KMER;
          clv.resize(KLIGN_LIMIT_CTGS_HITS_PER_KMER);
        }
      }
    }

    for (auto &kmer : to_be_purged) {
      auto it = kmer_map.find(kmer);
      if (it == kmer_map.end()) DIE("Could not find kmer with too many ctg_locs!");
      kmer_map.erase(it);
    }

    auto &pr = Timings::get_promise_reduce();
    auto fut_reduce = when_all(pr.reduce_one(num_purged, op_fast_add, 0), pr.reduce_one(num_ctg_locs_purged, op_fast_add, 0),
                               pr.reduce_one(num_resized, op_fast_add, 0), pr.reduce_one(num_ctg_locs_resized, op_fast_add, 0),
                               pr.reduce_one(max_ctg_locs, op_fast_max, 0), pr.reduce_one(total_depth, op_fast_add, 0),
                               pr.reduce_one(total_purged_depth, op_fast_add, 0), pr.msm_reduce_one(kmer_map.size(), 0),
                               pr.msm_reduce_one(num_purged, 0), pr.msm_reduce_one(max_ctg_locs, 0),
                               pr.msm_reduce_one(total_purged_depth), pr.msm_reduce_one(max_depth, 0),
                               pr.reduce_one(num_kmers, op_fast_add, 0), pr.reduce_one(num_unique_kmers, op_fast_add, 0));
    auto fut_report =
        fut_reduce.then([=](auto all_num_purged, auto all_ctg_locs_purged, auto all_num_resized, auto all_ctg_locs_resized,
                            auto global_max_ctg_locs, auto all_total_depth, auto all_total_purged_depth, auto msm_kmers_remaining,
                            auto msm_kmers_purged, auto msm_max_ctg_locs, auto msm_purged_depth, auto msm_max_depth,
                            auto total_num_kmers, auto total_num_unique_kmers) {
          LOG("Purge of high count ctg ", Kmer<MAX_K>::get_k(), "-mer seeds: num_purged=", num_purged,
              " num_ctg_locs_purged=", num_ctg_locs_purged, " num_resized=", num_resized,
              " num_ctg_locs_resized=", num_ctg_locs_resized, " max_ctg_locs=", max_ctg_locs, " total_depth=", total_depth,
              " total_purged_depth=", total_purged_depth, "\n");
          SLOG_VERBOSE("All purged ", all_num_purged, " high count ctg kmer seeds.  total_ctg_locs_purged=", all_ctg_locs_purged,
                       " max_ctg_locs=", global_max_ctg_locs, " all_total_depth=", all_total_depth,
                       " all_purged_depth=", all_total_purged_depth, " seeds_remaining=", msm_kmers_remaining.to_string(),
                       " seeds_purged=", msm_kmers_purged.to_string(), " max_ctg_locs=", msm_max_ctg_locs.to_string(),
                       " purged_depth=", msm_purged_depth.to_string(), " max_depth=", msm_max_depth.to_string(),
                       " unique_kmers=", perc_str(total_num_unique_kmers, total_num_kmers), "\n");
        });
    Timings::set_pending(fut_report);
  }  // purge_high_count_seeds

};  // class KmerCtgDHT

template <int MAX_K>
static upcxx::future<> fetch_ctg_maps_for_target(int target_rank, KmerCtgDHT<MAX_K> &kmer_ctg_dht,
                                                 KmersReadsBuffer<MAX_K> &kmers_reads_buffer, int64_t &num_alns,
                                                 int64_t &num_excess_alns_reads, int64_t &bytes_sent, int64_t &bytes_received,
                                                 int64_t &max_bytes_sent, int64_t &max_bytes_received, int64_t &num_rpcs,
                                                 KlignTimers &timers) {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  assert(kmers_reads_buffer.size());
  int64_t sent_msg_size = (sizeof(Kmer<MAX_K>) * kmers_reads_buffer.kmers.size());
  bytes_sent += sent_msg_size;
  max_bytes_sent = std::max(max_bytes_sent, sent_msg_size);
  num_rpcs++;
  // move and consume kmers_read_buffer.  Keep scope until the future completes.
  auto sh_krb = make_shared<KmersReadsBuffer<MAX_K>>();
  std::swap(*sh_krb, kmers_reads_buffer);
  static int count = 0;
  auto fut_get_ctgs = kmer_ctg_dht.get_ctgs_with_kmers(target_rank, sh_krb->kmers, timers);
  // this lambda is NOT thread safe!!
  auto process_ctgs_with_kmers = [count = count, target_rank, &kmers_reads_buffer = *sh_krb, sh_krb, &num_alns,
                                  &num_excess_alns_reads, &bytes_received,
                                  &max_bytes_received](const vector<CtgLocAndKmerIdx> &ctg_locs_and_kmers_idx) {
    DBG("process_ctgs_with_kmers ", count, " for ", target_rank, " with ", kmers_reads_buffer.size(), " kmer read buffer and ",
        ctg_locs_and_kmers_idx.size(), ": ", (void *)&ctg_locs_and_kmers_idx, "\n");
    int64_t received_msg_size = (sizeof(CtgLocAndKmerIdx) * ctg_locs_and_kmers_idx.size());
    bytes_received += received_msg_size;
    max_bytes_received = std::max(max_bytes_received, received_msg_size);
    int kmer_len = Kmer<MAX_K>::get_k();
    auto rr_size = kmers_reads_buffer.read_records.size();
    // iterate through the vector of ctg locations, each one is associated with an index in the read list
    for (int i = 0; i < ctg_locs_and_kmers_idx.size(); i++) {
      auto &ctg_loc = ctg_locs_and_kmers_idx[i].ctg_loc;
      auto kmer_idx = ctg_locs_and_kmers_idx[i].kmer_i;
      assert(kmer_idx < rr_size && kmer_idx >= 0);
      auto read_record = kmers_reads_buffer.read_records[kmer_idx].read_record;
      auto rlen = read_record->rlen;
      auto pos_in_read = kmers_reads_buffer.read_records[kmer_idx].read_offset;
      auto read_is_rc = kmers_reads_buffer.read_records[kmer_idx].is_rc;
      assert(read_record->is_valid());
      if (KLIGN_MAX_ALNS_PER_READ && read_record->aligned_ctgs_map.size() >= KLIGN_MAX_ALNS_PER_READ) {
        // too many mappings for this read, stop adding to it
        num_excess_alns_reads++;
        continue;
      }
      auto it = read_record->aligned_ctgs_map.find(ctg_loc.cid);
      int new_pos_in_read = (ctg_loc.is_rc == read_is_rc ? pos_in_read : rlen - (kmer_len + pos_in_read));
      assert(new_pos_in_read >= 0);
      if (ctg_loc.pos >= ctg_loc.clen) DIE("ctg_loc.pos ", ctg_loc.pos, " ctg_loc.clen ", ctg_loc.clen);
      auto [cstart, rstart, overlap_len] =
          KlignAligner::get_start_positions(kmer_len, ctg_loc.clen, ctg_loc.pos, new_pos_in_read, rlen);
      if (it == read_record->aligned_ctgs_map.end()) {
        auto [it2, inserted] =
            read_record->aligned_ctgs_map.insert({ctg_loc.cid, {ctg_loc.cid, ctg_loc.seq_gptr, ctg_loc.clen, ctg_loc.depth, {}}});
        assert(inserted);
        it = it2;
        assert(!read_record->aligned_ctgs_map.empty());
        assert(it != read_record->aligned_ctgs_map.end());
      }

      it->second.locs.push_back({ctg_loc.pos, ctg_loc.is_rc, cstart, pos_in_read, read_is_rc});
      num_alns++;
    }
  };  // process_ctgs_with_kmers lambda

  auto fut_rpc_returned = fut_get_ctgs.then([count = count, process_ctgs_with_kmers, target_rank, sh_krb](auto &x) {
    assert(upcxx::master_persona().active_with_caller());
    DBG("Then ", count, " for ", target_rank, " with ", sh_krb->size(), " kmer read buffer and ", x.size(),
        " ctg_locs_and_kmers: ", (void *)&x, "\n");
#ifndef KLIGN_NO_THREAD_POOL
    // explicitly move x for delayed execution
    auto sh_cwk = make_shared<vector<CtgLocAndKmerIdx>>(std::move(x));
    return upcxx_utils::execute_serially_in_thread_pool(
               [process_ctgs_with_kmers, sh_cwk, sh_krb]() { process_ctgs_with_kmers(*sh_cwk); })
        .then(  // ensure destructors happen within master persona
            [sh_cwk]() {});
#else
    process_ctgs_with_kmers(x);
    return make_future();
#endif
  });
  fut_rpc_returned = fut_rpc_returned.then([sh_krb]() {
    // ensure destructors happen within master persona
  });
  count++;
  return fut_rpc_returned;
};  // fetch_ctg_maps_for_target

template <int MAX_K>
size_t allocate_kmers_reads_buffers(vector<KmersReadsBuffer<MAX_K>> &kmers_reads_buffers) {
  // necessary especially in post-asm because this can be large and cause inattention while communication is buffering
  size_t max_rdvz_buffer_size = KLIGN_MAX_RPC_MESSAGE_SIZE / ::max(sizeof(Kmer<MAX_K>), sizeof(CtgLocAndKmerIdx));

  // further limit private memory buffering (per rank) to 10% of memory
  size_t max_mem = KLIGN_MAX_MEM_PCT * upcxx_utils::get_max_free_mem_per_rank() / 100;
  size_t max_mem_buffer_size =
      (max_mem - rank_n() * (sizeof(KmersReadsBuffer<MAX_K>))) / (rank_n() * KmersReadsBuffer<MAX_K>::get_size_per());
  max_rdvz_buffer_size = ::min(max_rdvz_buffer_size, max_mem_buffer_size) + 1;
  if (kmers_reads_buffers.size() != rank_n()) {
    LOG_MEM("Before kmer_reads_buffers allocation");
    kmers_reads_buffers.resize(rank_n());
    LOG("Allocating ", max_rdvz_buffer_size, " elements over ", rank_n(),
        " buffers: ", get_size_str(rank_n() * max_rdvz_buffer_size * KmersReadsBuffer<MAX_K>::get_size_per()),
        " for aggregated kmer_reads_buffers\n");
    for (auto &krb : kmers_reads_buffers) krb.reserve(max_rdvz_buffer_size);
    LOG_MEM("After kmer_reads_buffers allocation");
    LOG("max_mem for buffers=", get_size_str(max_mem), " KLIGN_MAX_RPC_MESSAGE_SIZE=", get_size_str(KLIGN_MAX_RPC_MESSAGE_SIZE),
        " max_mem_buffer_size=", max_mem_buffer_size, " max_rdvz_buffer_size=", max_rdvz_buffer_size, "\n");
  }
  return max_rdvz_buffer_size;
};  // allocate_kmers_reads_buffers

// TODO refactor inner loop on AlignReadPackage: vector<PackedReads>*, vector<ReadRecord> *, Alns *
template <int MAX_K>
void fetch_ctg_maps(KmerCtgDHT<MAX_K> &kmer_ctg_dht, AlignReadPackages &arps, int seed_space,
                    vector<KmersReadsBuffer<MAX_K>> &kmers_reads_buffers, KlignTimers &timers) {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  BarrierTimer bt(__FILEFUNC__);

  timers.fetch_ctg_maps.start();
  int64_t bytes_sent = 0;
  int64_t bytes_received = 0;
  int64_t max_bytes_sent = 0;
  int64_t max_bytes_received = 0;
  int64_t num_reads = 0;
  int64_t num_alns = 0;
  int64_t num_excess_alns_reads = 0;
  int64_t num_kmers = 0;
  int64_t num_rpcs = 0;

  size_t tot_num_local_reads = 0;
  for (auto &arp : arps) {
    tot_num_local_reads += arp.packed_reads->get_local_num_reads();
  }
  ProgressBar progbar(tot_num_local_reads, string("Fetching ctg maps for alignments - ") +
                                               upcxx_utils::get_basename(arps.front().packed_reads->get_fname()));

  upcxx::future<> fetch_fut_chain = make_future();
  for (auto &arp : arps) {
    auto &packed_reads = arp.packed_reads;
    auto &read_records = *arp.read_records;
    auto num_local_reads = packed_reads->get_local_num_reads();
    assert(num_local_reads == read_records.size());

    vector<Kmer<MAX_K>> kmers;
    int kmer_len = Kmer<MAX_K>::get_k();

    // should be noop here except to return the size
    if (kmers_reads_buffers.empty()) NET_LOG("Potential attention defecit here allocating empty kmers_reads_buffers");
    auto max_rdvz_buffer_size = allocate_kmers_reads_buffers(kmers_reads_buffers);

    // stagger first batches with a reduced count before flush for better load balance
    // each rank randomly chooses a size between 1/3 and 2/3 of the max size for the first 1/2 rank_n() messages
    size_t max_kmer_buffer_size =
        max_rdvz_buffer_size < 500 ? max_rdvz_buffer_size : max_rdvz_buffer_size / 3 + rand() % (1 + max_rdvz_buffer_size / 3) + 1;

    int stagger_count = 0;

    for (int ri = 0; ri < num_local_reads; ri++) {
      progress();
      progbar.update();
      string read_seq;
      packed_reads->get_read_seq(ri, read_seq);
      // this happens when a placeholder read with just a single N character is added after merging reads
      if (kmer_len > read_seq.length()) continue;
      num_reads++;
      assert(read_records[ri].index == -1);
      read_records[ri] = ReadRecord(ri, (int)read_seq.length());
      assert(read_records[ri].is_valid() && read_records[ri].rlen == read_seq.length());

      Kmer<MAX_K>::get_kmers(kmer_len, string_view(read_seq.data(), read_seq.size()), kmers, true);
      if (!kmers.size()) continue;

      for (int i = 0; i < kmers.size(); i += seed_space) {
        progress();
        const Kmer<MAX_K> &kmer_fw = kmers[i];
        if (!kmer_fw.is_valid()) continue;
        num_kmers++;
        const Kmer<MAX_K> kmer_rc = kmer_fw.revcomp();
        assert(kmer_fw.is_valid() && kmer_rc.is_valid());
        const Kmer<MAX_K> *kmer_lc = &kmer_fw;
        bool is_rc = false;
        if (kmer_rc < kmer_fw) {
          kmer_lc = &kmer_rc;
          is_rc = true;
        }
        assert(kmer_lc->is_least());
        auto target = kmer_ctg_dht.get_target_rank(*kmer_lc);
        auto &tgt_kr_buf = kmers_reads_buffers[target];
        if (tgt_kr_buf.empty()) tgt_kr_buf.reserve(max_rdvz_buffer_size);  // lazy but avoid excessive resizes
        tgt_kr_buf.add(*kmer_lc, &read_records[ri], i, is_rc);
        if (tgt_kr_buf.size() >= max_kmer_buffer_size) {
          // target buffer is full or stagger start
          auto fetch_fut = fetch_ctg_maps_for_target(target, kmer_ctg_dht, tgt_kr_buf, num_alns, num_excess_alns_reads, bytes_sent,
                                                     bytes_received, max_bytes_sent, max_bytes_received, num_rpcs, timers);
          fetch_fut_chain = when_all(fetch_fut_chain, fetch_fut);
          if (max_kmer_buffer_size < max_rdvz_buffer_size && ++stagger_count > rank_n() / 2) {
            LOG("Done staggering fetch_ctg_maps_for_target.  max_kmer_buffer_size=", max_kmer_buffer_size, " -> ",
                max_rdvz_buffer_size, " stagger_count=", stagger_count, "\n");
            max_kmer_buffer_size = max_rdvz_buffer_size;  // stagger achieved on first 2/3 of ranks, reset to max size
          }
          assert(tgt_kr_buf.empty());
        }
      }  // for kmer
      kmers.clear();
    }  // for local read
  }    // for arp

  LOG("flushing ctg maps for local reads\n");
  // flush any buffers
  if (tot_num_local_reads > 0) {
    for (auto target : upcxx_utils::foreach_rank_by_node()) {  // stagger by rank_me, round robin by node
      progress();
      auto &tgt_kr_buf = kmers_reads_buffers[target];
      if (tgt_kr_buf.size()) {
        auto fetch_fut = fetch_ctg_maps_for_target(target, kmer_ctg_dht, tgt_kr_buf, num_alns, num_excess_alns_reads, bytes_sent,
                                                   bytes_received, max_bytes_sent, max_bytes_received, num_rpcs, timers);
        fetch_fut_chain = when_all(fetch_fut_chain, fetch_fut);
      }
    }
  }
  upcxx_utils::flush_outstanding_futures();
  fetch_fut_chain.wait();
  auto fut_prog_done = progbar.set_done();
  timers.fetch_ctg_maps.stop();
  upcxx_utils::Timings::set_pending(fut_prog_done);

  LOG("Finished flush\n");

  // defer reporting
  auto &pr = Timings::get_promise_reduce();
  auto fut_reduce = when_all(pr.reduce_one(num_reads, op_fast_add, 0), pr.reduce_one(num_kmers, op_fast_add, 0),
                             pr.reduce_one(bytes_sent, op_fast_add, 0), pr.reduce_one(bytes_received, op_fast_add, 0),
                             pr.reduce_one(max_bytes_sent, op_fast_max, 0), pr.reduce_one(max_bytes_received, op_fast_max, 0),
                             pr.reduce_one(num_rpcs, op_fast_add, 0), pr.reduce_one(num_excess_alns_reads, op_fast_add, 0));

  auto fut_report = fut_reduce.then([](auto all_num_reads, auto all_num_kmers, auto all_bytes_sent, auto all_bytes_received,
                                       auto all_max_bytes_sent, auto all_max_bytes_received, auto all_num_rpcs,
                                       auto all_excess_alns_reads) {
    if (rank_me() == 0) {
      SLOG_VERBOSE("Parsed ", all_num_reads, " reads and extracted ", all_num_kmers, " kmers\n");
      SLOG_VERBOSE("Sent ", get_size_str(all_bytes_sent), " (", get_size_str(all_num_rpcs > 0 ? all_bytes_sent / all_num_rpcs : 0),
                   " avg msg, ", get_size_str(all_max_bytes_sent), " max msg) of kmers and received ",
                   get_size_str(all_bytes_received), " (", get_size_str(all_num_rpcs > 0 ? all_bytes_received / all_num_rpcs : 0),
                   " avg msg, ", get_size_str(all_max_bytes_received), " max msg )\n");
      if (all_excess_alns_reads)
        SLOG_VERBOSE("Dropped ", all_excess_alns_reads, " alignments in excess of ", KLIGN_MAX_ALNS_PER_READ, " per read\n");
    }
  });
  Timings::set_pending(fut_report);
  progress();
};  // fetch_ctg_maps

struct AlnStats {
  size_t num_bad_alns = 0;
  size_t num_dup_alns = 0;
  size_t size_alns = 0;
  int64_t num_reads_aligned = 0;
  int64_t num_reads = 0;
  upcxx::future<int64_t> fut_num_alns{};
  upcxx::future<int64_t> fut_num_perfect_alns{};
};

// TODO refactor inner loop on vector<PackedReads*, vector<ReadRecord> *, Alns *>
template <int MAX_K>
void compute_alns(AlignReadPackages &arps, KlignAligner &aligner, int64_t all_num_ctgs, int rget_buf_size, KlignTimers &timers) {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  BarrierTimer bt(__FILEFUNC__);
  size_t tot_num_reads = 0;
  for (auto &arp : arps) {
    const PackedReads *packed_reads = arp.packed_reads;
    tot_num_reads += packed_reads->get_local_num_reads();
  }

  timers.compute_alns.start();
  std::map<int, AlnStats> aln_stats_map;

  ProgressBar progbar(tot_num_reads, string("Computing alignments"));
  for (auto &arp : arps) {
    const PackedReads *packed_reads = arp.packed_reads;
    vector<ReadRecord> &read_records = *arp.read_records;
    Alns &alns = *arp.alns;
    int read_group_id = arp.read_group_id;
    auto short_name = get_basename(packed_reads->get_fname());
    LOG("compute_alns on ", short_name, "\n");
    auto &aln_stats = aln_stats_map[read_group_id];

    int kmer_len = Kmer<MAX_K>::get_k();
    aligner.set_alns(&alns);
    string read_seq, read_id, read_quals;

    for (auto &read_record : read_records) {
      progress();
      progbar.update();
      if (kmer_len > read_record.rlen) continue;
      aln_stats.num_reads++;
      // compute alignments
      if (read_record.aligned_ctgs_map.size()) {
        aln_stats.num_reads_aligned++;
        packed_reads->get_read(read_record.index, read_id, read_seq, read_quals);
        aligner.compute_alns_for_read(read_record.aligned_ctgs_map, read_id, read_seq, read_group_id, rget_buf_size);
      }
    }
    read_records = {};  // clear memory
    aln_stats.fut_num_alns = aligner.fut_get_num_alns();
    aln_stats.fut_num_perfect_alns = aligner.fut_get_num_perfect_alns();
  }

  LOG("Completed processing my reads for batch\n");

  aligner.flush_remaining();
  aligner.finish_alns();
  Timings::set_pending(progbar.set_done());
  timers.compute_alns.stop();
  for (auto &arp : arps) {
    const PackedReads *packed_reads = arp.packed_reads;
    vector<ReadRecord> &read_records = *arp.read_records;
    Alns &alns = *arp.alns;
    auto read_group_id = arp.read_group_id;
    read_records.clear();
    auto aln_stats = aln_stats_map[arp.read_group_id];
    aln_stats.num_dup_alns += alns.get_num_dups();
    aln_stats.num_bad_alns += alns.get_num_bad();
    aln_stats.size_alns += alns.size();

    // defer reporting for this read_group_id
    auto &pr = Timings::get_promise_reduce();
    auto short_name = get_basename(packed_reads->get_fname());
    auto fut_reduce =
        when_all(pr.reduce_one(aln_stats.num_reads, op_fast_add, 0), pr.reduce_one(aln_stats.num_reads_aligned, op_fast_add, 0),
                 aln_stats.fut_num_alns, aln_stats.fut_num_perfect_alns, pr.reduce_one(aln_stats.num_dup_alns, op_fast_add, 0),
                 pr.reduce_one(aln_stats.num_bad_alns, op_fast_add, 0), pr.reduce_one(aln_stats.size_alns, op_fast_add, 0));

    auto fut_report =
        fut_reduce.then([short_name, read_group_id](auto all_num_reads, auto all_num_reads_aligned, auto all_num_alns,
                                                    auto all_num_perfect, auto all_num_dups, auto all_num_bad, auto all_num_good) {
          SLOG_VERBOSE("Found ", all_num_alns, " alignments in rg=", read_group_id, " ", short_name, "\n");
          SLOG_VERBOSE("  perfect ", perc_str(all_num_perfect, all_num_alns), "\n");
          SLOG_VERBOSE("  good ", perc_str(all_num_good, all_num_alns), "\n");
          SLOG_VERBOSE("  bad ", perc_str(all_num_bad, all_num_alns), "\n");
          SLOG_VERBOSE("  duplicates ", perc_str(all_num_dups, all_num_alns), "\n");
          SLOG_VERBOSE("Mapped ", perc_str(all_num_reads_aligned, all_num_reads), " reads to contigs, average mappings per read ",
                       (double)all_num_alns / all_num_reads_aligned, "\n");
        });
    Timings::set_pending(fut_report);
  }

  aligner.log_ctg_bytes_fetched();

  // implement a delayed barrier until wait_pending is called
  auto fut_all_complete = upcxx::barrier_async();
  Timings::set_pending(fut_all_complete);
  progress();

};  // compute_alns

// just construct, no build
// used in post_assembly to better support multiple files
template <int MAX_K>
shared_ptr<KmerCtgDHT<MAX_K>> construct_kmer_ctg_dht(unsigned kmer_len, int max_store_size, int max_rpcs_in_flight,
                                                     size_t num_ctg_kmers, bool allow_multi_kmers) {
  BarrierTimer timer(__FILEFUNC__);
  Kmer<MAX_K>::set_k(kmer_len);
  return make_shared<KmerCtgDHT<MAX_K>>(max_store_size, max_rpcs_in_flight, allow_multi_kmers, num_ctg_kmers);
};

// just build, leaving kmer_store to be reused
// used in post_assembly to better support multiple files
template <int MAX_K>
void build_kmer_ctg_dht(KmerCtgDHT<MAX_K> &kmer_ctg_dht, Contigs &ctgs, int min_ctg_len) {
  BarrierTimer timer(__FILEFUNC__);
  kmer_ctg_dht.release_data();
  kmer_ctg_dht.build(ctgs, min_ctg_len);
  kmer_ctg_dht.purge_high_count_seeds();
};  // build_kmer_ctg_dht

// construct build and clean up kmer_store memory
// used in contigging and scaffolding
template <int MAX_K>
shared_ptr<KmerCtgDHT<MAX_K>> build_kmer_ctg_dht(unsigned kmer_len, int max_store_size, int max_rpcs_in_flight, Contigs &ctgs,
                                                 int min_ctg_len, bool allow_multi_kmers) {
  auto num_ctg_kmers = ctgs.get_num_ctg_kmers(kmer_len);
  auto sh_kmer_ctg_dht =
      construct_kmer_ctg_dht<MAX_K>(kmer_len, max_store_size, max_rpcs_in_flight, num_ctg_kmers, allow_multi_kmers);
  build_kmer_ctg_dht(*sh_kmer_ctg_dht, ctgs, min_ctg_len);
  sh_kmer_ctg_dht->clear_stores();
  return sh_kmer_ctg_dht;
};  // build_kmer_ctg_dht

template <int MAX_K>
pair<double, double> find_alignments(unsigned kmer_len, PackedReadsList &packed_reads_list, int max_store_size,
                                     int max_rpcs_in_flight, Contigs &ctgs, Alns &alns, int seed_space, int rlen_limit,
                                     bool use_blastn_scores, int min_ctg_len, int rget_buf_size) {
  BarrierTimer timer(__FILEFUNC__);
  SLOG_VERBOSE("Aligning with seed size of ", kmer_len, " and seed space ", seed_space, "\n");

  const bool ALLOW_MULTI_KMERS = false;
  const bool REPORT_CIGAR = false;
  auto sh_kmer_ctg_dht =
      build_kmer_ctg_dht<MAX_K>(kmer_len, max_store_size, max_rpcs_in_flight, ctgs, min_ctg_len, ALLOW_MULTI_KMERS);
  auto &kmer_ctg_dht = *sh_kmer_ctg_dht;
  KlignTimers timers;
  int64_t all_num_ctgs = reduce_all(ctgs.size(), op_fast_add).wait();

#ifdef DEBUG
// kmer_ctg_dht.dump_ctg_kmers();
#endif
  int read_group_id = 0;
  upcxx::promise prom_gpu(1);
#ifdef KLIGN_NO_THREAD_POOL
  std::vector<std::function<void()>> sort_lambdas;
#endif

  vector<KmersReadsBuffer<MAX_K>> kmers_reads_buffers;
  allocate_kmers_reads_buffers(kmers_reads_buffers);
  {
    BarrierTimer bt("Barrier after allocation of kmers_reads_buffers");
    LOG_MEM("After allocation of kmers_reads_buffer");
  }

  KlignAligner aligner(Kmer<MAX_K>::get_k(), rlen_limit, REPORT_CIGAR, use_blastn_scores, timers);

  upcxx::promise<> prom(1);
  future<> sort_fut = prom.get_future();
  for (auto packed_reads : packed_reads_list) {
    vector<ReadRecord> read_records(packed_reads->get_local_num_reads());
    auto sh_alns = make_shared<Alns>();
    Alns &alns_for_sample = *sh_alns;
    AlignReadPackages arps;
    arps.push_back({.packed_reads = packed_reads, .read_records = &read_records, .alns = &alns_for_sample});
    fetch_ctg_maps(kmer_ctg_dht, arps, seed_space, kmers_reads_buffers, timers);
    compute_alns<MAX_K>(arps, aligner, all_num_ctgs, rget_buf_size, timers);
    if (!alns_for_sample.empty()) {
      assert(alns_for_sample.is_valid());
      auto sort_lambda = [sh_alns, &alns, &timers, fname = packed_reads->get_fname()]() {
        LOG("Starting sorts on ", sh_alns->size(), " for ", fname, "\n");
        KlignAligner::sort_alns_batch(*sh_alns, timers, fname);
        alns.append(*sh_alns);
        LOG("Finished sorts on ", fname, "\n");
      };
#ifndef KLIGN_NO_THREAD_POOL
      sort_fut = upcxx_utils::execute_after_in_thread_pool(sort_fut, sort_lambda).then([sh_alns]() {
        // ensure destructor happens within master persona
        LOG("Returned sort_lambda\n");
      });
#else
      sort_lambdas.emplace_back(sort_lambda);
#endif
    }
    read_group_id++;
  }  // for packed_reads
  aligner.reset();
  kmers_reads_buffers.clear();

  Timings::wait_pending();  // ensure all ranks have finished compute_alns on this data
  LOG("Releasing KmerDHT\n");
  sh_kmer_ctg_dht.reset();  // destroy dht with implicit barriers on kmer_store.clear()

  prom.fulfill_anonymous(1);
  sort_fut.wait();
#ifdef KLIGN_NO_THREAD_POOL
  LOG("Sorting all lambdas now\n");
  for (auto &l : sort_lambdas) {
    l();
  }
  sort_lambdas = {};
#endif
  LOG("Finished all sorts\n");

  timers.done_all();
  double aln_kernel_elapsed = timers.aln_kernel.get_elapsed();
  double aln_comms_elapsed = timers.fetch_ctg_maps.get_elapsed() + timers.rget_ctg_seqs.get_elapsed();
  timers.clear();
  return {aln_kernel_elapsed, aln_comms_elapsed};
};  // find_alignments
