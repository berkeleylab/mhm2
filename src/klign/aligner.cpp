#include "aligner.hpp"
#include "upcxx/upcxx.hpp"

#include "upcxx_utils/limit_outstanding.hpp"

using namespace std;
using namespace upcxx;
using namespace upcxx_utils;

GlobalContigs::~GlobalContigs() { free_global_contigs(); }

void GlobalContigs::free_global_contigs() {
  assert(upcxx::master_persona().active_with_caller());
  LOG("Deallocting ", global_ctg_seqs.size(), " global contigs\n");
  for (auto &gptr : global_ctg_seqs) {
    DBG_VERBOSE("Deallocating ", gptr, "\n");
    upcxx::deallocate(gptr);
  }
}
global_ptr<char> GlobalContigs::add_ctg_seq(string seq) {
  assert(upcxx::master_persona().active_with_caller());
  auto x = Contig::validate_seq(seq);
  if (x != seq.size()) DIE("Invalid contig sequence at ", x, ": ", seq, "\n");
  auto seq_gptr = upcxx::allocate<char>(seq.length() + 1);
  global_ctg_seqs.push_back(seq_gptr);  // remember to dealloc!
  strcpy(seq_gptr.local(), seq.c_str());
  DBG_VERBOSE("Created ", seq_gptr, " with clen=", seq.size(), "\n");
  return seq_gptr;
}
bool GlobalContigs::empty() const { return global_ctg_seqs.empty(); }
void GlobalContigs::reserve(size_t sz) { global_ctg_seqs.reserve(sz); }

void KlignAligner::submit_block(Alns &dest_alns) {
  assert(upcxx::initialized() && upcxx::master_persona().active_with_caller());
  // may be called from within progress

  static int _counter = 0;
  auto counter = _counter++;
  auto sz = kernel_alns.size();

  assert(sz == ctg_seqs.size());
  assert(sz == read_seqs.size());
  if (sz == 0) {
    WARN("Submit block on empty kernel_alns!\n");
    return;
  }

  LOG("submit_block of ", kernel_alns.size(), " to alns=", (void *)&dest_alns, " with ", dest_alns.size(), " block_done ",
      sh_next_block_done, "\n");

  // swap next_block and AlignBlockData *before* any futures work... this could run within progress so ordering of futures could get
  // jumbled
  auto sh_this_block_done = sh_next_block_done;
  auto fut_ready = sh_this_block_done->get_future();
  sh_next_block_done = make_shared<promise<>>(1);
  DBG("Swapping block_done ", sh_this_block_done, " with ", sh_next_block_done, "\n");
  assert(!fut_ready.is_ready());
  ShAlignBlockData aln_block_data = make_shared<AlignBlockData>(kernel_alns, ctg_seqs, read_seqs, max_clen, max_rlen);
  assert(kernel_alns.empty());
  assert(ctg_seqs.empty() && "kernel_align_block did consumed ctg_seqs!");
  assert(read_seqs.empty() && "kernel_align_block did consumed read_seqs!");

  pending_alignments_fut = when_all(pending_alignments_fut, fut_ready);

  active_kernel_fut = active_kernel_fut.then([=, &dest_alns]() {
    assert(upcxx::master_persona().active_with_caller());
    DBG("Starting alignment # ", counter, " with ", sz, " dest_alns=", (void *)&dest_alns, " with ", dest_alns.size(),
        " block_done ", sh_this_block_done, "\n");
  });

  auto fut_block = kernel_align_block(cpu_aligner, aln_block_data, klign_timers);

  auto fut = fut_block.then([=, &dest_alns](ShAlignBlockData aln_block_data) {
    assert(upcxx::master_persona().active_with_caller());
    size_t added_alns = 0;
    if (aln_block_data) {
      added_alns = aln_block_data->finish(dest_alns);
    }
    LOG("Appended submit_block counter=", counter, " with sz=", sz, " added=", added_alns, " block_done ", sh_this_block_done,
        "\n");
  });

  active_kernel_fut = when_all(active_kernel_fut, fut);

  active_kernel_fut = active_kernel_fut.then([=]() {
    assert(upcxx::master_persona().active_with_caller());
    LOG("Completed submit_block alignment # ", counter, " with ", sz, ". Fulfilling block_done ", sh_this_block_done, "\n");
    sh_this_block_done->fulfill_anonymous(sz + 1);
    if (!sh_this_block_done->get_future().is_ready()) DIE("Failed to completely fulfill block_done: ", sh_this_block_done, "\n");
  });

  DBG("Leaving submit_block alignment # ", counter, " with sz=", kernel_alns.size(), " and block_done ", sh_next_block_done, "\n");
  if (!upcxx::in_progress()) progress();
}  // submit_block

// align a single read.
// future may be forgotten and may not be ready until flush has been called
upcxx::future<> KlignAligner::align_read(const string &rname, int64_t cid, const string_view &rseq, const string_view &ctg_seq,
                                         int rstart, int cstart, char orient, int overlap_len, int read_group_id, Alns &dest_alns) {
  assert(upcxx::initialized() && upcxx::master_persona().active_with_caller());
  // may be called from within progress -- NOT thread-safe
  assert(alns && "set_alns must be called before aligning a read");
  assert(read_group_id != -1);
  num_alns++;
  size_t clen = ctg_seq.length();
  size_t rlen = rseq.length();
  assert(clen > cstart);
  assert(rlen > rstart);
  assert(overlap_len <= clen && overlap_len <= rlen);
  string_view cseq = string_view(ctg_seq.data() + cstart, overlap_len);
  auto fut_done = make_future();
  // DBG_VERBOSE("r=", rname, " cid=", cid, " rg=", read_group_id, " to ", (void*) &dest_alns, "\n");
  if (cseq.compare(0, overlap_len, rseq, rstart, overlap_len) == 0) {
    num_perfect_alns++;
    int rstop = rstart + overlap_len;
    int cstop = cstart + overlap_len;
    if (orient == '-') switch_orient(rstart, rstop, rlen);
    int score1 = overlap_len * cpu_aligner.ssw_aligner.get_match_score();
    Aln aln(rname, cid, rstart, rstop, rlen, cstart, cstop, clen, orient, score1, 0, 0, read_group_id);
    assert(aln.is_valid());
    if (cpu_aligner.ssw_filter.report_cigar) aln.set_sam_string(to_string(overlap_len) + "=");
    dest_alns.add_aln(aln);
  } else {
    assert(cseq.size() < std::numeric_limits<uint32_t>::max());
    assert(rseq.size() < std::numeric_limits<uint16_t>::max());
    max_clen = max((uint32_t)cseq.size(), max_clen);
    max_rlen = max((uint16_t)rseq.size(), max_rlen);
    int64_t num_kernel_alns = kernel_alns.size() + 1;
    int64_t tot_mem_est = num_kernel_alns * (max_clen + max_rlen + 2 * sizeof(int) + 5 * sizeof(short));
    // contig is the ref, read is the query - done this way so that we can potentially do multiple alns to each read
    // this is also the way it's done in meraligner
    kernel_alns.emplace_back(rname, cid, rlen, cstart, clen, orient, read_group_id);
    ctg_seqs.emplace_back(cseq);
    read_seqs.emplace_back(rseq);
    sh_next_block_done->require_anonymous(1);
    DBG("Required another for block_done ", sh_next_block_done, " sz=", kernel_alns.size(), "\n");
    fut_done = sh_next_block_done->get_future();
    if (num_kernel_alns >= KLIGN_GPU_BLOCK_SIZE) {
      submit_block(dest_alns);
    }
  }
#ifdef DEBUG
  if (!fut_done.is_ready()) {
    pending_alignments_fut = when_all(pending_alignments_fut, fut_done);
  }
#endif
  if (!upcxx::in_progress()) progress();
  return fut_done;
}  // align_read

void KlignAligner::clear() {
  if (kernel_alns.size())
    DIE("clear called with alignments in the buffer - was flush_remaining called before destrutor? kernel_size=",
        kernel_alns.size(), "\n");
  clear_aln_bufs();
}

future<> KlignAligner::prep_do_rget_irregular(deque<rget_pair_t> &src, deque<ctg_pair_t> &dest,
                                              target_rank_rget_requests_t &tgt_rget_req, HASH_TABLE<cid_t, string> &ctgs_fetched) {
  upcxx::future<> fut_chain = make_future();
  for (auto &req : tgt_rget_req) {
    progress();
    auto clen = req.ctg_loc.clen;
    auto it = ctgs_fetched.find(req.ctg_loc.cid);
    if (it == ctgs_fetched.end()) {
      it = ctgs_fetched.insert({req.ctg_loc.cid, string(clen, ' ')}).first;
      if (KLIGN_MAX_IRREGULAR_RGET > 0 && clen >= KLIGN_MAX_IRREGULAR_RGET) {
        // issue a normal rget
        auto fut = rget(req.ctg_loc.seq_gptr, const_cast<char *>(it->second.data()), clen);
        // maintain attention and do not overload network
        limit_outstanding_futures(fut).wait();
        fut_chain = when_all(fut, fut_chain);
        large_rget_calls++;
        large_rget_bytes += clen;
      } else {
        src.push_back({req.ctg_loc.seq_gptr, clen});
        dest.push_back({const_cast<char *>(it->second.data()), clen});
        ctg_bytes_fetched += clen;
      }
    }
  }
  assert(src.size() == dest.size());
  return fut_chain;
}  // prep_do_rget_irregular

// returns future for the communication completed; the alignments will lag based on any pending batches
future<> KlignAligner::do_rget_irregular(int target, bool is_node) {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  assert(rget_requests.size() == upcxx_utils::split_rank::node_team().rank_n());
  static size_t _counter = 0;
  auto counter = ++_counter;
  AsyncTimer rget_timer(make_string("rget_timer tgt=", target, " ct=", counter));
  rget_timer.start();
  auto sh_src = make_shared<deque<rget_pair_t>>();
  auto &src = *sh_src;
  auto sh_dest = make_shared<deque<pair<char *, size_t>>>();
  auto &dest = *sh_dest;

  // consume and clear the rget_requests for target
  auto sh_pending_requests = make_shared<target_rank_rget_requests_t>();
  std::swap(*sh_pending_requests, rget_requests[target]);
  auto &tgt_rget_req = *sh_pending_requests;
  auto sz = tgt_rget_req.size();

  // create a new ctgs_fetched map with limited lifetime
  auto sh_ctgs_fetched = make_shared<HASH_TABLE<cid_t, string>>();
  auto &ctgs_fetched = *sh_ctgs_fetched;
  ctgs_fetched.reserve(sz);

  auto fut_chain = prep_do_rget_irregular(src, dest, tgt_rget_req, ctgs_fetched);

  DBG("Using rget_irregular to fetch ", perc_str(ctgs_fetched.size(), rget_requests[target].size()), " contigs from node ", target,
      " count=", counter, "\n");

  if (src.size()) {
    upcxx::future<> fut_rget_irr;
    if (is_node) {
      fut_rget_irr = rget_irregular_by_node(src.begin(), src.end(), dest.begin(), dest.end());
    } else {
      fut_rget_irr = rget_irregular(src.begin(), src.end(), dest.begin(), dest.end());
    }
    // maintain attention and do not overload network
    limit_outstanding_futures(fut_rget_irr).wait();
    fut_chain = when_all(fut_chain, fut_rget_irr).then([sh_src, sh_dest]() {
      // destroy sh_src and sh_dest after rget and rget_irregular complete
    });
  }
  fut_chain = fut_chain.then([&klign_timers = this->klign_timers, rget_timer]() {
    rget_timer.stop(rand() % 1000 == 0);
    klign_timers.rget_ctg_seqs += rget_timer;
    // destroy shared_ptrs for src & dest after rget_irregular has completed
  });
  rget_calls++;

  // must keep attention here.  NOT fut_chain.then([&self=*this,=](){ return self.process_rget_alignments(...); });
  fut_chain.wait();

  // ignore alignment future
  auto fut_ignored = process_rget_alignments(sh_pending_requests, sh_ctgs_fetched);
  return fut_chain;  // return future for after networking ops have competed
}  // do_rget_irregular

upcxx::future<> KlignAligner::process_rget_alignments(shared_ptr<target_rank_rget_requests_t> sh_pending_requests,
                                                      shared_ptr<HASH_TABLE<cid_t, string>> sh_ctgs_fetched) {
  assert(upcxx::master_persona().active_with_caller());
  assert(!upcxx::in_progress());  // must keep attention here
  auto sh_alns = make_shared<Alns>();
  DBG("Made temporary dest_alns=", (void *)sh_alns.get(), " with 0 for do_rget_irregular\n");
  // process the alignments after rgets have completed

  auto lambda = [=, &self = *this]() -> upcxx::future<> {
    // will be called from within progress
    assert(upcxx::master_persona().active_with_caller());
    assert(!upcxx::in_progress());
    auto &ctgs_fetched = *sh_ctgs_fetched;
    auto &dest_alns = *sh_alns;
    auto &pending_requests = *sh_pending_requests;
    auto fut_chain = make_future();
    for (auto &req : pending_requests) {
      if (!upcxx::in_progress()) progress();  // keep attention
      auto cid = req.ctg_loc.cid;
      auto it = ctgs_fetched.find(cid);
      if (it == ctgs_fetched.end()) DIE("Could not find the sequence for the contig ", cid);
      string &ctg_seq = it->second;
#ifdef DEBUG
      if (Contig::validate_seq(ctg_seq) != ctg_seq.size())
        DIE("Could not get full ctg for cid=", cid, " clen=", req.ctg_loc.clen, ": ", ctg_seq.length(), ": ", ctg_seq, "\n");
#endif
      auto fut = self.align_read(req.rname, cid, req.read_seq, ctg_seq, req.rstart, req.cstart, req.orient, req.overlap_len,
                                 req.read_group_id, dest_alns);
#ifdef DEBUG
      // track every alignment (takes mem)
      if (!fut.is_ready()) fut_chain = when_all(fut, fut_chain);
#endif
    }
    auto fut_next = self.kernel_alns.empty() ? make_future() : self.sh_next_block_done->get_future();
    auto fut_ret = when_all(fut_chain, self.active_kernel_fut, fut_next);
    return fut_ret;
  };  // lambda

  auto fut_align = lambda();

  fut_align = fut_align.then([alns = this->alns, sh_alns, sh_pending_requests, sh_ctgs_fetched] {
    assert(alns);
    DBG("Appending temporary dest_alns=", (void *)sh_alns.get(), " with ", alns->size(),
        " for do_rget_irregular to alns=", (void *)alns, "\n");
    const_cast<Alns *>(alns)->append(*sh_alns);
    // complete all pending alignments for this set of requests
    sh_pending_requests->prom.fulfill_anonymous(1);
    // also destroy shared_ptrs for requests and contigs and alns in master persona
  });

  // track alignments in pending_alignments_fut
  pending_alignments_fut = when_all(pending_alignments_fut, fut_align);
  // maintain attention and do not overload network
  if (!upcxx::in_progress()) progress();

  return fut_align;  // future for all communications have completed -- NOT all alignments
}  // process_rget_alignments

tuple<uint32_t, uint16_t, uint16_t> KlignAligner::get_start_positions(int kmer_len, uint32_t clen, uint32_t pos_in_ctg,
                                                                      uint16_t pos_in_read, uint16_t rlen) {
  // calculate available bases before and after the seeded kmer
  auto ctg_bases_left_of_kmer = pos_in_ctg;
  int ctg_bases_right_of_kmer = clen - ctg_bases_left_of_kmer - kmer_len;
  assert(ctg_bases_right_of_kmer >= 0);
  auto read_bases_left_of_kmer = pos_in_read;
  int read_bases_right_of_kmer = rlen - kmer_len - pos_in_read;
  assert(read_bases_right_of_kmer >= 0);
  auto left_of_kmer = min((uint32_t)read_bases_left_of_kmer, ctg_bases_left_of_kmer);
  int right_of_kmer = min(read_bases_right_of_kmer, ctg_bases_right_of_kmer);

  int cstart = pos_in_ctg - left_of_kmer;
  int rstart = pos_in_read - left_of_kmer;
  assert(cstart >= 0);
  assert(rstart >= 0);
  uint32_t overlap_len = left_of_kmer + kmer_len + right_of_kmer;
  assert(overlap_len >= 0);
  assert(overlap_len <= std::numeric_limits<uint16_t>::max());
  return {(uint32_t)cstart, (uint16_t)rstart, (uint16_t)overlap_len};
}  // get_start_positions

KlignAligner::KlignAligner(int kmer_len, int rlen_limit, bool report_cigar, bool use_blastn_scores, KlignTimers &timers)
    : num_alns(0)
    , num_perfect_alns(0)
    , kmer_len(kmer_len)
    , kernel_alns({})
    , ctg_seqs({})
    , read_seqs({})
    , active_kernel_fut(make_future())
    , pending_alignments_fut(make_future())
    , cpu_aligner(report_cigar, use_blastn_scores)
    , alns(nullptr)
    , _sh_rget_requests(get_singleton_rget_requests() ? get_singleton_rget_requests() :
                                                        make_shared<rget_requests_t>(upcxx_utils::split_rank::node_team().rank_n()))
    , rget_requests(*_sh_rget_requests)
    , rget_calls(0)
    , large_rget_calls(0)
    , large_rget_bytes(0)
    , local_ctg_fetches(0)
    , remote_ctg_fetches(0)
    , klign_timers(timers)
    , sh_next_block_done{} {
  assert(upcxx::initialized() && master_persona().active_with_caller());
  sh_next_block_done = make_shared<promise<>>(1);
  LOG("Initialized KlignAligner with block_done ", sh_next_block_done, "\n");
  init_aligner((int)cpu_aligner.ssw_aligner.get_match_score(), (int)cpu_aligner.ssw_aligner.get_mismatch_penalty(),
               (int)cpu_aligner.ssw_aligner.get_gap_opening_penalty(), (int)cpu_aligner.ssw_aligner.get_gap_extending_penalty(),
               (int)cpu_aligner.ssw_aligner.get_ambiguity_penalty(), rlen_limit, report_cigar);
  upcxx_utils::split_rank::node_team();  // just initialize the static node team if it has not already been created
}  // KlignAligner

KlignAligner::~KlignAligner() { reset(); }

void KlignAligner::reset() {
  LOG(__FILEFUNC__, "\n");
  assert(upcxx::initialized() && master_persona().active_with_caller());
  assert(!upcxx::in_progress());
  set_alns();
  while (!active_kernel_fut.is_ready()) {
    LOG("Waiting for outstanding kernel\n");
    wait_wrapper(active_kernel_fut);
  }
  finish_alns();
  cleanup_aligner();
}  // reset

void KlignAligner::set_alns(Alns *new_alns) {
  LOG("Setting new alns to ", (void *)new_alns, " from ", (void *)alns, "\n");
  if (alns != new_alns && alns != nullptr) {
    flush_remaining();
    clear();
  }
  alns = new_alns;
}

upcxx::future<int64_t> KlignAligner::fut_get_num_perfect_alns(bool all) {
  auto &pr = Timings::get_promise_reduce();
  if (!all) return pr.reduce_one(num_perfect_alns.load(), op_fast_add, 0);
  return pr.reduce_all(num_perfect_alns.load(), op_fast_add);
}

future<int64_t> KlignAligner::fut_get_num_alns(bool all) {
  auto &pr = Timings::get_promise_reduce();
  if (!all) return pr.reduce_one(num_alns.load(), op_fast_add, 0);
  return pr.reduce_all(num_alns.load(), op_fast_add);
}
void KlignAligner::clear_aln_bufs() {
  assert(upcxx::master_persona().active_with_caller());
  if (!kernel_alns.empty()) DIE("Clearing bufs but there are ", kernel_alns.size(), " alignments outstanding\n");
  kernel_alns.clear();
  ctg_seqs.clear();
  read_seqs.clear();
  if (alns) {
    kernel_alns.reserve(KLIGN_GPU_BLOCK_SIZE);
    ctg_seqs.reserve(KLIGN_GPU_BLOCK_SIZE);
    read_seqs.reserve(KLIGN_GPU_BLOCK_SIZE);
  }
  max_clen = 0;
  max_rlen = 0;
}  // clear_aln_bufs

void KlignAligner::flush_remaining() {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  LOG(__FILEFUNC__, "\n");
  BaseTimer t(__FILEFUNC__);
  assert(alns);
  t.start();
  auto fut_chain = make_future();
  auto &ntm = upcxx_utils::split_rank::node_team();
  assert(rget_requests.size() == ntm.rank_n());
  for (int i = 0; i < ntm.rank_n(); i++) {
    auto target = (ntm.rank_me() + i) % ntm.rank_n();
    progress();
    if (rget_requests[target].size()) {
      auto fut = do_rget_irregular(target, true);
      fut_chain = when_all(fut, fut_chain);
    }
  }
  flush_outstanding_futures();
  fut_chain.wait();
  barrier();  // to maintain full attention
  t.stop();
  auto num = kernel_alns.size();
  if (num) {
    submit_block();
  }
  progress();
}  // flush_remaining

// compute all seeded alignments for read.
// future may be forgotten and may not be ready until flush has been called and does not need to be explicitly waited
upcxx::future<> KlignAligner::compute_alns_for_read(CtgAndReadLocsMap &aligned_ctgs_map, const string &rname, const string &rseq_fw,
                                                    int read_group_id, int rget_buf_size) {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  assert(read_group_id != -1);
  int rlen = rseq_fw.length();
  string rseq_rc;
  string tmp_ctg;
  string rseq;
  future<> fut_chain = make_future();
  for (auto &ctg_and_read_locs : aligned_ctgs_map) {
    vector<CtgAndReadLoc> &locs = ctg_and_read_locs.second.locs;
    auto seq_gptr = ctg_and_read_locs.second.seq_gptr;
    auto clen = ctg_and_read_locs.second.clen;
    auto cid = ctg_and_read_locs.second.cid;
    auto depth = ctg_and_read_locs.second.depth;
    if (locs.empty()) continue;
    // sort list of positions in ctg so overlaps can be filtered when processing alignments
    sort(locs.begin(), locs.end(), [](CtgAndReadLoc c1, CtgAndReadLoc c2) {
      if (c1.pos_in_ctg == c2.pos_in_ctg) return c1.pos_in_read < c2.pos_in_read;
      return c1.pos_in_ctg < c2.pos_in_ctg;
    });
    int prev_cstart = -1;
    for (auto &ctg_and_read_loc : locs) {
      progress();
      auto pos_in_ctg = ctg_and_read_loc.pos_in_ctg;
      bool ctg_is_rc = ctg_and_read_loc.ctg_is_rc;
      auto pos_in_read = ctg_and_read_loc.pos_in_read;
      bool read_is_rc = ctg_and_read_loc.read_is_rc;
      if (pos_in_ctg >= clen || pos_in_read >= rlen)
        DIE("pos_in_ctg ", pos_in_ctg, " ", clen, " pos_in_read ", pos_in_read, " rlen ", rlen);

      char orient = '+';
      if (ctg_is_rc != read_is_rc) {
        // it's revcomp in either contig or read, but not in both or neither
        orient = '-';
        pos_in_read = rlen - (kmer_len + pos_in_read);
      }
      auto [cstart, rstart, overlap_len] = get_start_positions(kmer_len, clen, pos_in_ctg, pos_in_read, rlen);
      assert(cstart >= 0 && cstart + overlap_len <= clen);
      assert(overlap_len <= 2 * rlen);
      // has this alignment already been calculated?
      if (prev_cstart == cstart) continue;
      prev_cstart = cstart;
      if (ctg_is_rc != read_is_rc) {
        if (rseq_rc.empty()) rseq_rc = revcomp(rseq_fw);
        rseq = rseq_rc;
      } else {
        rseq = rseq_fw;
      }
      bool on_node = seq_gptr.is_local();
#ifdef DEBUG
      //     test both on node and off node on a single node
      if (on_node && local_team().rank_n() == rank_n()) on_node = (seq_gptr.where() % 2) == (rank_me() % 2);
#endif
      if (on_node) {
        // on same node already
        string_view ctg_seq = string_view(seq_gptr.local(), clen);
        assert(master_persona().active_with_caller());
        auto fut = align_read(rname, cid, rseq, ctg_seq, rstart, cstart, orient, overlap_len, read_group_id);
#ifdef DEBUG
        // track every alignment (takes mem)
        if (!fut.is_ready()) fut_chain = when_all(fut_chain, fut);
#endif
        local_ctg_fetches++;
      } else {
        auto target = seq_gptr.where() / local_team().rank_n();
        if (pos_in_ctg >= clen) DIE(" pos_in_ctg ", pos_in_ctg, " clen ", clen);
        CtgLoc ctg_loc({cid, seq_gptr, clen, depth, pos_in_ctg, ctg_is_rc});
        auto &tgt_rget_request = rget_requests[target];
#ifdef DEBUG
        // track every alignment (takes mem)
        fut_chain = when_all(fut_chain, tgt_rget_request.prom.get_future());
#endif
        tgt_rget_request.push_back({ctg_loc, rname, rseq, rstart, cstart, orient, overlap_len, read_group_id});
        if (tgt_rget_request.size() == rget_buf_size) {
          auto fut = do_rget_irregular(target, true);
          assert(tgt_rget_request.empty());
          fut_chain = when_all(fut_chain, fut);
        }
        remote_ctg_fetches++;
      }
    }
  }
  aligned_ctgs_map.clear();  // free memory
#ifdef DEBUG
  // track every alignment (takes mem)
  if (!fut_chain.is_ready()) pending_alignments_fut = when_all(pending_alignments_fut, fut_chain);
#endif
  return fut_chain;
}  // compute_alns_for_read

void KlignAligner::finish_alns() {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  LOG(__FILEFUNC__, "\n");
  flush_outstanding_futures();
  for (auto &rget : rget_requests)
    if (!rget.empty()) DIE("finish_alns must be called after flush_remaining!  There are pending rget_requests");
  if (!kernel_alns.empty()) DIE("finish_aln called while alignments are still pending to be processed - ", kernel_alns.size());
  while (!active_kernel_fut.is_ready()) {
    LOG("Waiting for outstanding kernel\n");
    wait_wrapper(active_kernel_fut);
  }
  while (!pending_alignments_fut.is_ready()) {
    LOG("Waiting for pending alignments to complete\n");
    wait_wrapper(pending_alignments_fut);
  }

  assert(active_kernel_fut.is_ready());
  DBG("Completed finish_alns\n");
}  // finish_alns

void KlignAligner::log_ctg_bytes_fetched() {
  assert(!upcxx::in_progress());
  assert(upcxx::master_persona().active_with_caller());
  DBG(__FILEFUNC__, "\n");
  auto &pr = Timings::get_promise_reduce();
  auto fut_reduce = when_all(pr.reduce_one(rget_calls, op_fast_add, 0), pr.reduce_one(ctg_bytes_fetched, op_fast_add, 0),
                             pr.reduce_one(ctg_bytes_fetched, op_fast_max, 0), pr.reduce_one(local_ctg_fetches, op_fast_add, 0),
                             pr.reduce_one(remote_ctg_fetches, op_fast_add, 0), pr.reduce_one(large_rget_calls, op_fast_add, 0),
                             pr.reduce_one(large_rget_bytes, op_fast_add, 0));

  auto fut_report = fut_reduce.then([](auto all_rget_calls, auto all_ctg_bytes_fetched, auto max_ctg_bytes_fetched,
                                       auto all_local_ctg_fetches, auto all_remote_ctg_fetches, auto all_large_rget_calls,
                                       auto all_large_rget_bytes) {
    if (all_ctg_bytes_fetched > 0)
      SLOG_VERBOSE("Contig bytes fetched ", get_size_str(all_ctg_bytes_fetched), " balance ",
                   (double)all_ctg_bytes_fetched / (rank_n() * max_ctg_bytes_fetched), " average rget size ",
                   get_size_str(all_ctg_bytes_fetched / all_rget_calls), ", large_rgets ", all_large_rget_calls, " avg ",
                   get_size_str(all_large_rget_calls > 0 ? all_large_rget_bytes / all_large_rget_calls : 0), "\n");

    if (all_local_ctg_fetches > 0)
      SLOG_VERBOSE("Local contig fetches ", perc_str(all_local_ctg_fetches, all_local_ctg_fetches + all_remote_ctg_fetches), "\n");
  });
  Timings::set_pending(fut_report);
  rget_calls = 0;
  large_rget_calls = 0;
  large_rget_bytes = 0;
  local_ctg_fetches = 0;
  remote_ctg_fetches = 0;
  ctg_bytes_fetched = 0;
}  // log_ctg_bytes_fetched

void KlignAligner::sort_alns_batch(Alns &batch_alns, KlignTimers &timers, const string &reads_fname) {
  LOG("Sorting ", batch_alns.size(), " alignments for ", reads_fname, "\n");
  timers.sort_t.start();
  batch_alns.sort_alns();
  timers.sort_t.stop();
  LOG("Finished sorting ", batch_alns.size(), " alignments\n");
}  // sort_alns_batch
