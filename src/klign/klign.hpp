#pragma once

/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include <memory>
#include <upcxx/upcxx.hpp>

#include "alignments.hpp"
#include "aligner.hpp"
#include "contigs.hpp"
#include "kmer.hpp"
#include "packed_reads.hpp"
#include "utils.hpp"

#include "upcxx_utils/timers.hpp"

using std::shared_ptr;
using upcxx::global_ptr;

#include "upcxx_utils/timers.hpp"

template <int MAX_K>
struct KmerAndCtgLoc {
  Kmer<MAX_K> kmer;
  CtgLoc ctg_loc;
  UPCXX_SERIALIZED_FIELDS(kmer, ctg_loc);
};

struct CtgLocAndKmerIdx {
  CtgLoc ctg_loc;
  int kmer_i;
};

struct ReadRecord {
  int64_t index : 40;
  int64_t rlen : 24;

  CtgAndReadLocsMap aligned_ctgs_map;

  ReadRecord()
      : index(-1)
      , rlen(0)
      , aligned_ctgs_map{} {}

  ReadRecord(int index, int rlen)
      : index(index)
      , rlen(rlen)
      , aligned_ctgs_map{} {}

  bool is_valid() const { return index >= 0 && rlen > 0; }
};  // struct ReadRecord

struct ReadRecordPtr {
  ReadRecord *read_record;
  uint16_t read_offset : 15;  // max 32kb read!
  bool is_rc : 1;
};  // struct ReadRecordPtr

template <int MAX_K>
struct KmersReadsBuffer {
  vector<Kmer<MAX_K>> kmers;
  vector<ReadRecordPtr> read_records;

  void add(const Kmer<MAX_K> &kmer, ReadRecord *read_record, uint16_t read_offset, bool read_is_rc) {
    kmers.push_back(kmer);
    read_records.push_back({read_record, read_offset, read_is_rc});
  }

  size_t size() const { return kmers.size(); }

  static size_t get_size_per() { return sizeof(Kmer<MAX_K>) + sizeof(ReadRecordPtr); }

  void clear() {
    LOG("Clearing KmersReadsBuffer with ", size(), " entries\n");
    kmers.clear();
    kmers = {};
    read_records.clear();
    read_records = {};
  }

  bool empty() const { return kmers.empty(); }
  void reserve(size_t sz) {
    kmers.reserve(sz);
    read_records.reserve(sz);
  }
};  // struct KmersReadsBuffer

struct AlignReadPackage {
  const PackedReads *packed_reads;
  vector<ReadRecord> *read_records;
  Alns *alns;
  int read_group_id;
};
using AlignReadPackages = std::vector<AlignReadPackage>;

template <int MAX_K>
struct KmerCtg {
  // struct to hold all the dist_object data. swappable
  using local_kmer_map_t = HASH_TABLE<Kmer<MAX_K>, SingleOrMultipleCtgLocs>;
  local_kmer_map_t kmer_map;
  GlobalContigs global_ctg_seqs;
  int64_t num_dropped_seed_to_ctgs;

  // only move operations allowed
  KmerCtg() = default;
  KmerCtg(const KmerCtg &copy) = delete;
  KmerCtg(KmerCtg &&move) = default;
  KmerCtg &operator=(const KmerCtg &copy) = delete;
  KmerCtg &operator=(KmerCtg &&move) = default;
  ~KmerCtg() = default;

  void swap(KmerCtg &other) {
    KmerCtg tmp(std::move(other));
    other = std::move(*this);
    *this = std::move(tmp);
  }

  bool empty() const { return kmer_map.empty() && global_ctg_seqs.empty(); }
  global_ptr<char> add_ctg_seq(string seq) { return global_ctg_seqs.add_ctg_seq(seq); }
  void reserve_ctgs(size_t sz) { global_ctg_seqs.reserve(sz); }
  void reserve_kmers(size_t sz) { kmer_map.reserve(sz); }
};

template <int MAX_K>
class KmerCtgDHT;

template <int MAX_K>
std::pair<double, double> find_alignments(unsigned kmer_len, PackedReadsList &packed_reads_list, int max_store_size,
                                          int max_rpcs_in_flight, Contigs &ctgs, Alns &alns, int seed_space, int rlen_limit,
                                          bool use_blastn_scores, int min_ctg_len, int rget_buf_size);

template <int MAX_K>
shared_ptr<KmerCtgDHT<MAX_K>> construct_kmer_ctg_dht(unsigned, int, int, size_t, bool);

template <int MAX_K>
void build_kmer_ctg_dht(KmerCtgDHT<MAX_K> &, Contigs &, int);

template <int MAX_K>
shared_ptr<KmerCtgDHT<MAX_K>> build_kmer_ctg_dht(unsigned, int, int, Contigs &, int, bool);

template <int MAX_K>
void compute_alns(AlignReadPackages &arps, KlignAligner &, int64_t, int, KlignTimers &);

template <int MAX_K>
size_t allocate_kmers_reads_buffers(vector<KmersReadsBuffer<MAX_K>> &);

template <int MAX_K>
void fetch_ctg_maps(KmerCtgDHT<MAX_K> &, AlignReadPackages &arps, int, vector<KmersReadsBuffer<MAX_K>> &, KlignTimers &);

// Reduce compile time by instantiating templates of common types
// extern template declarations are in kmer.hpp
// template instantiations each happen in src/CMakeLists via kmer-extern-template.in.cpp

#define __MACRO_KLIGN__(KMER_LEN, MODIFIER)                                                                                        \
  MODIFIER std::pair<double, double> find_alignments<KMER_LEN>(unsigned, PackedReadsList &, int, int, Contigs &, Alns &, int, int, \
                                                               bool, int, int);                                                    \
  MODIFIER shared_ptr<KmerCtgDHT<KMER_LEN>> construct_kmer_ctg_dht(unsigned, int, int, size_t, bool);                              \
  MODIFIER void build_kmer_ctg_dht(KmerCtgDHT<KMER_LEN> &, Contigs &, int);                                                        \
  MODIFIER shared_ptr<KmerCtgDHT<KMER_LEN>> build_kmer_ctg_dht<KMER_LEN>(unsigned, int, int, Contigs &, int, bool);                \
  MODIFIER void compute_alns<KMER_LEN>(AlignReadPackages & arps, KlignAligner &, int64_t, int, KlignTimers &);                     \
  MODIFIER size_t allocate_kmers_reads_buffers<KMER_LEN>(vector<KmersReadsBuffer<KMER_LEN>> &);                                    \
  MODIFIER void fetch_ctg_maps<KMER_LEN>(KmerCtgDHT<KMER_LEN> &, AlignReadPackages & arps, int,                                    \
                                         vector<KmersReadsBuffer<KMER_LEN>> &, KlignTimers &);

__MACRO_KLIGN__(32, extern template);
#if MAX_BUILD_KMER >= 64
__MACRO_KLIGN__(64, extern template);
#endif
#if MAX_BUILD_KMER >= 96
__MACRO_KLIGN__(96, extern template);
#endif
#if MAX_BUILD_KMER >= 128
__MACRO_KLIGN__(128, extern template);
#endif
#if MAX_BUILD_KMER >= 160
__MACRO_KLIGN__(160, extern template);
#endif
