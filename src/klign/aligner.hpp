#pragma once

/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include <tuple>
#include <variant>
#include <algorithm>
#include <deque>

#include <upcxx/upcxx.hpp>

#include "alignments.hpp"
#include "contigs.hpp"
#include "aligner_cpu.hpp"
#include "utils.hpp"

#include "upcxx_utils/log.hpp"
#include "upcxx_utils/split_rank.hpp"
#include "upcxx_utils/limit_outstanding.hpp"

struct KlignTimers;

// methods linked in by CMake where cpu vs gpu version is determined
// implementations located in block_aln_cpu.cpp and block_aln_gpu.cpp

upcxx::future<ShAlignBlockData> kernel_align_block(CPUAligner &cpu_aligner, ShAlignBlockData aln_block_data,
                                                   KlignTimers &klign_timers);
void init_aligner(int match_score, int mismatch_penalty, int gap_opening_penalty, int gap_extending_penalty, int ambiguity_penalty,
                  uint16_t rlen_limit, bool compute_cigar);
void cleanup_aligner();

struct KlignTimers {
  upcxx_utils::IntermittentTimer fetch_ctg_maps, compute_alns, rget_ctg_seqs, aln_kernel, aln_kernel_mem, aln_kernel_block,
      aln_kernel_launch, sort_t, get_ctgs_with_kmers_rt_t;

  static upcxx_utils::IntermittentTimer &get_ctgs_with_kmers_master_t() {
    static upcxx_utils::IntermittentTimer _("klign: get_ctgs_with_kmers (inner master)");
    return _;
  }
  static upcxx_utils::IntermittentTimer &get_ctgs_with_kmers_tp_t() {
    static upcxx_utils::IntermittentTimer _("klign: get_ctgs_with_kmers (inner tp)");
    return _;
  }

  KlignTimers()
      : fetch_ctg_maps("klign: fetch ctg maps")
      , compute_alns("klign: compute alns")
      , rget_ctg_seqs("klign: rget ctg seqs")
      , aln_kernel("klign: aln kernel")
      , aln_kernel_mem("klign: aln kernel mem")
      , aln_kernel_block("klign: aln kernel block")
      , aln_kernel_launch("klign: aln kernel launch")
      , sort_t("klign: sort alns")
      , get_ctgs_with_kmers_rt_t("klign: get_ctgs_with_kmers (round trip)") {}

  void done_all() {
    fetch_ctg_maps.done_all_async();
    compute_alns.done_all_async();
    rget_ctg_seqs.done_all_async();
    aln_kernel.done_all_async();
    aln_kernel_mem.done_all_async();
    aln_kernel_block.done_all_async();
    aln_kernel_launch.done_all_async();
    sort_t.done_all_async();
    get_ctgs_with_kmers_rt_t.done_all_async();
    KlignTimers::get_ctgs_with_kmers_master_t().done_all_async();
    KlignTimers::get_ctgs_with_kmers_tp_t().done_all_async();
  }

  void clear() {
    fetch_ctg_maps.clear();
    compute_alns.clear();
    rget_ctg_seqs.clear();
    aln_kernel.clear();
    aln_kernel_mem.clear();
    aln_kernel_block.clear();
    aln_kernel_launch.clear();
    sort_t.clear();
    get_ctgs_with_kmers_rt_t.clear();
    KlignTimers::get_ctgs_with_kmers_master_t().clear();
    KlignTimers::get_ctgs_with_kmers_tp_t().clear();
  }
};  // struct KlignTimers

struct CtgAndReadLoc {
  uint32_t pos_in_ctg : 31;  // pack int 31 bits
  uint32_t ctg_is_rc : 1;    // bool
  uint32_t cstart;
  uint32_t pos_in_read : 15;  // pack into 15 bits // max 32kb read!
  uint32_t read_is_rc : 1;    // bool
};

struct CtgAndReadLocs {
  cid_t cid;
  global_ptr<char> seq_gptr;
  uint32_t clen;
  float depth;
  vector<CtgAndReadLoc> locs;
};

using CtgAndReadLocsMap = HASH_TABLE<cid_t, CtgAndReadLocs>;

struct CtgLoc {
  cid_t cid;
  upcxx::global_ptr<char> seq_gptr;
  uint32_t clen;
  float depth;
  uint32_t pos : 31;   // pack int 31 bits
  uint32_t is_rc : 1;  // bool

  string to_string() const {
    return upcxx_utils::make_string("CtgLoc(cid=", cid, ",clen=", clen, ",depth=", depth, ",pos=", pos, ",is_rc=", is_rc,
                                    ",seq_gptr=", seq_gptr, ")");
  }

  // sort by > length, then > depth, then < cid, then >pos
  static bool cmp(const CtgLoc &a, const CtgLoc &b) {
    if (a.clen != b.clen) {
      return a.clen > b.clen;
    } else if (a.depth != b.depth) {
      return a.depth > b.depth;
    } else if (a.cid != b.cid) {
      return a.cid < b.cid;
    } else if (a.pos != b.pos) {
      return a.pos > b.pos;
    } else {
      assert(a.is_rc != b.is_rc && "no duplicates CtgLocs!");
      return a.is_rc;
    }
  }  // cmp
};

struct RgetRequest {
  CtgLoc ctg_loc;
  string rname;
  string read_seq;
  uint32_t rstart;
  uint32_t cstart;
  char orient;
  int overlap_len;
  int read_group_id;
};

using CtgLocVector = vector<CtgLoc>;
using SingleOrMultipleCtgLocs = std::variant<CtgLoc, CtgLocVector>;

struct GlobalContigs {
  using global_contigs_t = vector<global_ptr<char>>;
  global_contigs_t global_ctg_seqs;

  // only move operations allowed
  GlobalContigs() = default;
  GlobalContigs(const GlobalContigs &copy) = delete;
  GlobalContigs(GlobalContigs &&move) = default;
  GlobalContigs &operator=(const GlobalContigs &copy) = delete;
  GlobalContigs &operator=(GlobalContigs &&move) = default;
  ~GlobalContigs();  // calls free_global_contigs

  void free_global_contigs();
  global_ptr<char> add_ctg_seq(string seq);
  bool empty() const;
  void reserve(size_t);
};

template <typename T>
class PromVector : public vector<T> {
 public:
  upcxx::promise<> prom;
};

class KlignAligner {
  std::atomic<int64_t> num_alns;
  std::atomic<int64_t> num_perfect_alns;
  int kmer_len;

  vector<Aln> kernel_alns;
  vector<string> ctg_seqs;
  vector<string> read_seqs;

  upcxx::future<> active_kernel_fut;
  upcxx::future<> pending_alignments_fut;

  uint32_t max_clen = 0;
  uint16_t max_rlen = 0;
  CPUAligner cpu_aligner;

  int64_t ctg_bytes_fetched = 0;
  int64_t rget_calls = 0;
  int64_t large_rget_calls = 0;
  int64_t large_rget_bytes = 0;
  int64_t local_ctg_fetches = 0;
  int64_t remote_ctg_fetches = 0;

  Alns *alns;
  KlignTimers &klign_timers;
  shared_ptr<upcxx::promise<>> sh_next_block_done;

 public:
  using target_rank_rget_requests_t = PromVector<RgetRequest>;
  using rget_requests_t = vector<target_rank_rget_requests_t>;
  using sh_rget_requests_t = shared_ptr<rget_requests_t>;
  using rget_pair_t = pair<global_ptr<char>, size_t>;
  using ctg_pair_t = pair<char *, size_t>;

 private:
  sh_rget_requests_t _sh_rget_requests;  // private or singleton if initialized
  rget_requests_t &rget_requests;

  // alns2 here is for a thread-safety option, potentially independent from the current alns pointer
  future<> align_read(const string &rname, int64_t cid, const string_view &rseq, const string_view &ctg_seq, int rstart, int cstart,
                      char orient, int overlap_len, int read_group_id, Alns &dest_alns);
  future<> align_read(const string &rname, int64_t cid, const string_view &rseq, const string_view &ctg_seq, int rstart, int cstart,
                      char orient, int overlap_len, int read_group_id) {
    return align_read(rname, cid, rseq, ctg_seq, rstart, cstart, orient, overlap_len, read_group_id, *alns);
  };
  void clear();

  future<> prep_do_rget_irregular(std::deque<rget_pair_t> &src, std::deque<ctg_pair_t> &dest,
                                  target_rank_rget_requests_t &tgt_rget_req, HASH_TABLE<cid_t, string> &ctgs_fetched);
  future<> do_rget_irregular(int target, bool is_node = false);
  upcxx::future<> process_rget_alignments(shared_ptr<target_rank_rget_requests_t> sh_pending_requests,
                                          shared_ptr<HASH_TABLE<cid_t, string>> sh_ctgs_fetched);

  template <typename SrcIter, typename DestIter>
  future<> rget_irregular_by_node(SrcIter src_begin, SrcIter src_end, DestIter dest_begin, DestIter dest_end) {
    using GU = typename std::tuple_element<0, typename std::iterator_traits<SrcIter>::value_type>::type;
    using U = typename GU::element_type;
    using T = typename std::remove_const<U>::type;
    using D = typename std::tuple_element<0, typename std::iterator_traits<DestIter>::value_type>::type;

    static_assert(upcxx::is_trivially_serializable<T>::value, "RMA operations only work on TriviallySerializable types.");

    static_assert(std::is_convertible<D, const T *>::value, "SrcIter and DestIter need to be over same base T type");

    using VoV = vector<vector<T>>;

    // verify all global_ptr are from the same node
    int src_node = -1;
    auto dest_i = dest_begin;
    for (auto src_i = src_begin; src_i != src_end; src_i++, dest_i++) {
      GU &gptr = (*src_i).first;
      auto node = gptr.where() / local_team().rank_n();
      if (src_node == -1)
        src_node = node;
      else if (src_node != node)
        DIE("rget_irregular_by_node called with pointer to multiple nodes! ", src_node, " for ", (*src_begin).first, " vs ", node,
            " for ", gptr, "\n");
      if ((*src_i).second != (*dest_i).second) DIE("Inconsistent sizes ", (*src_i).second, " vs ", (*dest_i).second, "\n");
    }
    if (src_node == -1) {
      WARN("rget_irregular called with no src pointers to get!\n");
      return make_future();
    }

    // rpc to remote node
    auto fut_result = rpc(
        upcxx_utils::split_rank::node_team(), src_node,
        [](const view<pair<GU, size_t>> &src_view) -> VoV {
          VoV results;
          results.reserve(src_view.size());
          for (auto &gptr_sz : src_view) {
            auto &[gptr, sz] = gptr_sz;
            assert(gptr.is_local());
            results.emplace_back(gptr.local(), gptr.local() + sz);
          }
          return results;
        },
        make_view(src_begin, src_end));

    // complete copy to the local dest
    auto fut_ret = fut_result.then([=](const VoV &vov) {
      auto dest_i = dest_begin;
      for (auto &v : vov) {
        if (dest_i == dest_end)
          DIE("Different sizes of vectors src ", std::distance(src_begin, src_end), " dest ", std::distance(dest_begin, dest_end));
        auto &d = *dest_i;
        auto &dp = d.first;
        auto &dsz = d.second;
        if (dsz != v.size()) DIE("Mismatch in src and dest sizes ", v.size(), " vs ", dsz);
        std::copy(v.begin(), v.end(), dp);
        dest_i++;
      }
      if (dest_i != dest_end) DIE("Did not get all the results");
    });

    if (!upcxx::in_progress()) upcxx_utils::limit_outstanding_futures(fut_ret).wait();

    return fut_ret;
  };  // rget_irregular_by_node

 public:
  KlignAligner(int kmer_len, int rlen_limit, bool report_cigar, bool use_blastn_scores, KlignTimers &timers);
  KlignAligner(const KlignAligner &copy) = delete;
  KlignAligner(KlignAligner &&move) = default;
  KlignAligner &operator=(const KlignAligner &copy) = delete;
  KlignAligner &operator=(KlignAligner &&move) = default;

  ~KlignAligner();
  void reset();

  void submit_block(Alns &dest_alns);
  void submit_block() { submit_block(*alns); }

  static sh_rget_requests_t &get_singleton_rget_requests() {
    static sh_rget_requests_t _{};
    return _;
  }

  void set_alns(Alns *alns = nullptr);

  static std::tuple<uint32_t, uint16_t, uint16_t> get_start_positions(int kmer_len, uint32_t clen, uint32_t pos_in_ctg,
                                                                      uint16_t pos_in_read, uint16_t rlen);

  upcxx::future<int64_t> fut_get_num_perfect_alns(bool all = false);

  future<int64_t> fut_get_num_alns(bool all = false);

  void clear_aln_bufs();

  void flush_remaining();

  upcxx::future<> compute_alns_for_read(CtgAndReadLocsMap &aligned_ctgs_map, const string &rname, const string &rseq_fw,
                                        int read_group_id, int rget_buf_size);

  void finish_alns();

  void log_ctg_bytes_fetched();

  static void sort_alns_batch(Alns &batch_alns, KlignTimers &timers, const string &reads_fname);

};  // class KlignAligner
