/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include "klign.hpp"
#include "aligner.hpp"
#include "kmer.hpp"
#include "aligner_cpu.hpp"

#include "gpu-utils/gpu_utils.hpp"
#include "adept-sw/driver.hpp"

//#define NO_KLIGN_CPU_WORK_STEAL  // work steal works again after upcxx-utils 2f2a28ee41
//#define KLIGN_NO_THREAD_POOL     // threads work again after upcxx-utils 2f2a28ee41

using namespace std;
using namespace upcxx;
using namespace upcxx_utils;

static adept_sw::GPUDriver *gpu_driver;

static upcxx::future<> gpu_align_block(shared_ptr<AlignBlockData> aln_block_data, bool report_cigar, KlignTimers &klign_timers) {
  assert(upcxx::master_persona().active_with_caller());
  // may be called from within progress
  static upcxx::future<> fut_kernel_submission = make_future();

  auto lambda = [aln_block_data, report_cigar, &klign_timers]() {
    DBG("Starting gpu_align_block_kernel of ", aln_block_data->kernel_alns.size(), "\n");

    unsigned maxContigSize = aln_block_data->max_clen;
    unsigned maxReadSize = aln_block_data->max_rlen;
    auto &aln_kernel_timer = klign_timers.aln_kernel;
    auto &block_timer = klign_timers.aln_kernel_block;
    double launch_time = 0, mem_time = 0;
    unsigned maxCIGAR = (maxContigSize > maxReadSize) ?
                            3 * maxContigSize :
                            3 * maxReadSize;  // 3* size to eliminate overflow FIXME: Truncate CIGARs that are over maxCIGAR
    // printf("GPU align block: maxContigSize passed in = %d, maxReadSize passed in = %d\n", maxContigSize, maxReadSize);
    if (report_cigar) {
      aln_kernel_timer.start();

      gpu_driver->run_kernel_traceback(aln_block_data->read_seqs, aln_block_data->ctg_seqs, aln_block_data->max_rlen,
                                       aln_block_data->max_clen, launch_time, mem_time);
      block_timer.start();
      gpu_driver->kernel_block_fwd();
      block_timer.stop();

      aln_kernel_timer.stop();
      klign_timers.aln_kernel_launch += launch_time;
      klign_timers.aln_kernel_mem += mem_time;
    } else {
      aln_kernel_timer.start();

      // align query_seqs, ref_seqs, max_query_size, max_ref_size
      gpu_driver->run_kernel_forwards(aln_block_data->read_seqs, aln_block_data->ctg_seqs, aln_block_data->max_rlen,
                                      aln_block_data->max_clen, launch_time, mem_time);
      block_timer.start();
      gpu_driver->kernel_block_fwd();
      block_timer.stop();
      gpu_driver->run_kernel_backwards(aln_block_data->read_seqs, aln_block_data->ctg_seqs, aln_block_data->max_rlen,
                                       aln_block_data->max_clen, launch_time, mem_time);
      block_timer.start();
      gpu_driver->kernel_block_rev();
      block_timer.stop();

      aln_kernel_timer.stop();
      klign_timers.aln_kernel_launch += launch_time;
      klign_timers.aln_kernel_mem += mem_time;
    }

    auto aln_results = gpu_driver->get_aln_results();
    aln_block_data->final_alns.reserve(aln_block_data->final_alns.size() + aln_block_data->kernel_alns.size());

    for (int i = 0; i < aln_block_data->kernel_alns.size(); i++) {
      Aln &aln = aln_block_data->kernel_alns[i];
      // FIXME: need to get the second best score
      // FIXME: need to get the number of mismatches
      aln.set(aln_results.ref_begin[i], aln_results.ref_end[i], aln_results.query_begin[i], aln_results.query_end[i],
              aln_results.top_scores[i], 0, 0, aln.read_group_id);
      if (report_cigar) {
        // std::string cig = "GPU_CIGAR: "; //use this to calculate which percentage of traceback is being done on GPU
        std::string cig = "";
        int k = i * maxCIGAR;
        while (aln_results.cigar[k]) {
          cig += aln_results.cigar[k];
          k++;
        }
        aln.set_sam_string(cig);
        // cig = "GPU_CIGAR: "; //use this to calculate which percentage of traceback is being done on GPU
        cig = "";
      }
      aln_block_data->final_alns.add_aln(aln);
    }
    aln_block_data->clear_inputs();
  };  // gpu_align_block::lambda
#ifndef KLIGN_NO_THREAD_POOL
  fut_kernel_submission = upcxx_utils::execute_after_in_thread_pool(fut_kernel_submission, lambda);
#else
  progress();
  lambda();
  progress();
#endif
  return fut_kernel_submission;
}  // gpu_align_block

void init_aligner(int match_score, int mismatch_penalty, int gap_opening_penalty, int gap_extending_penalty, int ambiguity_penalty,
                  uint16_t rlen_limit, bool compute_cigar) {
  if (!gpu_utils::gpus_present()) {
    // CPU only
    SWARN("No GPU will be used for alignments");
  } else {
    double init_time;
    if (gpu_driver) {
      if (gpu_driver->is_same_config(local_team().rank_me(), (short)match_score, (short)-mismatch_penalty,
                                     (short)-gap_opening_penalty, (short)-gap_extending_penalty, rlen_limit, compute_cigar)) {
        LOG("Reusing existing GPU adept_sw driver");
      } else {
        LOG("Deleting old gpu driver\n");
        delete gpu_driver;
        gpu_driver = nullptr;
      }
    }
    if (!gpu_driver) {
      gpu_driver =
          new adept_sw::GPUDriver(local_team().rank_me(), local_team().rank_n(), (short)match_score, (short)-mismatch_penalty,
                                  (short)-gap_opening_penalty, (short)-gap_extending_penalty, rlen_limit, compute_cigar, init_time);
      SLOG_VERBOSE("Initialized GPU adept_sw driver in ", init_time, " s\n");
    }
  }
}  // init_aligner

void cleanup_aligner() {
  if (gpu_utils::gpus_present() && gpu_driver) {
    LOG("Deleting gpu_driver\n");
    delete gpu_driver;
  }
  gpu_driver = nullptr;
}  // cleanup_aligner

upcxx::future<ShAlignBlockData> kernel_align_block(CPUAligner &cpu_aligner, ShAlignBlockData aln_block_data,
                                                   KlignTimers &klign_timers) {
  assert(upcxx::initialized() && upcxx::master_persona().active_with_caller());
  // may be called from within progress
  BaseTimer steal_t("CPU work steal");
  auto &kernel_alns = aln_block_data->kernel_alns;
  auto &ctg_seqs = aln_block_data->ctg_seqs;
  auto &read_seqs = aln_block_data->read_seqs;
  auto num = kernel_alns.size();
  auto sh_ws_alns = make_shared<Alns>();
  static upcxx::future<> kab_serialize_fut = make_future();

#ifndef NO_KLIGN_CPU_WORK_STEAL
  if (!upcxx::in_progress() && !kab_serialize_fut.is_ready()) {
    steal_t.start();
    // steal work from this kernel block if the previous kernel is still active
    // if true, this balances the block size that will be sent to the kernel
    while ((!gpu_utils::gpus_present() || !kab_serialize_fut.is_ready()) && !kernel_alns.empty()) {
      assert(!ctg_seqs.empty());
      assert(!read_seqs.empty());
      // steal one from the block
      cpu_aligner.ssw_align_read(sh_ws_alns.get(), kernel_alns.back(), ctg_seqs.back(), read_seqs.back(),
                                 kernel_alns.back().read_group_id);
      kernel_alns.pop_back();
      ctg_seqs.pop_back();
      read_seqs.pop_back();
      progress();
    }  // while waiting and work to steal
    steal_t.stop();
  }
#endif

  auto steal_secs = steal_t.get_elapsed();
  if (num != kernel_alns.size()) {
    auto num_stole = num - kernel_alns.size();
    LOG("Stole from kernel block ", num_stole, " alignments in ", steal_secs, "s (",
        (steal_secs > 0 ? num_stole / steal_secs : 0.0), " aln/s), while waiting for previous block to complete",
        (kernel_alns.empty() ? " - THE ENTIRE BLOCK" : ""), "\n");
  } else if (steal_secs > 0.01) {
    LOG("Waited ", steal_secs, "s for previous block to complete\n");
  }
  if (!kernel_alns.empty()) {
    string kernel_label = make_string("Execution of ", gpu_utils::gpus_present() ? "gpu" : "ssw", "_align_block ", num,
                                      " aligns: ", (void *)aln_block_data.get());
    AsyncTimer t_kernel(kernel_label);
    auto lambda = [=, &cpu_aligner, &klign_timers]() -> upcxx::future<> {
      LOG("Started lambda ", kernel_label, "\n");  // FIXME remove
      assert(upcxx::initialized() && upcxx::master_persona().active_with_caller());
      t_kernel.start();
      future<> fut;
      if (gpu_utils::gpus_present()) {
        fut = gpu_align_block(aln_block_data, cpu_aligner.ssw_filter.report_cigar, klign_timers);
      } else {
        fut = cpu_aligner.ssw_align_block(aln_block_data, klign_timers.aln_kernel).then([aln_block_data](auto x) {
          assert(x == aln_block_data);
          assert(aln_block_data);
        });
      }
      LOG("alignments submitted lambda ", kernel_label, "\n");  // FIXME remove
      return fut.then([=]() {
        assert(upcxx::initialized() && upcxx::master_persona().active_with_caller());
        t_kernel.stop();
        DBG("Completed ", kernel_label, "\n");
        LOG("Completed lambda ", kernel_label, "\n");  // FIXME remove
      });
    };  // lambda
    if (!kab_serialize_fut.is_ready()) {
      DBG("block of ", num, " pending until last kernel completes\n");
      AsyncTimer t("waiting for previous kernel");
      t.start();
      kab_serialize_fut = kab_serialize_fut.then([=]() {
        t.stop();
        LOG("previous kernel completed. ready to start ", kernel_label, "\n");
      });
    }
    kab_serialize_fut = kab_serialize_fut.then(lambda);
    if (!sh_ws_alns->empty()) {
      LOG("Appending work-steal block of ", sh_ws_alns->size(), "\n");
      kab_serialize_fut = kab_serialize_fut.then([aln_block_data, sh_ws_alns]() {
        assert(aln_block_data);
        assert(sh_ws_alns);
        auto &ws_alns = *sh_ws_alns;
        auto invalid = 0;
        // add any work-stolen alignments back in reverse order
        for (auto it = ws_alns.rbegin(); it != ws_alns.rend(); it++) {
          if (it->is_valid())
            aln_block_data->final_alns.add_aln(*it);
          else
            invalid++;
        }
        LOG("appended work-steal block of ", sh_ws_alns->size(), " invalid=", invalid, "\n");
      });
    }
  }  // if kernel_alns !empty
  return when_all(kab_serialize_fut, aln_block_data);
}  // kernel_align_block
