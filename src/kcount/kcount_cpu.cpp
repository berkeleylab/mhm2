/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include "upcxx_utils.hpp"
#include "kcount.hpp"
#include "kmer_dht.hpp"
#include "fixed_hash_map.hpp"
#include "intX.hpp"

using namespace std;
using namespace upcxx;
using namespace upcxx_utils;

// #define SLOG_CPU_HT(...) SLOG(KLMAGENTA, __VA_ARGS__, KNORM)
#define SLOG_CPU_HT SLOG_VERBOSE

enum PASS_TYPE { PRELOAD_CTGS_PASS = 0, READ_KMERS_PASS = 1, CTG_KMERS_PASS = 2 };

template <int MAX_K>
struct SeqBlockInserter<MAX_K>::SeqBlockInserterState {
  int64_t bytes_kmers_sent = 0;
  int64_t bytes_supermers_sent = 0;
  int64_t num_kmers = 0;
  vector<Kmer<MAX_K>> kmers;
};

template <int MAX_K>
SeqBlockInserter<MAX_K>::SeqBlockInserter(int qual_offset, int minimizer_len) {
  state = new SeqBlockInserterState();
}

template <int MAX_K>
SeqBlockInserter<MAX_K>::~SeqBlockInserter() {
  if (state) delete state;
}

template <int MAX_K>
void SeqBlockInserter<MAX_K>::process_seq(string &seq, kmer_count_t depth, dist_object<KmerDHT<MAX_K>> &kmer_dht) {
  if (!depth) depth = 1;
  auto kmer_len = Kmer<MAX_K>::get_k();
  Kmer<MAX_K>::get_kmers(kmer_len, seq, state->kmers);
  for (size_t i = 0; i < state->kmers.size(); i++) {
    state->bytes_kmers_sent += sizeof(KmerAndExt<MAX_K>);
    Kmer<MAX_K> kmer_rc = state->kmers[i].revcomp();
    if (kmer_rc < state->kmers[i]) state->kmers[i] = kmer_rc;
  }

  Supermer supermer{.seq = seq.substr(0, kmer_len + 1), .count = (kmer_count_t)depth};
  auto prev_target_rank = kmer_dht->get_kmer_target_rank(state->kmers[1]);
  for (int i = 1; i < (int)(seq.length() - kmer_len); i++) {
    auto &kmer = state->kmers[i];
    auto target_rank = kmer_dht->get_kmer_target_rank(kmer);
    if (target_rank == prev_target_rank) {
      supermer.seq += seq[i + kmer_len];
    } else {
      state->bytes_supermers_sent += supermer.get_bytes();
      kmer_dht->add_supermer(supermer, prev_target_rank);
      supermer.seq = seq.substr(i - 1, kmer_len + 2);
      prev_target_rank = target_rank;
    }
  }
  if (supermer.seq.length() >= kmer_len + 2) {
    state->bytes_supermers_sent += supermer.get_bytes();
    kmer_dht->add_supermer(supermer, prev_target_rank);
  }
  state->num_kmers += seq.length() - 2 - kmer_len;
}

template <int MAX_K>
void SeqBlockInserter<MAX_K>::done_processing(dist_object<KmerDHT<MAX_K>> &kmer_dht) {
  auto tot_supermers_bytes_sent = reduce_one(state->bytes_supermers_sent, op_fast_add, 0).wait();
  auto tot_kmers_bytes_sent = reduce_one(state->bytes_kmers_sent, op_fast_add, 0).wait();
  SLOG_CPU_HT("Total bytes sent in compressed supermers ", get_size_str(tot_supermers_bytes_sent), " (compression is ", fixed,
              setprecision(3), (double)tot_kmers_bytes_sent / tot_supermers_bytes_sent, " over kmers)\n");
  auto avg_num_kmers = reduce_one(state->num_kmers, op_fast_add, 0).wait() / rank_n();
  SLOG_CPU_HT("Processed an average of ", avg_num_kmers, " kmers per rank\n");
}

struct ExtCounts {
  kmer_count_t count_A;
  kmer_count_t count_C;
  kmer_count_t count_G;
  kmer_count_t count_T;

  void set(uint16_t *counts) {
    count_A = counts[0];
    count_C = counts[1];
    count_G = counts[2];
    count_T = counts[3];
  }

  void set(uint32_t *counts) {
    count_A = static_cast<uint16_t>(counts[0]);
    count_C = static_cast<uint16_t>(counts[1]);
    count_G = static_cast<uint16_t>(counts[2]);
    count_T = static_cast<uint16_t>(counts[3]);
  }

  std::array<std::pair<char, int>, 4> get_sorted() {
    std::array<std::pair<char, int>, 4> counts = {std::make_pair('A', (int)count_A), std::make_pair('C', (int)count_C),
                                                  std::make_pair('G', (int)count_G), std::make_pair('T', (int)count_T)};
    std::sort(std::begin(counts), std::end(counts), [](const auto &elem1, const auto &elem2) {
      if (elem1.second == elem2.second)
        return elem1.first > elem2.first;
      else
        return elem1.second > elem2.second;
    });
    return counts;
  }

  bool is_zero() {
    if (count_A + count_C + count_G + count_T == 0) return true;
    return false;
  }

  kmer_count_t inc_with_limit(int count1, int count2) {
    count1 += count2;
    return std::min(count1, (int)std::numeric_limits<kmer_count_t>::max());
  }

  void inc(char ext, int count) {
    switch (ext) {
      case 'A': count_A = inc_with_limit(count_A, count); break;
      case 'C': count_C = inc_with_limit(count_C, count); break;
      case 'G': count_G = inc_with_limit(count_G, count); break;
      case 'T': count_T = inc_with_limit(count_T, count); break;
    }
  }

  void set(char ext, int count) {
    switch (ext) {
      case 'A': count_A = count; break;
      case 'C': count_C = count; break;
      case 'G': count_G = count; break;
      case 'T': count_T = count; break;
    }
  }

  void add(ExtCounts &ext_counts) {
    count_A = inc_with_limit(count_A, ext_counts.count_A);
    count_C = inc_with_limit(count_C, ext_counts.count_C);
    count_G = inc_with_limit(count_G, ext_counts.count_G);
    count_T = inc_with_limit(count_T, ext_counts.count_T);
  }

  char get_ext(kmer_count_t count) {
    auto sorted_counts = get_sorted();
    int top_count = sorted_counts[0].second;
    int runner_up_count = sorted_counts[1].second;
    // set dynamic_min_depth to 1.0 for single depth data (non-metagenomes)
    int dmin_dyn = std::max((int)((1.0 - DYN_MIN_DEPTH) * count), _dmin_thres);
    if (top_count < dmin_dyn) return 'X';
    if (runner_up_count >= dmin_dyn) return 'F';
    return sorted_counts[0].first;
  }

  string to_string() {
    ostringstream os;
    os << count_A << "," << count_C << "," << count_G << "," << count_T;
    return os.str();
  }
};

struct KmerExtsCounts {
  ExtCounts left_exts;
  ExtCounts right_exts;
  kmer_count_t count;
  bool from_ctg;

  char get_left_ext() { return left_exts.get_ext(count); }

  char get_right_ext() { return right_exts.get_ext(count); }
};

template <int MAX_K>
using KmersExtsCountsMap = FixedHashMap<Kmer<MAX_K>, KmerExtsCounts>;
// using a 32 bit key will reduce memory required in the singletons table at the cost of a slight increase in false positives
#ifndef SINGLETONS_KEY
#define SINGLETONS_KEY uint32_t
#endif
using SingletonsHashMap = FixedHashMap<SINGLETONS_KEY, uint8_t>;

template <int MAX_K>
static void get_kmers_and_exts(Supermer &supermer, vector<KmerAndExt<MAX_K>> &kmers_and_exts) {
  vector<bool> quals;
  quals.resize(supermer.seq.length());
  for (int i = 0; i < supermer.seq.length(); i++) {
    quals[i] = isupper(supermer.seq[i]);
    if (supermer.seq[i] >= 'a' && supermer.seq[i] <= 'z') supermer.seq[i] += ('A' - 'a');
  }
  auto kmer_len = Kmer<MAX_K>::get_k();
  vector<Kmer<MAX_K>> kmers;
  Kmer<MAX_K>::get_kmers(kmer_len, supermer.seq, kmers);
  kmers_and_exts.clear();
  for (int i = 1; i < (int)(supermer.seq.length() - kmer_len); i++) {
    Kmer<MAX_K> kmer = kmers[i];
    char left_ext = supermer.seq[i - 1];
    if (!quals[i - 1]) left_ext = '0';
    char right_ext = supermer.seq[i + kmer_len];
    if (!quals[i + kmer_len]) right_ext = '0';
    // get the lexicographically smallest
    Kmer<MAX_K> kmer_rc = kmer.revcomp();
    if (kmer_rc < kmer) {
      kmer = kmer_rc;
      swap(left_ext, right_ext);
      left_ext = comp_nucleotide(left_ext);
      right_ext = comp_nucleotide(right_ext);
    };
    kmers_and_exts.push_back({.kmer = kmer, .count = supermer.count, .left = left_ext, .right = right_ext});
  }
}

template <int MAX_K>
static void insert_supermer_from_read(Supermer &supermer, dist_object<KmersExtsCountsMap<MAX_K>> &kmers,
                                      dist_object<SingletonsHashMap> &singletons, bool filter_singletons) {
  auto char_to_bits = [](char c) -> uint8_t {
    switch (c) {
      case 'A': return 1;
      case 'C': return 2;
      case 'G': return 3;
      case 'T': return 4;
      case '0': return 5;
      default: return 0;
    }
  };

  auto bits_to_char = [](uint8_t b) -> char {
    switch (b) {
      case 1: return 'A';
      case 2: return 'C';
      case 3: return 'G';
      case 4: return 'T';
      case 5: return '0';
      default: return '\0';
    }
  };

  auto update_exts_counts = [](KmerExtsCounts &exts_counts, KmerAndExt<MAX_K> &kmer_and_ext) {
    int count = (int)exts_counts.count + kmer_and_ext.count;
    if (count > numeric_limits<kmer_count_t>::max()) count = numeric_limits<kmer_count_t>::max();
    exts_counts.count = count;
    exts_counts.left_exts.inc(kmer_and_ext.left, kmer_and_ext.count);
    exts_counts.right_exts.inc(kmer_and_ext.right, kmer_and_ext.count);
  };

  auto kmer_len = Kmer<MAX_K>::get_k();
  vector<KmerAndExt<MAX_K>> kmers_and_exts;
  kmers_and_exts.reserve(supermer.seq.length() - kmer_len);
  get_kmers_and_exts(supermer, kmers_and_exts);
  for (auto &kmer_and_ext : kmers_and_exts) {
    if (!kmer_and_ext.kmer.is_valid()) {
      WARN("Invalid kmer in insert_supermer_from_read\n");
      continue;
    }
    bool insert_in_kmers_ht = true;
    if (filter_singletons) {
      insert_in_kmers_ht = false;
      auto it_kmers = kmers->find(kmer_and_ext.kmer);
      if (it_kmers != kmers->end()) {
        auto [kmer, exts_counts] = *it_kmers;
        //  found in main hash table - update values
        update_exts_counts(exts_counts, kmer_and_ext);
      } else {
        // the singletons store the hashes of the kmers, not the kmers themselves
        SINGLETONS_KEY kmer_hash;
        if constexpr (sizeof(SINGLETONS_KEY) == 4)
          kmer_hash = kmer_and_ext.kmer.hash32();
        else
          kmer_hash = kmer_and_ext.kmer.hash();
        //  not found in main hash table, look in singletons
        auto it_singletons = singletons->find(kmer_hash);
        if (it_singletons != singletons->end()) {
          // found in singletons - add to main ht and erase from singletons
          auto [key, exts] = *it_singletons;
          auto [it, is_new] = kmers->insert({kmer_and_ext.kmer, {}});
          // no space - had to drop it
          if (it == kmers->end()) continue;
          assert(is_new);
          if (!is_new) DIE("Not new");
          auto &exts_counts = (*it).second;
          char left_ext = bits_to_char(exts & 7);
          char right_ext = bits_to_char(exts >> 4);
          // new addition to counts map - insert existing from singletons
          exts_counts.count = 1;
          exts_counts.left_exts.inc(left_ext, 1);
          exts_counts.right_exts.inc(right_ext, 1);
          update_exts_counts(exts_counts, kmer_and_ext);
          // remove from singletons ht
          singletons->erase(kmer_hash);
        } else {
          // not found in singletons - add it to singletons
          // pack extensions into byte value in singletons ht
          uint8_t exts = char_to_bits(kmer_and_ext.left) | (char_to_bits(kmer_and_ext.right) << 4);
          auto [it, is_new] = singletons->insert({kmer_hash, exts});
          if (it == singletons->end()) {
            // no space - insert into main hash table
            insert_in_kmers_ht = true;
          } else {
            if (!is_new) DIE("expected new insertion for ", kmer_and_ext.kmer);
            assert(is_new);
          }
        }
      }
    }
    if (insert_in_kmers_ht) {
      auto [it, is_new_kmers] = kmers->insert({kmer_and_ext.kmer, {}});
      // no space - had to drop it
      if (it == kmers->end()) continue;
      auto &exts_counts = (*it).second;
      update_exts_counts(exts_counts, kmer_and_ext);
    }
  }
}

template <int MAX_K>
static void insert_supermer_from_ctg(Supermer &supermer, dist_object<KmersExtsCountsMap<MAX_K>> &kmers) {
  auto set_exts_counts = [](KmerExtsCounts &exts_counts, KmerAndExt<MAX_K> &kmer_and_ext) {
    exts_counts.count = kmer_and_ext.count;
    exts_counts.from_ctg = true;
    exts_counts.left_exts.set(kmer_and_ext.left, kmer_and_ext.count);
    exts_counts.right_exts.set(kmer_and_ext.right, kmer_and_ext.count);
  };
  size_t dropped_ctg_kmers = 0;
  auto kmer_len = Kmer<MAX_K>::get_k();
  vector<KmerAndExt<MAX_K>> kmers_and_exts;
  kmers_and_exts.reserve(supermer.seq.length() - kmer_len);
  get_kmers_and_exts(supermer, kmers_and_exts);
  for (auto &kmer_and_ext : kmers_and_exts) {
    if (!kmer_and_ext.kmer.is_valid()) {
      WARN("Invalid kmer in insert_supermer_from_ctg\n");
      continue;
    }
    // insert a new kmer derived from the previous round's contigs
    auto [it, is_new] = kmers->insert({kmer_and_ext.kmer, {}});
    if (it == kmers->end()) {  // no space - had to drop it
      dropped_ctg_kmers++;
      continue;
    }
    auto &exts_counts = (*it).second;
    if (is_new) {
      set_exts_counts(exts_counts, kmer_and_ext);
    } else if (!exts_counts.from_ctg) {
      // existing entry is from a read
      if (exts_counts.count == 1) {
        // singleton read kmer, replace - this will just be purged anyway
        set_exts_counts(exts_counts, kmer_and_ext);
      } else {
        char left_ext = exts_counts.get_left_ext();
        char right_ext = exts_counts.get_right_ext();
        // non-UU, replace
        if (left_ext == 'X' || left_ext == 'F' || right_ext == 'X' || right_ext == 'F') set_exts_counts(exts_counts, kmer_and_ext);
      }
    } else {
      // existing entry from contig, if count is zero it was flagged as conflict, so don't update
      if (exts_counts.count) {
        // will always insert, although it may get purged later for a conflict
        char left_ext = exts_counts.get_left_ext();
        char right_ext = exts_counts.get_right_ext();
        if (left_ext != kmer_and_ext.left || right_ext != kmer_and_ext.right) {
          // if the two contig kmers disagree on extensions, set up to purge by setting the count to 0
          exts_counts.count = 0;
        } else {
          // multiple occurrences of the same kmer derived from different contigs or parts of contigs
          // The only way this kmer could have been already found in the contigs only is if it came from a localassm
          // extension. In which case, all such kmers should not be counted again for each contig, because each
          // contig can use the same reads independently, and the depth will be oversampled.
          // kmer_and_ext.count = min(kmer_and_ext.count, exts_counts.count);
          exts_counts.count = min(kmer_and_ext.count, exts_counts.count);
        }
      }
    }
  }
  if (dropped_ctg_kmers) NET_LOG("Dropped ", dropped_ctg_kmers, " from supermer because it was full!\n");
} // insert_supermer_from_ctg

template <int MAX_K>
struct HashTableInserter<MAX_K>::HashTableInserterState {
  PASS_TYPE pass;
  dist_object<KmersExtsCountsMap<MAX_K>> kmers;
  // We keep track of singletons in separate map to save on space. The keys are kmer hashes, which could lead to errors from
  // hard collisions, but those will be very rare and will only affect the counts by one up or down.
  dist_object<SingletonsHashMap> singletons;
  upcxx_utils::BaseTimer insert_timer, kernel_timer;
  bool filter_singletons;
  double fraction_singletons;

  // the kmer lengths are always odd so the last two bits of the binary value will always be 0, so we set them both to 1 to indicate
  // an empty slot
  const std::array<uint64_t, (MAX_K + 31) / 32> key_empty_longs = {0xffffffffffffffff};
  // same as empty key, except the very last bit is 0
  const std::array<uint64_t, (MAX_K + 31) / 32> key_erased_longs = {0xffffffffffffffff - 1};
  Kmer<MAX_K> key_empty;
  Kmer<MAX_K> key_erased;
  // There is a small possibility that a kmer hashes to either of these values, which will lead to an over or undercount by one
  SINGLETONS_KEY key_empty_singleton = 0;
  SINGLETONS_KEY key_erased_singleton = 1;

  HashTableInserterState()
      : key_empty(key_empty_longs.data())
      , key_erased(key_erased_longs.data())
      , kmers(KmersExtsCountsMap<MAX_K>{"kmers hash table", key_empty, key_erased})
      , singletons(SingletonsHashMap{"singletons hash table", key_empty_singleton, key_erased_singleton})
      , filter_singletons(true)
      , fraction_singletons(0.5) {}
};

template <int MAX_K>
HashTableInserter<MAX_K>::HashTableInserter() {}

template <int MAX_K>
HashTableInserter<MAX_K>::~HashTableInserter() {
  if (state) delete state;
}

template <int MAX_K>
void HashTableInserter<MAX_K>::init(size_t num_read_kmers, size_t num_ctg_kmers, size_t num_compact_kmers,
                                    size_t num_singleton_kmers, bool use_kmer_filter, double kcount_fraction_mem) {
  state = new HashTableInserterState();
  state->pass = READ_KMERS_PASS;
  state->filter_singletons = use_kmer_filter;
  double avail_mem = get_free_mem(true) / local_team().rank_n();
  SLOG_CPU_HT("There is ", get_size_str(avail_mem), " available memory per rank for hash table allocations\n");

  size_t elem_size = sizeof(Kmer<MAX_K>) + sizeof(KmerExtsCounts);
  // expected size of compact hash table
  size_t compact_elem_size = sizeof(Kmer<MAX_K>) + sizeof(KmerCounts);
  SLOG_CPU_HT("Element size for main HT ", elem_size, " and for compact HT ", compact_elem_size, "\n");

  double target_load_factor = 0.66;
  double load_multiplier = 1.0 / target_load_factor;

  if (!use_kmer_filter) num_read_kmers += num_singleton_kmers;
  // assume that some ctg kmers will be duplicates
  num_read_kmers = (num_read_kmers + num_ctg_kmers * 0.8);
  num_read_kmers *= load_multiplier;
  num_compact_kmers *= load_multiplier;

  size_t read_kmers_size = num_read_kmers * elem_size;
  size_t compact_kmers_size = num_compact_kmers * compact_elem_size;
  // the singletons take up less space and we want to reduce the load factor to improve performance
  size_t singletons_size = 0;
  ostringstream singletons_count_oss, singletons_size_oss;
  if (state->filter_singletons) {
    // store enough for just the singletons, since we will erase those that are not singletons. Also add some fudge factor
    num_singleton_kmers *= load_multiplier;
    singletons_count_oss << ", singletons ht " << num_singleton_kmers;
    // singletons_size = max_singleton_kmers * (sizeof(uint64_t) + sizeof(uint8_t));
    singletons_size = num_singleton_kmers * (sizeof(SINGLETONS_KEY) + sizeof(uint8_t));
    singletons_size_oss << ", singletons ht " << get_size_str(singletons_size);
  } else {
    num_singleton_kmers = 0;
  }

  SLOG_CPU_HT("Max element counts per rank for a target load factor of ", fixed, setprecision(3), target_load_factor,
              ": read kmers ", num_read_kmers, ", compact ht ", num_compact_kmers, singletons_count_oss.str(), "\n");
  size_t tot_size = read_kmers_size + compact_kmers_size + singletons_size;
  SLOG_CPU_HT("Hash table sizes per rank: read kmers ", get_size_str(read_kmers_size), ", compact ht ",
              get_size_str(compact_kmers_size), singletons_size_oss.str(), ", total ", get_size_str(tot_size), "\n");

  // keep some in reserve for all the various other requirements, e.g. rpcs, local allocs, etc
  double mem_ratio = (double)(kcount_fraction_mem * avail_mem) / tot_size;
  if (mem_ratio > 3.0) mem_ratio = 3.0;
  if (mem_ratio != 1.0) SLOG_CPU_HT("Adjusted element counts for available memory by ", fixed, setprecision(3), mem_ratio, "x\n");
  if (mem_ratio < 0.1) SDIE("Insufficent memory estimated for a load factor under 0.9 across all data structures, aborting\n");
  if (mem_ratio < 0.75) {
    // that ratio gives a load factor of around 0.9 - we could well start dropping after that
    SWARN("Insufficent memory estimated for a load factor under 0.9 across all data structures - this may result in an OOM or "
          "dropped kmers\n");
  }
  num_read_kmers *= mem_ratio;
  num_compact_kmers *= mem_ratio;
  num_singleton_kmers *= mem_ratio;
  state->kmers->reserve(num_read_kmers);
  if (state->filter_singletons) {
    SLOG_CPU_HT("Using a ", sizeof(SINGLETONS_KEY) * 8, " bit key for singleton filtering\n");
    state->singletons->reserve(num_singleton_kmers);
  }
  double avail_mem_after = get_free_mem(true) / local_team().rank_n();
  double used_mem = avail_mem - avail_mem_after;
  SLOG_CPU_HT("Memory available per rank after hash table allocations: ", get_size_str(avail_mem_after), ", used ",
              get_size_str(used_mem), "\n");
}

template <int MAX_K>
void HashTableInserter<MAX_K>::init_preload_ctg_kmers() {
  state->pass = PRELOAD_CTGS_PASS;
}

template <int MAX_K>
void HashTableInserter<MAX_K>::init_ctg_kmers(size_t max_elems) {
  state->pass = CTG_KMERS_PASS;
  // No longer need the singletons
  if (state->filter_singletons) state->singletons->clear();
}

template <int MAX_K>
void HashTableInserter<MAX_K>::init_read_kmers() {
  state->pass = READ_KMERS_PASS;
}

template <int MAX_K>
void HashTableInserter<MAX_K>::insert_supermer(const std::string &supermer_seq, kmer_count_t supermer_count) {
  Supermer supermer = {.seq = supermer_seq, .count = supermer_count};
  state->kernel_timer.start();
  for (int i = 0; i < supermer.seq.length(); i++) {
    char base = supermer.seq[i];
    if (base >= 'a' && base <= 'z') base += ('A' - 'a');
    if (base != 'A' && base != 'C' && base != 'G' && base != 'T' && base != 'N')
      DIE("bad char '", supermer.seq[i], "' in supermer seq int val ", (int)supermer.seq[i], " length ", supermer.seq.length(),
          " supermer ", supermer.seq);
  }
  // preload ctgs are treated like reads but without the singleton filter
  if (state->pass == READ_KMERS_PASS || state->pass == PRELOAD_CTGS_PASS)
    insert_supermer_from_read(supermer, state->kmers, state->singletons, state->pass == READ_KMERS_PASS && state->filter_singletons);
  else
    insert_supermer_from_ctg(supermer, state->kmers);
  state->kernel_timer.stop();
}

template <int MAX_K>
void HashTableInserter<MAX_K>::flush_inserts() {
  SLOG_CPU_HT("After inserting ",
              state->pass == CTG_KMERS_PASS  ? "ctg kmers" :
              state->pass == READ_KMERS_PASS ? "read kmers" :
                                               "preload contigs",
              "\n");
  bool warn_dropped = true;
  state->kmers->print_statistics(warn_dropped);
  if (state->pass == READ_KMERS_PASS && state->filter_singletons) {
    warn_dropped = false;
    state->singletons->print_statistics(warn_dropped);
  }
}

template <int MAX_K>
double HashTableInserter<MAX_K>::get_fraction_singletons() {
  return state->fraction_singletons;
}

template <int MAX_K>
double HashTableInserter<MAX_K>::insert_into_local_hashtable(dist_object<KmerMap<MAX_K>> &local_kmers) {
  BarrierTimer timer(__FILEFUNC__);
  int64_t num_good_kmers = state->kmers->size() + 2000;
  int64_t max_kmer_counts = 0;
  state->insert_timer.start();

  for (auto [kmer, kmer_ext_counts] : *(state->kmers)) {
    if (kmer_ext_counts.count > max_kmer_counts) max_kmer_counts = kmer_ext_counts.count;
    if ((kmer_ext_counts.count < 2) || (kmer_ext_counts.left_exts.is_zero() && kmer_ext_counts.right_exts.is_zero()))
      num_good_kmers--;
  }
  auto fut_msm_max_kmer_count = upcxx_utils::min_sum_max_reduce_all(max_kmer_counts);
  LOG_MEM("Before inserting into local hashtable");
  SLOG_CPU_HT("Reserving compact hash table for ", num_good_kmers, " elements per rank, requires ",
              get_size_str(num_good_kmers * (sizeof(Kmer<MAX_K>) + sizeof(KmerCounts))), "\n");
  auto free_mem = get_free_mem(true);
  // load factor of 0.666
  local_kmers->reserve(num_good_kmers * 1.5);
  auto free_mem_after = get_free_mem(true);
  double used_mem = free_mem - free_mem_after;
  SLOG_CPU_HT("Memory available per rank after compact hash table allocation: ", get_size_str(free_mem_after / rank_n()), ", used ",
              get_size_str(used_mem / rank_n()), "\n");
  LOG_MEM("After reserving for local hashtable");
  int64_t num_singletons_purged = 0, num_bad_purged = 0, num_inserted = 0;
  auto msm_max_kmer_count = fut_msm_max_kmer_count.wait();
  if (!rank_me()) LOG("High count (max) for kmers: ", msm_max_kmer_count.to_string(), "\n");
  int64_t high_count_threshold = msm_max_kmer_count.avg;
  uint64_t sum_kmer_counts = 0;
  for (auto [kmer, kmer_ext_counts] : *state->kmers) {
    if (kmer_ext_counts.count < 2) {
      num_singletons_purged++;
      continue;
    }
    if (!kmer.is_valid()) {
      WARN("Invalid kmer in insert_into_local_hashtable\n");
      continue;
    }
    if (kmer_ext_counts.count >= high_count_threshold) {
      NET_LOG("High count kmer: k = ", Kmer<MAX_K>::get_k(), " count = ", kmer_ext_counts.count, " kmer = ", kmer.to_string(),
              "\n");
    }
    KmerCounts kmer_counts = {.uutig_frag = nullptr,
                              .count = kmer_ext_counts.count,
                              .left = kmer_ext_counts.get_left_ext(),
                              .right = kmer_ext_counts.get_right_ext()};
    if (kmer_counts.left == 'X' || kmer_counts.right == 'X' || kmer_counts.left == 'F' || kmer_counts.right == 'F') {
      // these are never used in the dbjg traversal, and are overwritten by ctg kmers if those have proper extensions
      num_bad_purged++;
      continue;
    }
    auto it = local_kmers->find(kmer);
    if (it != local_kmers->end())
      DIE("Found a duplicate kmer ", kmer.to_string(), " - shouldn't happen: existing count ", (*it).second.count, " new count ",
          kmer_counts.count);
    local_kmers->insert({kmer, kmer_counts});
    num_inserted++;
    sum_kmer_counts += kmer_counts.count;
  }
  state->insert_timer.stop();
  if (num_inserted > num_good_kmers) WARN("Inserted ", num_inserted, " but was expecting only ", num_good_kmers, "\n");
  barrier();
  LOG_MEM("After inserting into local hashtable");
  auto tot_num_singletons_purged = reduce_all(num_singletons_purged, op_fast_add).wait();
  auto tot_num_bad_purged = reduce_one(num_bad_purged, op_fast_add, 0).wait();
  auto tot_num_kmers = reduce_all(state->kmers->size(), op_fast_add).wait();
  SLOG_CPU_HT("CPU Hashtable: purged ", perc_str(tot_num_singletons_purged, tot_num_kmers), " singleton kmers and ",
              perc_str(tot_num_bad_purged, tot_num_kmers), " bad kmers out of ", tot_num_kmers, " kmers\n");

  if (state->filter_singletons) {
    size_t num_singletons = state->singletons->size() + num_singletons_purged;
    auto all_num_singletons = reduce_all(num_singletons, op_fast_add).wait();
    state->fraction_singletons = (double)all_num_singletons / (all_num_singletons + tot_num_kmers - tot_num_singletons_purged);
    SLOG_CPU_HT("Fraction of singletons: ", fixed, setprecision(3), state->fraction_singletons, "\n");
    double expected_num_collisions = (double)num_singletons * num_singletons / 2.0 / std::numeric_limits<SINGLETONS_KEY>::max();
    SLOG_CPU_HT("Singleton hash table expected collisions: ", perc_str(expected_num_collisions, num_singletons), "\n");
  } else {
    state->fraction_singletons = (double)tot_num_singletons_purged / tot_num_kmers;
  }
  auto final_load_factor = (double)reduce_one(local_kmers->load_factor(), op_fast_add, 0).wait() / rank_n();
  auto max_final_load_factor = (double)reduce_one(local_kmers->load_factor(), op_fast_max, 0).wait();
  auto all_kmers_inserted = reduce_all(num_inserted, op_fast_add).wait();
  SLOG_CPU_HT("Compact hash table has ", num_inserted, " kmers and load factor ", fixed, setprecision(3), final_load_factor,
              " avg ", max_final_load_factor, " max\n");
  auto all_sum_kmer_counts = reduce_all(sum_kmer_counts, op_fast_add).wait();
  double avg_kmer_count = (double)all_sum_kmer_counts / all_kmers_inserted;
  SLOG_CPU_HT("For ", all_kmers_inserted, " kmers, average kmer count (depth): ", fixed, setprecision(2), avg_kmer_count, "\n");
  return avg_kmer_count;
}

template <int MAX_K>
void HashTableInserter<MAX_K>::get_elapsed_time(double &insert_time, double &kernel_time) {
  insert_time = state->insert_timer.get_elapsed();
  kernel_time = state->kernel_timer.get_elapsed();
}

#define SEQ_BLOCK_INSERTER_K(KMER_LEN) template struct SeqBlockInserter<KMER_LEN>;
#define HASH_TABLE_INSERTER_K(KMER_LEN) template class HashTableInserter<KMER_LEN>;

SEQ_BLOCK_INSERTER_K(32);
HASH_TABLE_INSERTER_K(32);
#if MAX_BUILD_KMER >= 64
SEQ_BLOCK_INSERTER_K(64);
HASH_TABLE_INSERTER_K(64);
#endif
#if MAX_BUILD_KMER >= 96
SEQ_BLOCK_INSERTER_K(96);
HASH_TABLE_INSERTER_K(96);
#endif
#if MAX_BUILD_KMER >= 128
SEQ_BLOCK_INSERTER_K(128);
HASH_TABLE_INSERTER_K(128);
#endif
#if MAX_BUILD_KMER >= 160
SEQ_BLOCK_INSERTER_K(160);
HASH_TABLE_INSERTER_K(160);
#endif
#undef SEQ_BLOCK_INSERTER_K
#undef HASH_TABLE_INSERTER_K
