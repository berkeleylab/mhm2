#pragma once

/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include <vector>
#include <iterator>
#include <cstddef>

#include "upcxx_utils.hpp"
#include "upcxx_utils/log.h"
#include "utils.hpp"
#include "kmer.hpp"
#include "prime.hpp"

using std::pair;
using std::string;
using std::vector;

// #define SLOG_KEY_HASH_MAP(...) SLOG(KLMAGENTA, __VA_ARGS__, KNORM)
#define SLOG_KEY_HASH_MAP SLOG_VERBOSE

template <typename K, typename V>
class FixedHashMap {
  size_t capacity = 0;
  size_t num_elems = 0;
  size_t num_dropped = 0;
  size_t num_replaced = 0;
  size_t num_erased = 0;
  vector<K> keys;
  size_t num_probes = 0;
  size_t max_probe_len = 0;
  size_t sum_probe_lens = 0;
  vector<V> values;
  size_t iter_pos = 0;
  string description;
  // Need to ensure that the hash value inserted is never 0 or 1
  // static const uint64_t KEY_EMPTY = 0;
  // static const uint64_t KEY_ERASED = 1;
  K key_empty;
  K key_erased;

  uint64_t get_hash(K key) {
    std::hash<K> hash_key;
    return hash_key(key);
  }

  size_t find_slot(K key) {
    // std::hash<K> hash_key;
    // size_t slot = hash_key(key) % capacity;
    size_t slot = get_hash(key) % capacity;
    for (size_t probe_len = 0; probe_len <= max_probe_len; probe_len++) {
      num_probes++;
      if (keys[slot] == key_empty)
        return capacity;
      else if (key == keys[slot])
        return slot;
      slot = (slot + 1) % capacity;
    }
    return capacity;
  }

  void insert_new(int insert_slot, int start_slot, int probe_len, K key, V value) {
    keys[insert_slot] = key;
    values[insert_slot] = value;
    num_elems++;
    max_probe_len = std::max((int)max_probe_len, probe_len);
    sum_probe_lens += probe_len;
  }

 public:
  struct iterator;

  explicit FixedHashMap(const string &description, K key_empty, K key_erased)
      : description(description)
      , key_empty(key_empty)
      , key_erased(key_erased) {}

  void reserve(size_t max_elems) {
    primes::Prime prime;
    prime.set(max_elems, true);
    capacity = prime.get();
    num_elems = 0;
    keys.resize(capacity);
    std::fill(keys.begin(), keys.end(), key_empty);
    values.resize(capacity, {0});
    SLOG_KEY_HASH_MAP(description, ": capacity is set to ", capacity, " for ", max_elems, " max elements, size ",
                      upcxx_utils::get_size_str(capacity * (sizeof(K) + sizeof(V))), "\n");
  }

  void clear() {
    capacity = 0;
    num_elems = 0;
    num_dropped = 0;
    num_replaced = 0;
    num_erased = 0;
    num_probes = 0;
    max_probe_len = 0;
    sum_probe_lens = 0;
    iter_pos = 0;
    keys.clear();
    vector<K>().swap(keys);
    values.clear();
    vector<V>().swap(values);
  }

  // returns a pointer to the element found; inserts it if not found. The bool second part indicates if the key was inserted
  // returns {end(), false} when there is no space
  pair<iterator, bool> insert(const pair<K, V> &elem) {
    assert(capacity > 0);
    auto [key, value] = elem;
    // size_t slot = hash_key(key) % capacity;
    size_t slot = get_hash(key) % capacity;
    int start_slot = slot;
    const size_t MAX_PROBE = (capacity < KCOUNT_HT_MAX_PROBE ? capacity : KCOUNT_HT_MAX_PROBE);
    int insert_slot = capacity;
    for (size_t probe_len = 0; probe_len < MAX_PROBE; probe_len++) {
      num_probes++;
      if (keys[slot] == key_empty) {
        if (insert_slot == capacity) {
          insert_slot = slot;
        } else {
          num_replaced++;
          if (keys[insert_slot] != key_erased) DIE("key should be erased");
          assert(keys[insert_slot] == key_erased);
        }
        insert_new(insert_slot, start_slot, probe_len, key, value);
        return {iterator(insert_slot, capacity, keys, values, key_empty, key_erased), true};
      } else if (key == keys[slot]) {
        // return existing element
        return {iterator(slot, capacity, keys, values, key_empty, key_erased), false};
      } else if (keys[slot] == key_erased) {
        // we can't just use this slot because we may find the kmer later on, so we just keep track of this in case we don't find
        // the kmer later
        if (insert_slot == capacity) insert_slot = slot;
      }
      slot = (slot + 1) % capacity;
    }
    if (insert_slot != capacity) {
      // we have an available erased slot
      if (keys[insert_slot] != key_erased) DIE("key should be erased");
      assert(keys[insert_slot] == key_erased);
      num_replaced++;
      int probe_len = insert_slot - start_slot;
      if (probe_len < 0) probe_len += capacity;
      insert_new(insert_slot, start_slot, probe_len, key, value);
      return {iterator(insert_slot, capacity, keys, values, key_empty, key_erased), true};
    }
    num_dropped++;
    return {end(), false};
  }

  size_t erase(K key) {
    assert(capacity > 0);
    size_t slot = find_slot(key);
    if (slot == capacity) return 0;
    keys[slot] = key_erased;
    num_erased++;
    num_elems--;
    return 1;
  }

  iterator find(K key) {
    assert(capacity > 0);
    size_t slot = find_slot(key);
    if (slot == capacity) return end();
    return iterator(slot, capacity, keys, values, key_empty, key_erased);
  }

  size_t size() { return num_elems; }

  size_t get_num_erased() { return num_erased; }

  size_t get_num_dropped() { return num_dropped; }

  float load_factor() { return (double)num_elems / capacity; }

  void print_statistics(bool warn_dropped) {
    SLOG_KEY_HASH_MAP(description, "\n");
    int64_t tot_num_elems = reduce_one(num_elems, op_fast_add, 0).wait();
    auto max_num_elems = reduce_one(num_elems, op_fast_max, 0).wait();
    SLOG_KEY_HASH_MAP("  avg number of elements per rank: ", tot_num_elems / rank_n(), "\n");
    auto my_load_factor = load_factor();
    auto avg_load_factor = reduce_one(my_load_factor, op_fast_add, 0).wait() / upcxx::rank_n();
    auto max_load_factor = reduce_one(my_load_factor, op_fast_max, 0).wait();
    SLOG_KEY_HASH_MAP("  load factor: ", avg_load_factor, " avg, ", max_load_factor, " max, load balance ", std::fixed,
                      std::setprecision(3), (double)avg_load_factor / max_load_factor, "\n");
    int64_t tot_num_probes = reduce_one(num_probes, op_fast_add, 0).wait();
    auto avg_probe_lens = (double)reduce_one(sum_probe_lens, op_fast_add, 0).wait() / tot_num_elems;
    auto all_max_probe_len = (double)reduce_one(max_probe_len, op_fast_max, 0).wait();
    SLOG_KEY_HASH_MAP("  num probes: ", tot_num_probes, "\n");
    SLOG_KEY_HASH_MAP("  probe length: ", avg_probe_lens, " avg, ", all_max_probe_len, " max\n");
    auto tot_num_dropped = reduce_one(num_dropped, op_fast_add, 0).wait();
    auto tot_num_replaced = reduce_one(num_replaced, op_fast_add, 0).wait();
    auto tot_num_erased = reduce_one(num_erased, op_fast_add, 0).wait();
    num_dropped = 0;
    num_replaced = 0;
    num_erased = 0;
    auto tot_elems = tot_num_elems + tot_num_dropped;
    if (tot_num_dropped) SLOG_KEY_HASH_MAP("  Number dropped ", upcxx_utils::perc_str(tot_num_dropped, tot_elems), "\n");
    if (tot_num_replaced) SLOG_KEY_HASH_MAP("  Number replaced ", upcxx_utils::perc_str(tot_num_replaced, tot_elems), "\n");
    if (tot_num_erased) SLOG_KEY_HASH_MAP("  Number erased ", upcxx_utils::perc_str(tot_num_erased, tot_elems), "\n");
    SLOG_KEY_HASH_MAP("CPU kcount found total of ", tot_elems, " unique kmers (including singletons and dropped kmers)\n");
    if (warn_dropped && 100.0 * tot_num_dropped / tot_elems > 0.1)
      SWARN("Lack of memory caused ", upcxx_utils::perc_str(tot_num_dropped, tot_elems), " kmers to be dropped from the ",
            description);
    barrier();
    auto avg_kmers_processed = tot_num_elems / rank_n();
    auto max_kmers_processed = reduce_one(num_elems, op_fast_max, 0).wait();
    SLOG_KEY_HASH_MAP("  Avg kmers processed per rank ", avg_kmers_processed, " (balance ",
                      (double)avg_kmers_processed / max_kmers_processed, ")\n");
  }

  struct iterator {
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;

   private:
    size_t pos;
    size_t capacity;
    vector<K> &keys;
    vector<V> &values;
    K key_empty;
    K key_erased;

    void move_to_next() {
      for (; pos < capacity; pos++) {
        if (keys[pos] != key_empty && keys[pos] != key_erased) break;
      }
    }

   public:
    iterator(size_t pos, size_t capacity, vector<K> &keys, vector<V> &values, K key_empty, K key_erased)
        : pos(pos)
        , capacity(capacity)
        , keys(keys)
        , values(values)
        , key_empty(key_empty)
        , key_erased(key_erased) {
      move_to_next();
    }

    pair<K &, V &> operator*() {
      assert(pos < capacity);
      return std::make_pair(std::ref(keys[pos]), std::ref(values[pos]));
    }

    // pre-increment
    iterator &operator++() {
      pos++;
      move_to_next();
      return *this;
    }

    // post-increment
    iterator operator++(int) {
      iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    const bool operator==(iterator other) { return pos == other.pos; };
    const bool operator!=(iterator other) { return pos != other.pos; };
  };

  iterator begin() { return iterator(0, capacity, keys, values, key_empty, key_erased); }
  iterator end() { return iterator(capacity, capacity, keys, values, key_empty, key_erased); }
};
