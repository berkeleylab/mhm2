/*
 HipMer v 2.0, Copyright (c) 2020, The Regents of the University of California,
 through Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).  All rights reserved."

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

 (3) Neither the name of the University of California, Lawrence Berkeley National
 Laboratory, U.S. Dept. of Energy nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

 You are under no obligation whatsoever to provide any bug fixes, patches, or upgrades
 to the features, functionality or performance of the source code ("Enhancements") to
 anyone; however, if you choose to make your Enhancements available either publicly,
 or directly to Lawrence Berkeley National Laboratory, without imposing a separate
 written license agreement for such Enhancements, then you hereby grant the following
 license: a  non-exclusive, royalty-free perpetual license to install, use, modify,
 prepare derivative works, incorporate into other computer software, distribute, and
 sublicense such enhancements or derivative works thereof, in binary and source code
 form.
*/

#include <bitset>
#include <filesystem>

#include "post_assembly.hpp"
#include "aln_depths.hpp"
#include "fastq.hpp"
#include "gasnet_stats.hpp"
#include "klign.hpp"
#include "packed_reads.hpp"
#include "stage_timers.hpp"
#include "shuffle_reads.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/mem_profile.hpp"
#include "upcxx_utils/three_tier_aggr_store.hpp"

using namespace upcxx_utils;
using namespace std;

struct CtgBaseRange {
  cid_t cid;
  int clen;
  int cstart;
  int cstop;
};

#define BITSET_SIZE 256

static size_t get_target_rank(cid_t cid) { return std::hash<cid_t>{}(cid) % upcxx::rank_n(); }

struct CtgBasesCovered {
  int clen;
  vector<bitset<BITSET_SIZE>> bases_covered;

  int num_bits_set() {
    int num_bits = 0;
    for (auto &b : bases_covered) num_bits += b.count();
    return num_bits;
  }
};

class CtgsCovered {
  using local_ctgs_covered_map_t = HASH_TABLE<cid_t, CtgBasesCovered>;
  using ctgs_covered_map_t = upcxx::dist_object<local_ctgs_covered_map_t>;
  ctgs_covered_map_t ctgs_covered;
  ThreeTierAggrStore<CtgBaseRange> ctg_bases_covered_store;

  void update_ctg_bases(const CtgBaseRange &ctg_base_range) {
    cid_t cid = ctg_base_range.cid;
    int clen = ctg_base_range.clen;
    auto elem = ctgs_covered->find(cid);
    if (elem == ctgs_covered->end()) {
      CtgBasesCovered new_elem = {.clen = clen, .bases_covered = {}};
      new_elem.bases_covered.resize(clen / BITSET_SIZE + (clen % BITSET_SIZE != 0), 0);
      elem = ctgs_covered->insert({cid, new_elem}).first;
    }
    if (elem->second.clen != ctg_base_range.clen) DIE("clens don't match ", elem->second.clen, " != ", ctg_base_range.clen);
    // update the ranges of covered bases
    for (int i = ctg_base_range.cstart; i < ctg_base_range.cstop; i++) {
      elem->second.bases_covered[i / BITSET_SIZE][i % BITSET_SIZE] = 1;
    }
  }

 public:
  CtgsCovered(size_t num_ctgs)
      : ctgs_covered(local_ctgs_covered_map_t{})
      , ctg_bases_covered_store() {
    ctgs_covered->reserve(num_ctgs * 2 + 2000);  // entries for self + distributed calcs
    int64_t mem_to_use = 0.1 * get_free_mem(true) / local_team().rank_n();
    size_t max_store_bytes = std::max(mem_to_use, (int64_t)sizeof(CtgBasesCovered) * 100);
    ctg_bases_covered_store.set_size("Ctg Bases Covered", max_store_bytes);
    ctg_bases_covered_store.set_update_func(
        [&self = *this](CtgBaseRange ctg_base_range) { self.update_ctg_bases(ctg_base_range); });
  }

  void add_ctg_range(cid_t cid, int clen, int cstart, int cstop) {
    // cid is -1 if the aln was dropped in favor of better alignments
    if (cid == -1) return;
    CtgBaseRange ctg_base_range = {.cid = cid, .clen = clen, .cstart = cstart, .cstop = cstop};
    auto tgt = get_target_rank(cid);
    if (tgt != rank_me()) {
      ctg_bases_covered_store.update(tgt, ctg_base_range);
    } else {
      update_ctg_bases(ctg_base_range);
    }
  }

  void done_adding() { ctg_bases_covered_store.flush_updates(); }

  size_t get_covered_bases() {
    size_t num_covered_bases = 0;
    for (auto &[key, val] : *ctgs_covered) num_covered_bases += val.num_bits_set();
    return num_covered_bases;
  }
};

void post_assembly(Contigs &ctgs, Options &options) {
  SLOG(KBLUE, "_________________________", KNORM, "\n");
  SLOG(KBLUE, "Post processing", KNORM, "\n\n");
  LOG_MEM("Starting Post Assembly");
  auto start_t = clock_now();
  // set up output files
  string sam_header_fname = "final_assembly.header.sam.gz";
  if (options.post_assm_write_sam && !upcxx_utils::file_exists(sam_header_fname)) {
    SLOG_VERBOSE("Writing SAM headers\n");
    dist_ofstream sam_header_ofs(sam_header_fname);
    stage_timers.dump_alns->start();
    Alns::write_sam_header(sam_header_ofs, options.reads_fnames, ctgs, options.min_ctg_print_len).wait();
    sam_header_ofs.close();
    stage_timers.dump_alns->stop();
  }
  auto num_read_groups = options.reads_fnames.size();
  SLOG_VERBOSE("Preparing aln depths for post assembly abundance\n");
  AlnDepths aln_depths(ctgs, options.min_ctg_print_len, num_read_groups);
  LOG_MEM("After Post Assembly Ctgs Depths");
  CtgsCovered ctgs_covered(ctgs.size());
  auto max_kmer_store = options.max_kmer_store_mb * ONE_MB;
  const int MAX_K = (POST_ASM_ALN_K + 31) / 32 * 32;
  const bool REPORT_CIGAR = true;
  const bool USE_BLASTN_SCORES = true;
  const bool ALLOW_MULTI_KMERS = true;
  size_t tot_num_reads = 0;
  size_t tot_num_bases = 0;
  size_t tot_reads_aligned = 0;
  size_t tot_bases_aligned = 0;
  size_t tot_proper_pairs = 0;
  auto num_ctg_kmers = ctgs.get_num_ctg_kmers(POST_ASM_ALN_K) / options.post_assm_subsets + 1;
  SLOG(KBLUE, "Processing contigs in ", options.post_assm_subsets, " subsets", KNORM, "\n");

  // single dht with kmer_ctg data swapped over each subset
  std::shared_ptr<KmerCtgDHT<MAX_K>> sh_kmer_ctg_dht;
  sh_kmer_ctg_dht =
      construct_kmer_ctg_dht<MAX_K>(POST_ASM_ALN_K, max_kmer_store, options.max_rpcs_in_flight, num_ctg_kmers, ALLOW_MULTI_KMERS);
  bool kmer_ctg_dht_initialized = false;
  vector<size_t> sliced_all_num_contigs;  // the total count for each subset
  vector<KmersReadsBuffer<MAX_K>> kmers_reads_buffers;
  allocate_kmers_reads_buffers(kmers_reads_buffers);
  {
    BarrierTimer bt("Barrier after allocation of kmers reads buffers");
    LOG_MEM("After allocation of kmers reads buffers");
  }

  Adapters adapters(POST_ASM_ALN_K, options.adapter_fname, false);

  // iterate over all files in batches and look for checkpoints
  vector<uint8_t> needs_generation(options.reads_fnames.size(), 1);
  for (int batch_i = 0; batch_i < options.reads_fnames.size(); batch_i++) {  // batch_i
    int read_group_id = batch_i;
    string &reads_fname = options.reads_fnames[read_group_id];
    auto short_name = get_basename(reads_fname);
    string sam_fname = "final_assembly." + short_name + ".sam.gz";
    string ckpt_depths = aln_depths.get_checkpoint_fname(read_group_id);
    if (options.restart && rank_me() == 0) {
      LOG("Looking for checkpoint for read_group=", read_group_id, " ", short_name, "\n");
      if ((!options.post_assm_write_sam || upcxx_utils::file_exists(sam_fname)) && upcxx_utils::file_exists(ckpt_depths)) {
        LOG("Found depths checkpoint: ", ckpt_depths,
            (options.post_assm_write_sam ? ". No sam requested." : " and found sam file: " + sam_fname), "\n");
        needs_generation[batch_i] = 0;
      }
    }
  }
  upcxx::broadcast(needs_generation.data(), needs_generation.size(), 0).wait();

  future<> fut_ready_new_kmer_dht = make_future();
  ctgs.clear_slices();
  for (int subset_i = 0; subset_i < options.post_assm_subsets; subset_i++) {
    ctgs.set_next_slice(options.post_assm_subsets);
    auto all_num_ctgs = reduce_all(ctgs.size(), op_fast_add).wait();
    sliced_all_num_contigs.push_back(all_num_ctgs);
    LOG("Subset ", subset_i, " has ", ctgs.size(), " local ctgs, global total ", all_num_ctgs, "\n");
  }
  ctgs.clear_slices();

  int batch_num = 0;
  int read_group_start = 0;
  size_t batch_fastq_size = options.post_assm_batch_mb * ONE_MB * (rank_n() / local_team().rank_n());
  while (read_group_start < options.reads_fnames.size()) {
    SLOG(KBLUE, "_________________________", KNORM, "\n");

    // find the batch of read_group_ids in order that just exceed the targeted post_assm_batch_mb
    int read_group_batch_count = 0;
    size_t this_batch_size = 0;
    do {
      auto read_group_id = read_group_start + read_group_batch_count;
      read_group_batch_count++;
      string reads_fname = options.reads_fnames[read_group_id];
      string reads_fname2;
      bool dummy;
      FastqReader::split_file_name(reads_fname, reads_fname2, dummy);
      this_batch_size += get_file_size(reads_fname, 0);
      if (!reads_fname2.empty()) this_batch_size += get_file_size(reads_fname2, 0);
      if (this_batch_size > batch_fastq_size) break;
    } while (read_group_start + read_group_batch_count < options.reads_fnames.size());

    vector<bool> is_paired(read_group_batch_count, true);
    vector<string> reads_list;
    auto range = make_string("read_goups ", read_group_start, "-", read_group_start + read_group_batch_count - 1);

    SLOG("Aligning batch ", ++batch_num, " with ", read_group_batch_count, " fastqs: ", range, " of ",
         get_size_str(this_batch_size), " first: ", get_basename(options.reads_fnames[read_group_start]), "\n");

    bool all_checkpointed = true;
    // look for checkpoints and files that can be skipped
    for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
      int read_group_id = read_group_start + batch_i;
      string &reads_fname = options.reads_fnames[read_group_id];
      if (!needs_generation[read_group_id] && aln_depths.test_restore_checkpoint(read_group_id)) {
        SLOG("Skipping checkpointed read_group=", read_group_id, " file=", reads_fname, "\n");
        continue;
      }
      all_checkpointed = false;

      SLOG(KBLUE, "Processing file ", reads_fname, KNORM, "\n");
      reads_list.push_back(reads_fname);
    }  // for batch_i

    if (all_checkpointed) {
      SLOG("Skipping entire batch ", batch_num, " as all read groups are checkpointed\n");
      read_group_start += read_group_batch_count;
      continue;
    }

    {
      BarrierTimer bt("Before reading fastq file(s) " + range);
      LOG_MEM("Before read fastq file(s) " + range);
    }

    // open all files partitioned globally across the ranks
    SLOG("Reading all files in batch\n");
    FastqReaders::open_all_file_blocking(reads_list);  // FIXME open with global_blocking then shuffle to rebalance
    PackedReadsList batch_packed_reads;
    unsigned max_rlen_limit = 0;
    auto fut_read_packed_reads = make_future();
    KlignAligner::get_singleton_rget_requests() =
        make_shared<KlignAligner::rget_requests_t>(upcxx_utils::split_rank::node_team().rank_n());
    // read all files
    for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
      int read_group_id = read_group_start + batch_i;
      string &reads_fname = options.reads_fnames[read_group_id];
      if (!needs_generation[read_group_id]) {
        batch_packed_reads.push_back(nullptr);
        LOG("Skipping reading read_group=", read_group_id, " fastq: ", reads_fname, "\n");
        continue;
      }

      batch_packed_reads.push_back(new PackedReads(options.qual_offset, reads_fname, true));
      auto &packed_reads = *batch_packed_reads[batch_i];
      stage_timers.cache_reads->start();
      auto fut = packed_reads.load_reads_nb(adapters);
      fut_read_packed_reads = when_all(fut, fut_read_packed_reads);
      unsigned rlen_limit = packed_reads.get_max_read_len();
      max_rlen_limit = std::max(max_rlen_limit, rlen_limit);
      stage_timers.cache_reads->stop();
      is_paired[batch_i] = packed_reads.is_paired();
      tot_num_reads += packed_reads.get_local_num_reads();
      tot_num_bases += packed_reads.get_local_bases();
    }  // for batch_i
    DBG("Waiting for all pending ops\n");
    Timings::wait_pending();
    DBG("Waiting for all packed reads to be ingested\n");
    fut_read_packed_reads.wait();
    {
      BarrierTimer bt("After reading fastq file(s) " + range);
      LOG_MEM("Read fastq file(s) " + range);
      // FIXME balance_reads(options.qual_offset, batch_packed_reads); // possible fixme to get proper ids
      // LOG_MEM("Afte read balancing of fastq file(s) " + range);
    }
    // prep for aligment over all subsets and batched files
    vector<vector<vector<ReadRecord>>> batch_read_records;  // batch_read_records[batch][subset]
    Alns batch_alns(max_rlen_limit);  // use a single alns for all batch read groups for efficiency.  split them out when sorting
    batch_read_records.resize(read_group_batch_count, {});
    for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
      batch_read_records[batch_i].reserve(options.post_assm_subsets);
      for (int subset_i = 0; subset_i < options.post_assm_subsets; subset_i++) {
        int read_group_id = read_group_start + batch_i;
        auto packed_reads = batch_packed_reads[batch_i];
        batch_read_records[batch_i].emplace_back(needs_generation[read_group_id] ? packed_reads->get_local_num_reads() : 0);
      }
    }
    assert(batch_read_records.size() == read_group_batch_count);
    ctgs.clear_slices();

    {
      BarrierTimer bt("Starting alignments file(s) " + range);
      LOG_MEM("Starting alignments file(s) " + range);
    }

    // allocate 1 KlignAligner per batch
    KlignTimers aln_timers;
    KlignAligner aligner(Kmer<MAX_K>::get_k(), max_rlen_limit, REPORT_CIGAR, USE_BLASTN_SCORES, aln_timers);

    // iterate over subsets, then files by batched read_groups
    for (int subset_i = 0; subset_i < options.post_assm_subsets; subset_i++) {
      LOG("Starting subset ", subset_i, "\n");
      if (options.post_assm_subsets > 1 || !kmer_ctg_dht_initialized) {
        SLOG(KBLUE, "\nContig subset ", subset_i, KNORM, ":\n");
        ctgs.set_next_slice(options.post_assm_subsets);
        auto all_num_ctgs = sliced_all_num_contigs[subset_i];

        LOG("Waiting for all ranks to complete last subset alignments\n");
        Timings::wait_pending();
        fut_ready_new_kmer_dht.wait();

        SLOG("Rebuilding kmer contig maps for subset ", subset_i, "\n");
        stage_timers.build_aln_seed_index->start();
        build_kmer_ctg_dht<MAX_K>(*sh_kmer_ctg_dht, ctgs, options.min_ctg_print_len);
        stage_timers.build_aln_seed_index->stop();
        kmer_ctg_dht_initialized = true;
        barrier();
      }
      auto &kmer_ctg_dht = *sh_kmer_ctg_dht;

      progress();

      stage_timers.alignments->start();
      // align files to this subset of contigs
      assert(read_group_batch_count == batch_packed_reads.size());
      AlignReadPackages arps;

      for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
        int read_group_id = read_group_start + batch_i;
        if (!needs_generation[read_group_id]) {
          LOG("Skipping aligning batch ", batch_num, " ", batch_i, "of", read_group_batch_count, " subset=", subset_i,
              " read_group=", read_group_id, "\n");
          continue;
        }
        LOG("Starting my alignment for batch_i=", batch_i, " of ", read_group_batch_count, " subset_i=", subset_i, "\n");
        assert(batch_packed_reads.size() == read_group_batch_count);
        assert(batch_packed_reads[batch_i]);
        assert(batch_read_records[batch_i].size() == options.post_assm_subsets);
        auto &alns = batch_alns;
        auto packed_reads = batch_packed_reads[batch_i];
        auto &read_records = batch_read_records[batch_i][subset_i];

        arps.push_back(
            {.packed_reads = packed_reads, .read_records = &read_records, .alns = &alns, .read_group_id = read_group_id});
        progress();
      }  // for batch_i

      auto all_num_ctgs = sliced_all_num_contigs[subset_i];
      fetch_ctg_maps(kmer_ctg_dht, arps, POST_ASM_ALN_SEED_SPACE, kmers_reads_buffers, aln_timers);
      compute_alns<MAX_K>(arps, aligner, all_num_ctgs, options.klign_rget_buf_size, aln_timers);
      flush_outstanding_futures();
      stage_timers.kernel_alns->inc_elapsed(aln_timers.aln_kernel.get_elapsed());
      stage_timers.aln_comms->inc_elapsed(aln_timers.fetch_ctg_maps.get_elapsed() + aln_timers.rget_ctg_seqs.get_elapsed());

      LOG("Completed my alignment for subset_i=", subset_i, "\n");

      stage_timers.alignments->stop();

      // keep this data in kmer_ctg_dht around until all ranks are finished with it

      LOG("Starting barrier_async for batch_read_records[*][", subset_i, "] cleanup\n");
      auto fut_barrier = upcxx::barrier_async();

      // free the memory in a separate thread to keep attention
      auto fut_free_mem = execute_after_in_thread_pool(fut_barrier, [=, &batch_read_records]() {
        assert(batch_read_records.size() == read_group_batch_count);
        LOG("Freeing subset_i=", subset_i, " batch_read_records\n");
        for (auto &brr : batch_read_records) {
          brr[subset_i] = {};  // free memory when all ranks have completed this subset
        }
      });
      fut_barrier = when_all(fut_barrier, fut_free_mem);
      Timings::set_pending(fut_barrier);

      // do not allocate a new dht until this one is deallocated
      fut_ready_new_kmer_dht = when_all(fut_ready_new_kmer_dht, fut_barrier);
      progress();
    }  // for subset_i

    {
      LOG("Waiting for all subsets to be completed\n");
      BarrierTimer bt("After aligning fastq file(s)  " + range);
      fut_ready_new_kmer_dht.wait();
      for (auto &pr : batch_packed_reads) delete pr;
      batch_packed_reads = {};
      LOG_MEM("After batch aligning fastq file(s)  " + range);
    }
    ctgs.clear_slices();

    // complete and write files
    LOG("Processing alignments for batch\n");

    aligner.set_alns();
    aligner.finish_alns();
    {
      upcxx::progress();
      upcxx::discharge();
      BarrierTimer bt("after aligning and before batched sorting");
    }

    std::map<int, Alns> rg_alns;
    batch_alns.split_by_read_group(rg_alns);

    // sort alignments and select pairs and count bases
    for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
      int read_group_id = read_group_start + batch_i;
      if (!needs_generation[read_group_id]) {
        LOG("Skipping sorting alns read_group=", read_group_id, "\n");
        progress();
        continue;
      }
      auto &alns = rg_alns[read_group_id];

      // the alignments have to be accumulated per read so they can be sorted to keep alignments to each read together
      LOG("Sorting ", alns.size(), " alignments for batch ", batch_i, "\n");
      assert(alns.is_valid());
      alns.sort_alns();
      stage_timers.alignments->inc_elapsed(aln_timers.sort_t.get_elapsed());
      size_t num_proper_pairs = 0;
      if (is_paired[batch_i]) alns.select_pairs(num_proper_pairs);
      tot_proper_pairs += num_proper_pairs;
      size_t num_reads_aligned, num_bases_aligned;
      alns.compute_stats(num_reads_aligned, num_bases_aligned);
      tot_reads_aligned += num_reads_aligned;
      tot_bases_aligned += num_bases_aligned;

    }  // for batch_i

    {
      LOG_MEM("After batched sort");
      BarrierTimer bt("After batched sort and computations and before writing files");
    }

    future<> fut_files = make_future();
    for (int batch_i = 0; batch_i < read_group_batch_count; batch_i++) {  // batch_i
      int read_group_id = read_group_start + batch_i;
      if (!needs_generation[read_group_id]) {
        continue;
      }
      auto &alns = rg_alns[read_group_id];

      for (auto &aln : alns) {
        ctgs_covered.add_ctg_range(aln.cid, aln.clen, aln.cstart, aln.cstop);
        progress();
      }
      string &reads_fname = options.reads_fnames[read_group_id];
      auto short_name = get_basename(reads_fname);
      string sam_fname = "final_assembly." + short_name + ".sam.gz";

      if (options.post_assm_write_sam) {
        // check for existence of SAM file - if so don't write again. This can be the slowest component on HPC systems, and we
        // still need to do the other computations to obtain the global avg depth information
        bool need_sam = true;
        if (!rank_me() && filesystem::exists(filesystem::path(sam_fname))) need_sam = false;
        need_sam = upcxx::broadcast(need_sam ? 1 : 0, 0).wait() == 1;
        if (!need_sam) {
          SLOG("SAM file \"", sam_fname, "\" exists: will not write again\n");
        } else {
          auto sh_t = make_shared<BaseTimer>("");
          sh_t->start();
          auto fut = alns.dump_sam_file(sam_fname, options.min_ctg_print_len).then([sh_t]() {
            sh_t->stop();
            *stage_timers.dump_alns += *sh_t;
          });
          fut_files = when_all(fut_files, fut);
          LOG_MEM("After Post Assembly SAM Saved");
        }
      }
      progress();
#ifdef PAF_OUTPUT_FORMAT
      string aln_name("final_assembly-" + short_name + ".paf");
      auto fut_paf = alns.dump_single_file(aln_name, Alns::Format::PAF);
      fut_files = when_all(fut_files, fut_paf);
      SLOG("\n", KBLUE, "PAF alignments can be found at ", options.output_dir, "/", aln_name, KNORM, "\n");
      LOG_MEM("After Post Assembly PAF Alignments Saved");
#elif BLAST6_OUTPUT_FORMAT
      string aln_name("final_assembly-" + short_name + ".b6");
      auto fut_blast = alns.dump_single_file(aln_name, Alns::Format::BLAST);
      fut_files = when_all(fut_files, fut_blast);
      SLOG("\n", KBLUE, "Blast alignments can be found at ", options.output_dir, "/", aln_name, KNORM, "\n");
      LOG_MEM("After Post Assembly BLAST Alignments Saved");
#endif
      progress();
      stage_timers.compute_ctg_depths->start();
      // compute depths 1 column at a time
      aln_depths.compute_for_read_group(alns, read_group_id);
      stage_timers.compute_ctg_depths->stop();
      if (options.checkpoint) {
        auto fut_ckpt = aln_depths.save_checkpoint(read_group_id);
        fut_files = when_all(fut_files, fut_ckpt);
        progress();
      }
      if (upcxx_utils::default_limit_outstanding_count() <= 4) {
        LOG("Blocking on file writes to avoid potential network issues\n");
        fut_files.wait();
      }
      LOG_MEM("Purged Post Assembly Reads " + short_name);
    }  // for batch_i in read_group_batch
    LOG("Done processing batches\n");
    barrier();
    batch_alns.clear();
    aln_timers.done_all();
    aln_timers.clear();
    LOG("Waiting for all files to be completed\n");
    fut_files.wait();
    Timings::wait_pending();
    barrier();
    if (options.checkpoint) {
      // write a log entry so that auto-resume will continue and retry if OOM
      SLOG("Completed post assembly batch ", batch_num, "\n");
    }
    read_group_start += read_group_batch_count;
    LOG_MEM("After batch " + range);
  }  // while read_group_start
  LOG("Waiting on pending\n");
  Timings::wait_pending();
  ctgs.clear_slices();
  fut_ready_new_kmer_dht.wait();
  sh_kmer_ctg_dht.reset();  // free up resources
  aln_depths.restore_checkpoints();
  LOG("waiting on aln done computing\n");
  aln_depths.done_computing();
  kmers_reads_buffers.clear();
  KlignAligner::get_singleton_rget_requests().reset();
  string fname("final_assembly_depths.txt");
  SLOG_VERBOSE("Writing ", fname, "\n");
  aln_depths.dump_depths(fname, options.reads_fnames);
  aln_depths.clear();
  ctgs_covered.done_adding();
  auto covered_bases = ctgs_covered.get_covered_bases();

  {
    BarrierTimer bt("After clearing post-assembly memory");
    LOG_MEM("After clearing post-assembly memory");
  }

  // FIXME get alignment stats per file via PromiseReduce
  // And then summary of the batch of files via PromiseReduce
  // And then summary of the all files

  auto all_covered_bases = reduce_one(covered_bases, op_fast_add, 0).wait();
  auto all_tot_num_reads = reduce_one(tot_num_reads, op_fast_add, 0).wait();
  auto all_tot_num_bases = reduce_one(tot_num_bases, op_fast_add, 0).wait();
  auto all_num_ctgs = reduce_one(ctgs.size(), op_fast_add, 0).wait();
  auto all_ctgs_len = reduce_all(ctgs.get_length(), op_fast_add).wait();
  auto all_reads_aligned = reduce_one(tot_reads_aligned, op_fast_add, 0).wait();
  auto all_bases_aligned = reduce_all(tot_bases_aligned, op_fast_add).wait();
  auto all_proper_pairs = reduce_one(tot_proper_pairs, op_fast_add, 0).wait();
  size_t all_unmapped_bases = 0;

  // every rank needs the avg coverage to calculate the std deviation
  double avg_coverage = (double)all_bases_aligned / all_ctgs_len;
  double sum_diffs = 0;
  for (auto &ctg : ctgs) sum_diffs += pow((double)ctg.depth - avg_coverage, 2.0) * (double)ctg.seq.length();
  auto all_sum_diffs = reduce_one(sum_diffs, op_fast_add, 0).wait();
  auto std_dev_coverage = sqrt(all_sum_diffs / all_ctgs_len);

  SLOG(KBLUE "_________________________", KNORM, "\n");
  SLOG("Alignment statistics\n");
  SLOG("  Reads: ", all_tot_num_reads, "\n");
  SLOG("  Bases: ", all_tot_num_bases, "\n");
  SLOG("  Mapped reads: ", all_reads_aligned, "\n");
  SLOG("  Mapped bases: ", all_bases_aligned, "\n");
  SLOG("  Ref scaffolds: ", all_num_ctgs, "\n");
  SLOG("  Ref bases: ", all_ctgs_len, "\n");
  SLOG("  Percent mapped: ", 100.0 * (double)all_reads_aligned / all_tot_num_reads, "\n");
  SLOG("  Percent bases mapped: ", 100.0 * (double)all_bases_aligned / all_tot_num_bases, "\n");
  //  a proper pair is where both sides of the pair map to the same contig in the correct orientation, less than 32kbp apart
  SLOG("  Percent proper pairs: ", 100.0 * (double)all_proper_pairs / (all_tot_num_reads / 2), "\n");
  // average depth per base
  SLOG("  Average coverage: ", avg_coverage, "\n");
  SLOG("  Average coverage with deletions: N/A\n");
  // standard deviation of depth per base
  SLOG("  Standard deviation: ", std_dev_coverage, "\n");
  // this will always be 100% because the contigs are created from the reads in the first place
  SLOG("  Percent scaffolds with any coverage: 100.0\n");
  SLOG("  Percent of reference bases covered: ", 100.0 * (double)all_covered_bases / all_ctgs_len, "\n");

  stage_timers.alignments->inc_elapsed(stage_timers.build_aln_seed_index->get_elapsed());

  SLOG(KBLUE "_________________________", KNORM, "\n");
  SLOG("Stage timing:\n");
  SLOG("    ", stage_timers.cache_reads->get_final(), "\n");
  SLOG("    ", stage_timers.build_aln_seed_index->get_final(), "\n");
  SLOG("    ", stage_timers.alignments->get_final(), "\n");
  SLOG("      -> ", stage_timers.kernel_alns->get_final(), "\n");
  SLOG("      -> ", stage_timers.aln_comms->get_final(), "\n");
  SLOG("    ", stage_timers.compute_ctg_depths->get_final(), "\n");
  SLOG("    ", stage_timers.dump_alns->get_final(), "\n");
  // if (options.shuffle_reads) SLOG("    ", stage_timers.shuffle_reads->get_final(), "\n");
  SLOG("    FASTQ total read time: ", FastqReader::get_io_time(), "\n");
  SLOG(KBLUE "_________________________", KNORM, "\n");
  chrono::duration<double> t_elapsed = clock_now() - start_t;
  SLOG("Finished in ", setprecision(2), fixed, t_elapsed.count(), " s at ", get_current_time(), " for ", MHM2_VERSION, "\n");

  SLOG("\n", KBLUE, "Aligned unmerged reads to final assembly. Files can be found in directory \"", options.output_dir, "\":\n");
  SLOG(KBLUE, "  \"final_assembly_depths.text\" contains scaffold depths (abundances)", KNORM, "\n");
  if (options.post_assm_write_sam) {
    SLOG(KBLUE, "  \"final_assembly.header.sam.gz\" contains header information for all input file alignments", KNORM, "\n");
    SLOG(KBLUE, "  \"*.sam.gz\" files contain alignments per input/read file", KNORM, "\n");
  }
  SLOG(KBLUE, "_________________________", KNORM, "\n");
}
