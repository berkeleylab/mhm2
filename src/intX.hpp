#pragma once

/* Copied from https://github.com/crepererum/intX_t
   Modified and templated by Rob Egan
   */

#include <cstdint>
#include <climits>
#include <functional>
#include <ostream>

template <typename intX, int bits>
class intX_t final {
 public:
  static_assert(sizeof(intX) >= (bits + 7) / 8, "underlying intX must be larger than the bits requested in intX_t template");

  // default constructors
  intX_t()
      : _s(0) {}
  intX_t(const intX_t& obj) = default;
  intX_t(intX_t&& obj) noexcept = default;

  // converter constructors
  template <typename X>
  intX_t(const X i)
      : _s(static_cast<X>(i)) {}

  // default assigments
  intX_t& operator=(const intX_t& obj) = default;
  intX_t& operator=(intX_t&& obj) noexcept = default;

  // converter assigments
  template <typename X>
  intX_t& operator=(X i) {
    _s = static_cast<X>(i);
    return *this;
  }

  // cast operators
  template <typename X>
  operator X() const {
    return static_cast<X>(_s);
  }

  // comparison
  bool operator==(const intX_t& obj) const { return _s == obj._s; }

  bool operator!=(const intX_t& obj) const { return _s != obj._s; }

  template <typename X, typename intX2, int bits2>
  friend bool operator==(const X i, const intX_t<intX2, bits2>& obj);
  template <typename X, typename intX2, int bits2>
  friend bool operator==(const intX_t<intX2, bits2>& obj, const X i);

  template <typename X, typename intX2, int bits2>
  friend bool operator!=(const X i, const intX_t<intX2, bits2>& obj);
  template <typename X, typename intX2, int bits2>
  friend bool operator!=(const intX_t<intX2, bits2>& obj, const X i);

  // printing
  template <typename intX2, int bits2>
  friend std::ostream& operator<<(std::ostream& stream, const intX_t<intX2, bits2>& obj);

 private:
  intX _s : bits;
} __attribute__((packed));

template <typename intX, int bits>
std::ostream& operator<<(std::ostream& stream, const intX_t<intX, bits>& obj) {
  stream << obj._s;
  return stream;
};

template <typename X, typename intX, int bits>
bool operator==(const X i, const intX_t<intX, bits>& obj) {
  return i == obj._s;
};

template <typename X, typename intX, int bits>
bool operator==(const intX_t<intX, bits>& obj, const X i) {
  return i == obj._s;
};

template <typename X, typename intX, int bits>
bool operator!=(const X i, const intX_t<intX, bits>& obj) {
  return i != obj._s;
};

template <typename X, typename intX, int bits>
bool operator!=(const intX_t<intX, bits>& obj, const X i) {
  return i != obj._s;
}

namespace std {

// specialization of hash
template <typename intX, int bits>
struct hash<intX_t<intX, bits>> {
  size_t operator()(const intX_t<intX, bits>& obj) const {
    // XXX: implement fast path
    return _helper(static_cast<intX>(obj));
  }

  std::hash<std::int64_t> _helper;
};

// specialization of numeric_limits
template <typename intX, int bits>
struct numeric_limits<intX_t<intX, bits>> : public std::numeric_limits<intX> {
  using Base = numeric_limits<intX>;
  static constexpr intX max() { return (intX_t<intX, bits>) Base::max(); }
  static constexpr intX min() { return (intX_t<intX, bits>) Base::min(); }
};

};  // namespace std

using int24_t = intX_t<std::int32_t, 24>;
using uint24_t = intX_t<std::uint32_t, 24>;
using int40_t = intX_t<std::int64_t, 40>;
using uint40_t = intX_t<std::uint64_t, 40>;
using int48_t = intX_t<std::int64_t, 48>;
using uint48_t = intX_t<std::uint64_t, 48>;
using int56_t = intX_t<std::int64_t, 56>;
using uint56_t = intX_t<std::uint64_t, 56>;

static_assert(sizeof(int24_t) == 3, "size of int24_t is not 24bit, check your compiler!");
static_assert(sizeof(int24_t[2]) == 6, "size of int24_t[2] is not 2*24bit, check your compiler!");
static_assert(sizeof(int24_t[3]) == 9, "size of int24_t[3] is not 3*24bit, check your compiler!");
static_assert(sizeof(int24_t[4]) == 12, "size of int24_t[4] is not 4*24bit, check your compiler!");

static_assert(sizeof(int40_t) == 5, "size of int40_t is not 40bit, check your compiler!");
static_assert(sizeof(int40_t[2]) == 10, "size of int40_t[2] is not 2*40bit, check your compiler!");
static_assert(sizeof(int40_t[3]) == 15, "size of int40_t[3] is not 3*40bit, check your compiler!");
static_assert(sizeof(int40_t[4]) == 20, "size of int40_t[4] is not 4*40bit, check your compiler!");

static_assert(sizeof(int48_t) == 6, "size of int48_t is not 48bit, check your compiler!");
static_assert(sizeof(int48_t[2]) == 12, "size of int48_t[2] is not 2*48bit, check your compiler!");
static_assert(sizeof(int48_t[3]) == 18, "size of int48_t[3] is not 3*48bit, check your compiler!");
static_assert(sizeof(int48_t[4]) == 24, "size of int48_t[4] is not 4*48bit, check your compiler!");

static_assert(sizeof(int56_t) == 7, "size of int56_t is not 56bit, check your compiler!");
static_assert(sizeof(int56_t[2]) == 14, "size of int56_t[2] is not 2*56bit, check your compiler!");
static_assert(sizeof(int56_t[3]) == 21, "size of int56_t[3] is not 3*56bit, check your compiler!");
static_assert(sizeof(int56_t[4]) == 28, "size of int56_t[4] is not 4*56bit, check your compiler!");

static_assert(sizeof(uint24_t) == 3, "size of uint24_t is not 24bit, check your compiler!");
static_assert(sizeof(uint24_t[2]) == 6, "size of uint24_t[2] is not 2*24bit, check your compiler!");
static_assert(sizeof(uint24_t[3]) == 9, "size of uint24_t[3] is not 3*24bit, check your compiler!");
static_assert(sizeof(uint24_t[4]) == 12, "size of uint24_t[4] is not 4*24bit, check your compiler!");

static_assert(sizeof(uint40_t) == 5, "size of uint40_t is not 40bit, check your compiler!");
static_assert(sizeof(uint40_t[2]) == 10, "size of uint40_t[2] is not 2*40bit, check your compiler!");
static_assert(sizeof(uint40_t[3]) == 15, "size of uint40_t[3] is not 3*40bit, check your compiler!");
static_assert(sizeof(uint40_t[4]) == 20, "size of uint40_t[4] is not 4*40bit, check your compiler!");

static_assert(sizeof(uint48_t) == 6, "size of uint48_t is not 48bit, check your compiler!");
static_assert(sizeof(uint48_t[2]) == 12, "size of uint48_t[2] is not 2*48bit, check your compiler!");
static_assert(sizeof(uint48_t[3]) == 18, "size of uint48_t[3] is not 3*48bit, check your compiler!");
static_assert(sizeof(uint48_t[4]) == 24, "size of uint48_t[4] is not 4*48bit, check your compiler!");

static_assert(sizeof(uint56_t) == 7, "size of uint56_t is not 56bit, check your compiler!");
static_assert(sizeof(uint56_t[2]) == 14, "size of uint56_t[2] is not 2*56bit, check your compiler!");
static_assert(sizeof(uint56_t[3]) == 21, "size of uint56_t[3] is not 3*56bit, check your compiler!");
static_assert(sizeof(uint56_t[4]) == 28, "size of uint56_t[4] is not 4*56bit, check your compiler!");
