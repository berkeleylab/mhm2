#include "gtest/gtest.h"

#include "intX.hpp"

#include <iostream>

template <typename T, typename S, typename U>
void test_equal(T a, S b, U c) {
  EXPECT_EQ(a, b) << "FAIL !(a == b): a=" << a << " b=" << b << " state=" << c;
  EXPECT_EQ(b, a) << "FAIL !(b == a): a=" << a << " b=" << b << " state=" << c;
  EXPECT_EQ(a == b, true) << "FAIL !(a == b): a=" << a << " b=" << b << " state=" << c;
  EXPECT_EQ(b == a, true) << "FAIL !(b == a): a=" << a << " b=" << b << " state=" << c;
  EXPECT_EQ(a != b, false) << "FAIL a != b: a=" << a << " b=" << b << " state=" << c;
  EXPECT_EQ(b != a, false) << "FAIL b != a: a=" << a << " b=" << b << " state=" << c;
}

template <typename T>
void _run_complete_test(std::int64_t i) {
  T x0(i);
  std::int64_t x1 = x0;
  test_equal(i, x1, x0);
  test_equal(x0, x1, x0);

  T x2(x0);
  test_equal(x0, x2, i);
}

void run_complete_test(std::int64_t i) {
  if (i >= -128 && i < 128) _run_complete_test<int8_t>(i);
  if (i >= -32768 && i < 32768) _run_complete_test<int16_t>(i);
  if (i >= -8388608 && i < 8388608) _run_complete_test<int24_t>(i);
  if (i >= -2147483648 && i < 2147483648) _run_complete_test<int32_t>(i);
  if (i >= -549755813888 && i < 549755813888) _run_complete_test<int40_t>(i);
  if (i >= -140737488355328 && i < 140737488355328) _run_complete_test<int48_t>(i);
  if (i >= -36028797018963968 && i < 36028797018963968) _run_complete_test<int56_t>(i);
_run_complete_test<int64_t>(i);
  if (i >= 0) {
    if (i < 256) _run_complete_test<uint8_t>(i);
    if (i < 65536) _run_complete_test<uint16_t>(i);
    if (i < 16777216) _run_complete_test<uint24_t>(i);
    if (i < 4294967296l) _run_complete_test<uint32_t>(i);
    if (i < 1099511627776l) _run_complete_test<uint40_t>(i);
    if (i < 281474976710656l) _run_complete_test<uint48_t>(i);
    if (i < 72057594037927936l) _run_complete_test<uint56_t>(i);
    _run_complete_test<uint64_t>(i);
  }
}

constexpr std::int64_t M = 65536l;
constexpr std::int64_t M2 = M * M;
constexpr std::int64_t M_minus = 65535l;
constexpr std::int64_t M_2minus = 65534l;
constexpr std::int64_t M_half = 32768l;

TEST(intX_t, All) {
  // positive
  for (std::int64_t i = 0; i < M; i += i + 1) {
    run_complete_test(i);
  }
  for (std::int64_t i = 0; i < M; i += i + 1) {
    for (std::int64_t j = 0; j < 16; ++j) {
      run_complete_test(i * M + j);
    }
    run_complete_test(i * M + M_minus);
  }
  for (std::int64_t i = 0; i < M_half; i += i + 1) {
    for (std::int64_t j = 0; j < 16; ++j) {
      for (std::int64_t k = 0; k < 16; ++k) {
        run_complete_test(i * M2 + j * M + k);
      }
      run_complete_test(i * M2 + M * M_minus);
    }
    run_complete_test(i * M * M2 * M + M_minus);
  }

  // negative
  for (std::int64_t i = 0; i < M_minus; i += i + 1) {
    run_complete_test(-i);
  }
  for (std::int64_t i = 0; i < M; i += i + 1) {
    for (std::int64_t j = 0; j < 16; ++j) {
      run_complete_test(-(i * M + j));
    }
    run_complete_test(-(i * M + M_2minus));
  }
  for (std::int64_t i = 0; i < M_half; i += i + 1) {
    for (std::int64_t j = 0; j < 16; ++j) {
      for (std::int64_t k = 0; k < 16; ++k) {
        run_complete_test(-(i * M2 + j * M + k));
      }
      run_complete_test(-(i * M2 + M * M_2minus));
    }
    run_complete_test(-(i * M * M2 * M + M_2minus));
  }
}
